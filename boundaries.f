C NEWCOUPLED
C
C.....OUTLET BOUNDARIES (CONSTANT GRADIENT BETWEEN BOUNDARY &
CCV-CENTER ASSUMED)
C
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
        CALL FLUXUVW(IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                       XUR(IO),YOR(IO),ZOR(IO),
     *               FMO(IO),CP,CB,ONE,ZERO,1)
        AP(IJP)=AP(IJP)-CB
        SU(IJP)=SU(IJP)-CB*U(IJB)
        SV(IJP)=SV(IJP)-CB*V(IJB)
        SW(IJP)=SW(IJP)-CB*W(IJB)
      END DO
C
C.....Zu Wand- und Symmetrierandbedingungen siehe Peric S.305
C(8.90).
C.....Für die Implizite formulierung wird der in kartesischen
C.....Koordinaten diskretisierte Diffusionsterm verwendet, als
C.....Randbedingung wird hier nur die Dirichletrandbedingung
Ceingebaut.
C.....Da im Rahmen einer verzögerten Korrektur diese DirichletRB
Cauch
C.....auf der rechten seite des GS auftritt kürzt sie sich heraus.
CZur
C.....expliziten Behandlung werden die diffusiven Flüsse durch
C.....Scherkräfte an den Wänden bzw. randnormale Kräfte ersetzt.
CDiese
C.....berechnen sich aus den im lokalen Koordinatensystem
Cangegebenen 
C.....Komponenten des Spannungsvektors aus einer Integration über
Cdie 
C.....Randfläche des Kontrollvolumens
C
C.....WALL BOUNDARIES
C
      DO IW=1,NWALBKAL
        IJP=IJPW(IW)
        IJB=IJW(IW)
        VISS=MAX(VISC,VISW(IW))
        COEF=VISS*SRDW(IW)
 
        ARE=SQRT(XNW(IW)**2+YNW(IW)**2+ZNW(IW)**2)
        UPB=U(IJP)-U(IJB)
        VPB=V(IJP)-V(IJB)
        WPB=W(IJP)-W(IJB)
        VNP=UPB*XNW(IW)+VPB*YNW(IW)+WPB*ZNW(IW)
C.....Calculate componentts of the difference of the tangential
Cvelocities
C.....DU_T=DU-DU*N*N Peric p.316
        XTP=UPB-VNP*XNW(IW)/(ARE**2+SMALL)
        YTP=VPB-VNP*YNW(IW)/(ARE**2+SMALL)
        ZTP=WPB-VNP*ZNW(IW)/(ARE**2+SMALL)
 
        DPB=SQRT((XC(IJP)-XC(IJB))**2+(YC(IJP)-YC(IJB))**2
     *          +(ZC(IJP)-ZC(IJB))**2)
        VISOL=VISS*ARE/DPB
 
        AP(IJP)=AP(IJP)+VISOL
        SU(IJP)=SU(IJP)+VISOL*U(IJP)-COEF*XTP
        SV(IJP)=SV(IJP)+VISOL*V(IJP)-COEF*YTP
        SW(IJP)=SW(IJP)+VISOL*W(IJP)-COEF*ZTP
      END DO
C
C.....SYMMETRY BOUNDARIES
C
      DO ISY=1,NSYMBKAL
        IJP=IJPS(ISY)
        IJB=IJS(ISY)
        COEF=VIS(IJB)*SRDS(ISY)
 
        ARE=SQRT(XNS(ISY)**2+YNS(ISY)**2+ZNS(ISY)**2)
        XNP=XNS(ISY)/(ARE+SMALL)
        YNP=YNS(ISY)/(ARE+SMALL)
        ZNP=ZNS(ISY)/(ARE+SMALL)
        FDE=2.0D0*COEF*((U(IJP)-U(IJB))*XNP
     *                 +(V(IJP)-V(IJB))*YNP
     *                 +(W(IJP)-W(IJB))*ZNP)
 
        DPB=SQRT((XC(IJP)-XC(IJB))**2+(YC(IJP)-YC(IJB))**2
     *          +(ZC(IJP)-ZC(IJB))**2)
        VISOL=VIS(IJB)*ARE/DPB
 
        AP(IJP)=AP(IJP)+VISOL
        SU(IJP)=SU(IJP)+VISOL*U(IJP)-FDE*XNP
        SV(IJP)=SV(IJP)+VISOL*V(IJP)-FDE*YNP
        SW(IJP)=SW(IJP)+VISOL*W(IJP)-FDE*ZNP
      END DO
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES!)
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        MIJ=IBLKOCBK(I)        
        CALL FLUXUVW(IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                       XOCR(I),YOCR(I),ZOCR(I),
     *              FMOC(I),AL(I),AR(I),FOCBK(I),GU,MIJ)
        AP(IJP) =AP(IJP)-AR(I)
        AP(IJN) =AP(IJN)-AL(I)
      END DO
C NEW
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE NO INTERNAL BOUNDARIES!)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
        CALL FLUXUVW(IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                       XFR(I),YFR(I),ZFR(I),
     *               FMF(I),AFL(I),AFR(I),FFSGBK(I),GU,MIJ)
        AP(IJP) =AP(IJP)-AFR(I)
        AP(IJN) =AP(IJN)-AFL(I)
      END DO
C END NEW
C END NEWCOUPLED
C
C.....Call to Inner Walls UVW modifications routine
C
C NEW
C     CALL INNWALLUVW
C     CALL SOURCEUVW
C END NEW
C NEW

