      INTEGER NI,NJ,NK,NIJ,NIJK,NIM
      INTEGER NJM,NKM,IST,JST,KST,IJKST
C
      COMMON /INDEXI/
     *  NI,NJ,NK,NIJ,NIJK,NIM,NJM,NKM,IST,JST,KST,IJKST
C
C
      INTEGER, DIMENSION(NBK):: NIBK,NJBK,NKBK,IBK
      INTEGER, DIMENSION(NBK):: JBK,KBK,IJKBK,NIJKBK
      INTEGER NBLKS,NIBKAL,NKBKAL
      INTEGER NIJKBKAL,NINLBKAL,NOUTBKAL
      INTEGER NSYMBKAL,NOCBKAL,NWALBKAL
C NEW
      INTEGER NMTMBKAL,NFSGBKAL
C END NEW
C
      COMMON /INDEXBK/
     *  NIBK,NJBK,NKBK,IBK,JBK,KBK,IJKBK,NIJKBK,
     *  NBLKS,NIBKAL,NKBKAL,NIJKBKAL,NINLBKAL,NOUTBKAL,
     *  NSYMBKAL,NOCBKAL,NWALBKAL,
C NEW
     *  NMTMBKAL,NFSGBKAL
C END NEW
C
C
      INTEGER LKBK(NZA),LIBK(NXA),LK(NZA),LI(NXA),
C NEWCOUPLED
C    *        ZEROS(2*(NXA*NYA+NXA*NZA+NYA*NZA)),NZERO
     *        ZEROS(4*NXYZA),NZERO
C END NEWCOUPLED
C
      COMMON /INDEXL/
     *  LKBK,LIBK,LK,LI,
C NEW
     *  ZEROS,NZERO
C END NEW
C
C
      INTEGER NSW(NFI),IU,IV,IWW,IP,IEN,IPR,JPR,KPR,MPR
      INTEGER IJKPR,IMON,JMON,KMON,MMON,IJKMON,LSG,NOTT
      INTEGER ITIM,ITIMS,ITIME,ITSTEP,NPCOR,NIGRAD
C NEW
      INTEGER IJKPRC
C END NEW
C 
      COMMON /ICONTRO/
     *  NSW,IU,IV,IWW,IP,IEN,IPR,JPR,KPR,MPR,
     *  IJKPR,IMON,JMON,KMON,MMON,IJKMON,LSG,NOTT,
     *  ITIM,ITSTEP,NPCOR,NIGRAD,
C NEW
     *  IJKPRC
C END NEW
C NEW
C
C
      PetscOffset SUUI,SVVI,SWWI,APPI,APRRI,
     *            UUI,VVI,WWI,PPI,PPPI,TTI,
C NEWCOUPLED
     *            SPPI,
C END NEWCOUPLED
     *            DUXXI,DUYYI,DUZZI,DVXXI,DVYYI,DVZZI,
     *            DWXXI,DWYYI,DWZZI,DPXXI,DPYYI,DPZZI,
     *            DENNI,VISSI,VOLLI,
     *            XCCI,YCCI,ZCCI

      IS ISU,ISV,ISW,ISP
C
      COMMON /OFFSET/
     * SUUI,SVVI,SWWI,APPI,APRRI,
     * UUI,VVI,WWI,PPI,PPPI,
C NEWCOUPLED
     * SPPI,ISU,ISV,ISW,ISP,
C END NEWCOUPLED
     * DUXXI,DUYYI,DUZZI,DVXXI,DVYYI,DVZZI,
     * DWXXI,DWYYI,DWZZI,DPXXI,DPYYI,DPZZI,
     * DENNI,VISSI,VOLLI,
     * XCCI,YCCI,ZCCI

