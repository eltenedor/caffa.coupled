C#############################################################
      SUBROUTINE DISTRIBUTELOAD
C#############################################################
C     This routine initializes the PETSc Matrix and Vector
C     Objects by allocating space for the calculated number of
C     nonzero elements and so distributing the load
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "gradold3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      INTEGER DNNZ(NXYZA*4),IJRGL((NOCA+NFA)*4)
      INTEGER ONNZT(NXYZA*4)
      REAL*8 ONNZARR(1)
      INTEGER I,J,K,M,IJK,IJP,IJN,IJKP,C
C
      Vec ONNZVEC,ONNZL,ONNZGHOST
      PetscOffset ONNZZI
      PetscInt NGH,RANK
      PetscScalar PZERO
      PetscErrorCode IERR
C
      COMMON /MAPPING/ IJRGL
#include "petsc.user.inc"
C=============================================================
C
C......Initialize variables and arrays
C
      PZERO=0.0D0
C NEWCOUPLED
C     IJKP=0
      IJKP=-3
C END NEWCOUPLED
      DNNZ=1
      NGH=0
      IJRGL=0
C.........NOT IMPLEMENTED YET!!!
C
C.....FIND NUMBER OF VALUES THAT NEED TO BE GHOSTED AND CREATE VECTOR
C
C     DO I=1,NOCBKAL
C       IJN=IJRPBK(I)
C       IF (IJN.GT.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
C         NGH=NGH+1
C         IJGH(NGH)=IJN-1
C       END IF
C     END DO
C
C     DO I=1,NFSGBKAL
C       IJN=IJFR(I)
C       IF (IJN.GT.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
C         NGH=NGH+1
C         IJGH(NGH)=IJN-1
C       END IF
C     END DO
C
C     CALL VecCreateGhost(PETSC_COMM_WORLD,
C    *     NIJKBKAL,PETSC_DECIDE,NGH,IJGH,ONNZVEC,IERR)
C     CALL VecGhostGetLocalForm(ONNZVEC,ONNZL,IERR)
C     CALL VecSet(ONNZL,PZERO,IERR)
C     CALL VecGetArray(ONNZL,ONNZARR,ONNZZI,IERR)
C
      DO M=1,NBLKS
      CALL SETIND(M)
C NEWCOUPLED
C
C.....LOOP THROUGH INNER CELLS, CORRECT NUMBER OF NONZEROS
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJK=(IJK-1)*4+1
        DO C=0,2
          DNNZ(IJK+C)=2
        END DO
        C=3
        DNNZ(IJK+C)=4
      END DO
      END DO
      END DO
C END NEWCOUPLED
C
C.....LOOP THROUGH INNER FACES. EAST FACES
C
      DO K=2,NKM
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C NEWCOUPLED
C
C.....NEW MAPPING FOR 4 VARIABLES AT ONE NODE
C.....ASSEMBLED SYSTEM WILL CONSIST OF SUBMATRICES OF SHAPE 4x4
C.....        
C.....             X 0 0 X     U
C.....             0 X 0 X     V                       
C.....             0 0 X X     W
C.....             X X X X     P                    
C.....                                       
        IJK=(IJK-1)*4+1
        DO C=0,2
          DNNZ(IJK+C)     =DNNZ(IJK+C)+2
          DNNZ(IJK+NJ*4+C)=DNNZ(IJK+NJ*4+C)+2
        END DO
        C=3
        DNNZ(IJK+C)     =DNNZ(IJK+C)+4
        DNNZ(IJK+NJ*4+C)=DNNZ(IJK+NJ*4+C)+4
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
C.....LOOP THROUGH INNER FACES. NORTH FACES
C
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C NEWCOUPLED
        IJK=(IJK-1)*4+1
        DO C=0,2
          DNNZ(IJK+C)  =DNNZ(IJK+C)+2
          DNNZ(IJK+4+C)=DNNZ(IJK+4+C)+2
        END DO
        C=3
        DNNZ(IJK+C)  =DNNZ(IJK+C)+4
        DNNZ(IJK+4+C)=DNNZ(IJK+4+C)+4
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
C.....Loop through inner faces. Top
C
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
C NEWCOUPLED
        IJK=(IJK-1)*4+1
        DO C=0,2
          DNNZ(IJK+C)      =DNNZ(IJK+C)+2
          DNNZ(IJK+NIJ*4+C)=DNNZ(IJK+NIJ*4+C)+2
        END DO
        C=3
        DNNZ(IJK+C)      =DNNZ(IJK+C)+4
        DNNZ(IJK+NIJ*4+C)=DNNZ(IJK+NIJ*4+C)+4
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
C.....Determine rows to zero out. West/East bound.
C     
      DO K=1,NK
      DO I=1,NI,NI-1
      DO J=1,NJ
C NEWCOUPLED
        IJKP=IJKP+4
C END NEWCOUPLED
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J-1
C NEWCOUPLED
C.....IJK is now a zero-based index thus the different mapping
        IJK=IJK*4
        DO C=0,3
          ZEROS(IJKP+C)=IJK+C
        END DO
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
C.....Determine rows to zero out. South/North bound.
C     
      DO K=1,NK
      DO I=2,NI-1
      DO J=1,NJ,NJ-1
C NEWCOUPLED
        IJKP=IJKP+4
C END NEWCOUPLED
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J-1
C NEWCOUPLED
        IJK=IJK*4
        DO C=0,3
          ZEROS(IJKP+C)=IJK+C
        END DO
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
C.....Determine rows to zero out. Bottom/Top bound.
C     
      DO K=1,NK,NK-1
      DO I=2,NI-1
      DO J=2,NJ-1
C NEWCOUPLED
        IJKP=IJKP+4
C END NEWCOUPLED
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J-1
C NEWCOUPLED
        IJK=IJK*4
        DO C=0,3
          ZEROS(IJKP+C)=IJK+C
        END DO
      END DO
      END DO
      END DO
      END DO
C NEWCOUPLED
C     NZERO=IJKP
      NZERO=IJKP+3
C END NEWCOUPLED

C
C.....Block Boundaries (OC and FSG)
C
C     DO I=1,NOCBKAL
C.....SUBTRACT THE PROCESSOR OFFSET
C       IJLPBK(I)=IJLPBK(I)-IJKPRC
C       IJP=IJLPBK(I)
C       IJN=IJRPBK(I)
C.....ELEMENT NOT ON PROCESSOR 
C       IF (IJN.GT.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
C         NGH=NGH+1
C         ONNZ(IJP)=ONNZ(IJP)+1.0D0
C         ONNZ(NIJKBKAL+NGH)=ONNZ(NIJKBKAL+NGH)+1.0D0
C         IJRPBK(I)=NIJKBKAL+NGH
C.....ELEMENT ON PROCESSOR
C       ELSE
C         DNNZ(IJP)=DNNZ(IJP)+1
C.....SUBTRACT THE PROCESSOR OFFSET
C         IJRPBK(I)=IJRPBK(I)-IJKPRC
C         DNNZ(IJRPBK(I))=DNNZ(IJRPBK(I))+1
C       END IF
C.....SAVE THE GLOBAL INDEX (FOR ASSEMBLY ONLY)
C       IJRGL(I)=IJN
C     END DO
C
C     DO I=1,NFSGBKAL
C       IJFL(I)=IJFL(I)-IJKPRC
C       IJP=IJFL(I)
C       IJN=IJFR(I)
C       IF (IJN.GE.IJKPRC+NIJKBKAL.OR.IJN.LT.IJKPRC) THEN
C         NGH=NGH+1
C         ONNZ(IJP)=ONNZ(IJP)+1.0D0
C         ONNZ(NIJKBKAL+NGH)=ONNZ(NIJKBKAL+NGH)+1.0D0
C         IJFR(I)=NIJKBKAL+NGH
C       ELSE
C         DNNZ(IJP)=DNNZ(IJP)+1
C         IJFR(I)=IJFR(I)-IJKPRC
C         DNNZ(IJFR(I))=DNNZ(IJFR(I))+1
C       END IF
C       IJRGL(NOCBKAL+I)=IJN
C     END DO
C
C.....SEND NONZEROS IN OFFDIAGONAL PART TO THE RESPECTIVE PROCESSOR
C
C     CALL VecRestoreArray(ONNZL,ONNZARR,ONNZZI,IERR)
C     CALL VecGhostRestoreLocalForm(ONNZVEC,ONNZL,IERR)
C
C     CALL VecGhostUpdateBegin(
C    *     ONNZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C     CALL VecGhostUpdateEnd(ONNZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C.....CONVERT TO INTEGER
C
C     CALL VecGetArray(ONNZVEC,ONNZARR,ONNZZI,IERR)
C NEWCOUPLED
C     DO I=1,NIJKBKAL
C END NEWCOUPLED
C       ONNZT(I)=INT(ONNZ(I))
C     END DO
C     CALL VecRestoreArray(ONNZVEC,ONNZARR,ONNZZI,IERR)
C     CALL VecDestroy(ONNZVEC,IERR)
C
C.....Create Matrix
C
      CALL MatCreate(PETSC_COMM_WORLD,AMAT,IERR)
C NEWCOUPLED
      CALL MatSetSizes(AMAT,NIJKBKAL*4,NIJKBKAL*4,
     *                 PETSC_DECIDE,PETSC_DECIDE,IERR)
C END NEWCOUPLED
      CALL MatSetFromOptions(AMAT,IERR)
C     CALL MatSeqAIJSetPreallocation(AMAT,PETSC_NULL_INTEGER,DNNZ,IERR)
C     DO I=1,NXYZA*4
C       WRITE(*,*) I-1, DNNZ(I)
C     END DO
C     CALL MatMPIAIJSetPreallocation(
C    *     AMAT,PETSC_NULL_INTEGER,DNNZ,PETSC_NULL_INTEGER,ONNZT,IERR)
C......Vereinheitlichung durch Verwendung dieser Routine
C     CALL MatXAIJSetPreallocation(
C    *     AMAT,4,28,0,
C    *     PETSC_NULL_INTEGER,PETSC_NULL_INTEGER,IERR)
      CALL MatSeqBAIJSetPreallocation(
     *     AMAT,4,28,PETSC_NULL_INTEGER,IERR)
C......Not necessary because already using preallocation routine
      CALL MatSetUp(AMAT,IERR)
C     CALL MatDuplicate(,MAT_SHARE_NONZERO_PATTERN,A2,IERR)
C
C.....Create ghosted Solution Vector
C
      CALL VecCreateGhost(
     *     PETSC_COMM_WORLD,NIJKBKAL*4,PETSC_DECIDE,NGH*4,IJGH,
     *     UVWPVEC,IERR)
      CALL VecSetFromOptions(UVWPVEC,IERR)
C
C.....Duplicate ghosted Vector
C
      CALL VecDuplicate(UVWPVEC,SUVWPVEC,IERR)
C
C.....Create strided index sets
C
      CALL ISCreateStride(MPI_COMM_SELF,NIJKBKAL+NGH,0,4,ISU,IERR)
      CALL ISCreateStride(MPI_COMM_SELF,NIJKBKAL+NGH,1,4,ISV,IERR)
      CALL ISCreateStride(MPI_COMM_SELF,NIJKBKAL+NGH,2,4,ISW,IERR)
      CALL ISCreateStride(MPI_COMM_SELF,NIJKBKAL+NGH,3,4,ISP,IERR)
C
C.....Create ghosted Vectors - not implemented yet
C
      CALL VecCreateGhost(
     *     PETSC_COMM_WORLD,NIJKBKAL,PETSC_DECIDE,NGH,IJGH,
     *     DUXVEC,IERR)
      CALL VecSetFromOptions(DUXVEC,IERR)
C
C.....Duplicate ghosted Vector
C
      CALL VecDuplicate(DUXVEC,DUYVEC,IERR)
      CALL VecDuplicate(DUXVEC,DUZVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,DVXVEC,IERR)
      CALL VecDuplicate(DUXVEC,DVYVEC,IERR)
      CALL VecDuplicate(DUXVEC,DVZVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,DWXVEC,IERR)
      CALL VecDuplicate(DUXVEC,DWYVEC,IERR)
      CALL VecDuplicate(DUXVEC,DWZVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,DPXVEC,IERR)
      CALL VecDuplicate(DUXVEC,DPYVEC,IERR)
      CALL VecDuplicate(DUXVEC,DPZVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,TVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,DFXOVEC,IERR)
      CALL VecDuplicate(DUXVEC,DFYOVEC,IERR)
      CALL VecDuplicate(DUXVEC,DFZOVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,DENVEC,IERR)
      CALL VecDuplicate(DUXVEC,VISVEC,IERR)
      CALL VecDuplicate(DUXVEC,VOLVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,XCVEC,IERR)
      CALL VecDuplicate(DUXVEC,YCVEC,IERR)
      CALL VecDuplicate(DUXVEC,ZCVEC,IERR)
C
      CALL VecDuplicate(DUXVEC,APVEC,IERR)
      CALL VecDuplicate(DUXVEC,APRVEC,IERR)
C
#ifndef USE_ZEROS
      CALL VecCreate(PETSC_COMM_WORLD,NSPANVEC,IERR)
      CALL VecSetType(NSPANVEC,VECMPI,IERR)
      CALL VecSetSizes(NSPANVEC,NIJKBKAL*4,PETSC_DECIDE,IERR)
      CALL VecSetFromOptions(NSPANVEC,IERR)
C     CALL VecDuplicate(UVEC,NSPANVEC,IERR)
C
C.....INITIALIZE NULLSPACE VECTOR
C
      CALL VecSet(NSPANVEC,0.0D0,IERR)
      CALL VecGetArray(NSPANVEC,NSPANARR,NSSI,IERR)
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
        IJK=(IJK-1)*4+3
        NSPAN(IJK)=1.0D0
      END DO
      END DO
      END DO
      END DO
C
      CALL VecRestoreArray(NSPANVEC,NSPANARR,NSSI,IERR)
      CALL VecNorm(NSPANVEC,NORM_2,VAL,IERR)
      CALL VecNormalize(NSPANVEC,VAL,IERR)
#endif
C
C
      RETURN
      END
#include "petsc.user.inc"
C
C#############################################################
      SUBROUTINE ASSEMBLESYSUVW(CMAT)
C#############################################################
C     This routine assembles the PETSc Matrix of the linear
C     subsystem corresponding to uvw-momentum balances
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      INTEGER I,J,K,M,IJK,IJKP,IJP,IJN,C
      INTEGER IJRGL((NOCA+NFA)*4)
C
C NEWCOUPLED
      PetscInt       I1,I7,I14,ROW,COL(7*2),COL1,RANK
      PetscScalar    VAL(7*2),VAL1,PONE
C END NEWCOUPLED
      PetscErrorCode IERR
      Mat CMAT
C
      COMMON /MAPPING/ IJRGL
C
#include "petsc.user.inc"
C=============================================================
C     
      I1=1
C NEWCOUPLED
C     I7=7
      I14=14
C END NEWCOUPLED
      PONE=1.0D0
      CALL MatZeroEntries(CMAT,IERR)
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
C
      DO M=1,NBLKS
      CALL SETIND(M)
C
C.....INITIALIZE BOUNDARY ENTRIES SOUTH/NORH
C
      DO K=1,NK
      DO I=1,NI
      DO J=1,NJ,NJ-1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
C NEWCOUPLED
        IJK=(IJK-1)*4+1
        DO C=0,3
          ROW=IJK-1+C
          COL1=IJK-1+C
          CALL MatSetValues(CMAT,I1,ROW,I1,COL1,PONE,INSERT_VALUES,ierr)
        END DO
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
C.....INITIALIZE BOUNDARY ENTRIES WEST/EAST
C
      DO K=1,NK
      DO I=1,NI,NI-1
      DO J=2,NJ-1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
C NEWCOUPLED
        IJK=(IJK-1)*4+1
        DO C=0,3
          ROW=IJK-1+C
          COL1=IJK-1+C
          CALL MatSetValues(CMAT,I1,ROW,I1,COL1,PONE,INSERT_VALUES,ierr)
        END DO
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
C.....INITIALIZE BOUNDARY ENTRIES BOTTOM/TOP
C
      DO K=1,NK,NK-1
      DO I=2,NI-1
      DO J=2,NJ-1
        IJK=IJKPRC+LKBK(K+KST)+LIBK(I+IST)+J
C NEWCOUPLED
        IJK=(IJK-1)*4+1
        DO C=0,3
          ROW=IJK-1+C
          COL1=IJK-1+C
          CALL MatSetValues(CMAT,I1,ROW,I1,COL1,PONE,INSERT_VALUES,ierr)
        END DO
C END NEWCOUPLED
      END DO
      END DO
      END DO
C        
C.....ASSEMBLE MATRIX AND RHS-VECTOR
C
      DO K=3,NKM-1
      DO I=3,NIM-1
      DO J=3,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
C NEWCOUPLED
        IJKP=IJKP*4
C END NEWCOUPLED
        ROW=IJKP
C       
        COL(1)=IJKP-NIJ*4; COL(2)=IJKP-NJ*4; COL(3)=IJKP-4
        COL(4)=IJKP
        COL(5)=IJKP+4; COL(6)=IJKP+NJ*4; COL(7)=IJKP+NIJ*4
C NEWCOUPLED
C        
C.....WORK IN OFFSET TO CORRESPONDING PRESSURE VARIABLE POSITION (U,V,W,P)
C
        DO C=1,7
          COL(C+7)=COL(C)+3
        END DO
C END NEWCOUPLED
C       
        VAL(1)=AB(IJK); VAL(2)=AW(IJK); VAL(3)=AS(IJK)
        VAL(4)=AP(IJK)
        VAL(5)=AN(IJK); VAL(6)=AE(IJK); VAL(7)=AT(IJK)
C NEWCOUPLED
C        
C.....COEFFICIENTS DUE TO IMPLICIT TREATMENT OF PRESSURE GRADIENT
C
        VAL(8)=ABU(IJK); VAL(9)=AWU(IJK); VAL(10)=ASU(IJK)
        VAL(11)=APU(IJK)
        VAL(12)=ANU(IJK); VAL(13)=AEU(IJK); VAL(14)=ATU(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,ierr)
C
C.....WORK IN OFFSET TO V-VARIABLE
C
        DO C=1,7
          COL(C)=COL(C)+1
        END DO
        ROW=ROW+1
C
        VAL(8)=ABV(IJK); VAL(9)=AWV(IJK); VAL(10)=ASV(IJK)
        VAL(11)=APV(IJK)
        VAL(12)=ANV(IJK); VAL(13)=AEV(IJK); VAL(14)=ATV(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,ierr)
C
C.....WORK IN OFFSET TO W-VARIABLE
C
        DO C=1,7
          COL(C)=COL(C)+1
        END DO
        ROW=ROW+1
C
        VAL(8)=ABW(IJK); VAL(9)=AWW(IJK); VAL(10)=ASW(IJK)
        VAL(11)=APW(IJK)
        VAL(12)=ANW(IJK); VAL(13)=AEW(IJK); VAL(14)=ATW(IJK)
C
C END NEWCOUPLED
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,ierr)
      END DO
      END DO
      END DO
C        
C.....WEST/EAST BOUNDARIES
C
      IF (NIM.EQ.2) NIM=3
      DO K=2,NKM
      DO I=2,NIM,NIM-2
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
C NEWCOUPLED
        IJKP=IJKP*4
C END NEWCOUPLED
        ROW=IJKP
C       
C NEWCOUPLED
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C END NEWCOUPLED
C       
        VAL(4)=AP(IJK)
C NEWCOUPLED
        VAL(11)=APU(IJK)
C END NEWCOUPLED
        IF (AB(IJK).NE.0) THEN
            VAL(1)=AB(IJK); COL(1)=IJKP-NIJ*4
            VAL(8)=ABU(IJK);  COL(8)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN
            VAL(2)=AW(IJK); COL(2)=IJKP-NJ*4
            VAL(9)=AWU(IJK);  COL(9)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=AS(IJK); COL(3)=IJKP-4  
            VAL(10)=ASU(IJK);  COL(10)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=AN(IJK); COL(5)=IJKP+4  
            VAL(12)=ANU(IJK);  COL(12)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AE(IJK); COL(6)=IJKP+NJ*4
            VAL(13)=AEU(IJK);  COL(13)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=AT(IJK); COL(7)=IJKP+NIJ*4
            VAL(14)=ATU(IJK);  COL(14)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C.....WORK IN OFFSET TO V-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APV(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABV(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWV(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASV(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANV(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEV(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATV(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C.....WORK IN OFFSET TO W-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APW(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABW(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWW(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASW(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANW(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEW(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATW(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....SOUTH/NORTH BOUNDARIES
C
      IF (NJM.EQ.2) NJM=3
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM,NJM-2
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
C NEWCOUPLED
        IJKP=IJKP*4
C END NEWCOUPLED
        ROW=IJKP
C       
C NEWCOUPLED
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C END NEWCOUPLED
C       
        VAL(4)=AP(IJK)
C NEWCOUPLED
        VAL(11)=APU(IJK)
C END NEWCOUPLED
        IF (AB(IJK).NE.0) THEN
            VAL(1)=AB(IJK); COL(1)=IJKP-NIJ*4
            VAL(8)=ABU(IJK); COL(8)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AW(IJK); COL(2)=IJKP-NJ*4
            VAL(9)=AWU(IJK); COL(9)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=AS(IJK); COL(3)=IJKP-4
            VAL(10)=ASU(IJK); COL(10)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=AN(IJK); COL(5)=IJKP+4  
            VAL(12)=ANU(IJK);  COL(12)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AE(IJK); COL(6)=IJKP+NJ*4
            VAL(13)=AEU(IJK);  COL(13)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=AT(IJK); COL(7)=IJKP+NIJ*4
            VAL(14)=ATU(IJK);  COL(14)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C.....WORK IN OFFSET TO V-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APV(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABV(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWV(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASV(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANV(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEV(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATV(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C.....WORK IN OFFSET TO W-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APW(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABW(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWW(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASW(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANW(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEW(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATW(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....BOTTOM/TOP BOUNDARIES
C
      IF (NKM.EQ.2) NKM=3
      DO K=2,NKM,NKM-2
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
C NEWCOUPLED
        IJKP=IJKP*4
C END NEWCOUPLED
        ROW=IJKP
C       
C NEWCOUPLED
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C END NEWCOUPLED
C       
        VAL(4)=AP(IJK)
C NEWCOUPLED
        VAL(11)=APU(IJK)
C END NEWCOUPLED
        IF (AB(IJK).NE.0) THEN
            VAL(1)=AB(IJK); COL(1)=IJKP-NIJ*4
            VAL(8)=ABU(IJK);  COL(8)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN
            VAL(2)=AW(IJK); COL(2)=IJKP-NJ*4
            VAL(9)=AWU(IJK);  COL(9)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=AS(IJK); COL(3)=IJKP-4  
            VAL(10)=ASU(IJK);  COL(10)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=AN(IJK); COL(5)=IJKP+4  
            VAL(12)=ANU(IJK);  COL(12)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AE(IJK); COL(6)=IJKP+NJ*4
            VAL(13)=AEU(IJK);  COL(13)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=AT(IJK); COL(7)=IJKP+NIJ*4
            VAL(14)=ATU(IJK);  COL(14)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C
C.....WORK IN OFFSET TO V-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APV(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABV(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWV(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASV(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANV(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEV(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATV(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
C.....WORK IN OFFSET TO W-VARIABLE
C
        ROW=ROW+1
        COL(4)=COL(4)+1
        VAL(11)=APW(IJK)
        IF (COL(1).NE.-1) THEN
            COL(1)=COL(1)+1
            VAL(8)=ABW(IJK)
        ENDIF
        IF (COL(2).NE.-1) THEN
            COL(2)=COL(2)+1
            VAL(9)=AWW(IJK)
        ENDIF
        IF (COL(3).NE.-1) THEN
            COL(3)=COL(3)+1
            VAL(10)=ASW(IJK)
        ENDIF
        IF (COL(5).NE.-1) THEN
            COL(5)=COL(5)+1
            VAL(12)=ANW(IJK)
        ENDIF
        IF (COL(6).NE.-1) THEN
            COL(6)=COL(6)+1
            VAL(13)=AEW(IJK)
        ENDIF
        IF (COL(7).NE.-1) THEN
            COL(7)=COL(7)+1
            VAL(14)=ATW(IJK)
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I14,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C
      END DO
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES) NOT IMPLEMENTED 
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRGL(I)
C
        ROW=IJKPRC+IJP-1
        COL1=IJN-1
        VAL1=AR(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
        ROW=IJN-1
        COL1=IJKPRC+IJP-1
        VAL1=AL(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE INTERNAL BOUNDARIES) NOT
C.....IMPLEMENTED
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJRGL(NOCBKAL+I)
C
        ROW=IJKPRC+IJP-1
        COL1=IJN-1
        VAL1=AFR(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
        ROW=IJN-1
        COL1=IJKPRC+IJP-1
        VAL1=AFL(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
      END DO
C
C.....FINAL MATRIX ASSEMBLY
C
C NEWCOUPLED
      CALL MatAssemblyBegin(CMAT,MAT_FLUSH_ASSEMBLY,IERR)
      CALL MatAssemblyEnd(  CMAT,MAT_FLUSH_ASSEMBLY,IERR)
C END NEWCOUPLED
C
      RETURN
      END
#include "petsc.user.inc"
C
C NEWCOUPLED
C#############################################################
      SUBROUTINE ASSEMBLESYSP(CMAT)
C#############################################################
C     This routine assembles the PETSc Matrix of the linear
C     subsystem corresponding to the pressure equation
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      INTEGER I,J,K,M,IJK,IJKP,IJP,IJN,C
      INTEGER IJRGL((NOCA+NFA)*4)
C
      PetscInt       I1,I28,ROW,COL(7*4),COL1,RANK
      PetscScalar    VAL(7*4),VAL1,PONE
      PetscErrorCode IERR
      Mat CMAT
C
      COMMON /MAPPING/ IJRGL
C
#include "petsc.user.inc"
C=============================================================
C     
      I1=1
      I28=28
      PONE=1.0D0
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
C        
C.....ASSEMBLE MATRIX
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=3,NKM-1
      DO I=3,NIM-1
      DO J=3,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*4
        ROW=IJKP+3
C       
        COL(1)=IJKP-NIJ*4; COL(2)=IJKP-NJ*4; COL(3)=IJKP-4
        COL(4)=IJKP
        COL(5)=IJKP+4; COL(6)=IJKP+NJ*4; COL(7)=IJKP+NIJ*4
C        
C.....WORK IN OFFSET TO CORRESPONDING PRESSURE VARIABLE POSITION (U,V,W,P)
C
        DO C=1,7
          COL(C+7)=COL(C)+1
          COL(C+14)=COL(C)+2
          COL(C+21)=COL(C)+3
        END DO
C       
        VAL(1)=ABU(IJK); VAL(2)=AWU(IJK); VAL(3)=ASU(IJK)
        VAL(4)=APU(IJK)
        VAL(5)=ANU(IJK); VAL(6)=AEU(IJK); VAL(7)=ATU(IJK)
C
        VAL(8)=ABV(IJK); VAL(9)=AWV(IJK); VAL(10)=ASV(IJK)
        VAL(11)=APV(IJK)
        VAL(12)=ANV(IJK); VAL(13)=AEV(IJK); VAL(14)=ATV(IJK)
C
        VAL(15)=ABW(IJK); VAL(16)=AWW(IJK); VAL(17)=ASW(IJK)
        VAL(18)=APW(IJK)
        VAL(19)=ANW(IJK); VAL(20)=AEW(IJK); VAL(21)=ATW(IJK)
C
        VAL(22)=AB(IJK); VAL(23)=AW(IJK); VAL(24)=AS(IJK)
        VAL(25)=AP(IJK)
        VAL(26)=AN(IJK); VAL(27)=AE(IJK); VAL(28)=AT(IJK)
C
        CALL MatSetValues(CMAT,I1,ROW,I28,COL,VAL,INSERT_VALUES,ierr)
C
      END DO
      END DO
      END DO
C        
C.....WEST/EAST BOUNDARIES
C
      IF (NIM.EQ.2) NIM=3
      DO K=2,NKM
      DO I=2,NIM,NIM-2
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*4
        ROW=IJKP+3
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+1,-1,-1,-1,
     *        -1,-1,-1,IJKP+2,-1,-1,-1,  
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C       
        VAL(4)=APU(IJK)
        VAL(11)=APV(IJK)
        VAL(18)=APW(IJK)
        VAL(25)=AP(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=ABU(IJK); COL(1)=IJKP-NIJ*4
            VAL(8)=ABV(IJK); COL(8)=COL(1)+1
            VAL(15)=ABW(IJK); COL(15)=COL(1)+2
            VAL(22)=AB(IJK); COL(22)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AWU(IJK); COL(2)=IJKP-NJ*4
            VAL(9)=AWV(IJK); COL(9)=COL(2)+1
            VAL(16)=AWW(IJK); COL(16)=COL(2)+2
            VAL(23)=AW(IJK); COL(23)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=ASU(IJK); COL(3)=IJKP-4
            VAL(10)=ASV(IJK); COL(10)=COL(3)+1
            VAL(17)=ASW(IJK); COL(17)=COL(3)+2
            VAL(24)=AS(IJK); COL(24)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=ANU(IJK);  COL(5)=IJKP+4  
            VAL(12)=ANV(IJK); COL(12)=COL(5)+1
            VAL(19)=ANW(IJK); COL(19)=COL(5)+2
            VAL(26)=AN(IJK); COL(26)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AEU(IJK);  COL(6)=IJKP+NJ*4
            VAL(13)=AEV(IJK); COL(13)=COL(6)+1
            VAL(20)=AEW(IJK); COL(20)=COL(6)+2
            VAL(27)=AE(IJK); COL(27)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=ATU(IJK);  COL(7)=IJKP+NIJ*4
            VAL(14)=ATV(IJK); COL(14)=COL(7)+1
            VAL(21)=ATW(IJK); COL(21)=COL(7)+2
            VAL(28)=AT(IJK); COL(28)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I28,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....SOUTH/NORTH BOUNDARIES
C
      IF (NJM.EQ.2) NJM=3
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM,NJM-2
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*4
        ROW=IJKP+3
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+1,-1,-1,-1,
     *        -1,-1,-1,IJKP+2,-1,-1,-1,  
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C       
        VAL(4)=APU(IJK)
        VAL(11)=APV(IJK)
        VAL(18)=APW(IJK)
        VAL(25)=AP(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=ABU(IJK); COL(1)=IJKP-NIJ*4
            VAL(8)=ABV(IJK); COL(8)=COL(1)+1
            VAL(15)=ABW(IJK); COL(15)=COL(1)+2
            VAL(22)=AB(IJK); COL(22)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AWU(IJK); COL(2)=IJKP-NJ*4
            VAL(9)=AWV(IJK); COL(9)=COL(2)+1
            VAL(16)=AWW(IJK); COL(16)=COL(2)+2
            VAL(23)=AW(IJK); COL(23)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=ASU(IJK); COL(3)=IJKP-4
            VAL(10)=ASV(IJK); COL(10)=COL(3)+1
            VAL(17)=ASW(IJK); COL(17)=COL(3)+2
            VAL(24)=AS(IJK); COL(24)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=ANU(IJK);  COL(5)=IJKP+4  
            VAL(12)=ANV(IJK); COL(12)=COL(5)+1
            VAL(19)=ANW(IJK); COL(19)=COL(5)+2
            VAL(26)=AN(IJK); COL(26)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AEU(IJK);  COL(6)=IJKP+NJ*4
            VAL(13)=AEV(IJK); COL(13)=COL(6)+1
            VAL(20)=AEW(IJK); COL(20)=COL(6)+2
            VAL(27)=AE(IJK); COL(27)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=ATU(IJK);  COL(7)=IJKP+NIJ*4
            VAL(14)=ATV(IJK); COL(14)=COL(7)+1
            VAL(21)=ATW(IJK); COL(21)=COL(7)+2
            VAL(28)=AT(IJK); COL(28)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I28,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C        
C.....BOTTOM/TOP BOUNDARIES
C
      IF (NKM.EQ.2) NKM=3
      DO K=2,NKM,NKM-2
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        IJKP=IJKPRC+IJK-1
        IJKP=IJKP*4
        ROW=IJKP+3
C       
        COL=(/-1,-1,-1,IJKP  ,-1,-1,-1,
     *        -1,-1,-1,IJKP+1,-1,-1,-1,
     *        -1,-1,-1,IJKP+2,-1,-1,-1,  
     *        -1,-1,-1,IJKP+3,-1,-1,-1 /)
C       
        VAL(4)=APU(IJK)
        VAL(11)=APV(IJK)
        VAL(18)=APW(IJK)
        VAL(25)=AP(IJK)
        IF (AB(IJK).NE.0) THEN
            VAL(1)=ABU(IJK); COL(1)=IJKP-NIJ*4
            VAL(8)=ABV(IJK); COL(8)=COL(1)+1
            VAL(15)=ABW(IJK); COL(15)=COL(1)+2
            VAL(22)=AB(IJK); COL(22)=COL(1)+3
        ENDIF
        IF (AW(IJK).NE.0) THEN 
            VAL(2)=AWU(IJK); COL(2)=IJKP-NJ*4
            VAL(9)=AWV(IJK); COL(9)=COL(2)+1
            VAL(16)=AWW(IJK); COL(16)=COL(2)+2
            VAL(23)=AW(IJK); COL(23)=COL(2)+3
        ENDIF
        IF (AS(IJK).NE.0) THEN
            VAL(3)=ASU(IJK); COL(3)=IJKP-4
            VAL(10)=ASV(IJK); COL(10)=COL(3)+1
            VAL(17)=ASW(IJK); COL(17)=COL(3)+2
            VAL(24)=AS(IJK); COL(24)=COL(3)+3
        ENDIF
        IF (AN(IJK).NE.0) THEN
            VAL(5)=ANU(IJK);  COL(5)=IJKP+4  
            VAL(12)=ANV(IJK); COL(12)=COL(5)+1
            VAL(19)=ANW(IJK); COL(19)=COL(5)+2
            VAL(26)=AN(IJK); COL(26)=COL(5)+3
        ENDIF
        IF (AE(IJK).NE.0) THEN
            VAL(6)=AEU(IJK);  COL(6)=IJKP+NJ*4
            VAL(13)=AEV(IJK); COL(13)=COL(6)+1
            VAL(20)=AEW(IJK); COL(20)=COL(6)+2
            VAL(27)=AE(IJK); COL(27)=COL(6)+3
        ENDIF
        IF (AT(IJK).NE.0) THEN
            VAL(7)=ATU(IJK);  COL(7)=IJKP+NIJ*4
            VAL(14)=ATV(IJK); COL(14)=COL(7)+1
            VAL(21)=ATW(IJK); COL(21)=COL(7)+2
            VAL(28)=AT(IJK); COL(28)=COL(7)+3
        ENDIF
C
        CALL MatSetValues(CMAT,I1,ROW,I28,COL,VAL,INSERT_VALUES,IERR)
C
      END DO
      END DO
      END DO
C
      END DO
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
C
C.....O- AND C-GRID CUTS (THESE ARE NOT BOUNDARIES)
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRGL(I)
C
        ROW=IJKPRC+IJP-1
        COL1=IJN-1
        VAL1=AR(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
        ROW=IJN-1
        COL1=IJKPRC+IJP-1
        VAL1=AL(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE INTERNAL BOUNDARIES)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJRGL(NOCBKAL+I)
C
        ROW=IJKPRC+IJP-1
        COL1=IJN-1
        VAL1=AFR(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
        ROW=IJN-1
        COL1=IJKPRC+IJP-1
        VAL1=AFL(I)
        CALL MatSetValue(CMAT,ROW,COL1,VAL1,INSERT_VALUES,IERR)
C
      END DO
C
C.....FINAL MATRIX ASSEMBLY
C
      CALL MatAssemblyBegin(CMAT,MAT_FINAL_ASSEMBLY,IERR)
      CALL MatAssemblyEnd(  CMAT,MAT_FINAL_ASSEMBLY,IERR)
C
      RETURN
      END
#include "petsc.user.inc"
C
C#############################################################
      SUBROUTINE SOLVESYS(FIVEC,IFI,CMAT,RHSVEC)
C#############################################################
C     This routine solves the linear system for momentum and
C     pressure
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      INTEGER IFI,LS,LC
C
      Vec FIVEC,VT1,VT2,RES,RHSVEC
      KSP KRYLOV,KRYLOVP,KRYLOVPIN
      PC PRECON,PRECONP
      PetscReal RTOL,RINIT
      MatNullSpace NULLSP
      PetscErrorCode IERR
      PetscOffset FIII
      Mat CMAT
      INTEGER IJRGL((NOCA+NFA)*4)
      INTEGER I,J,K,IJK,M
      COMMON /OUTER/ LS
      COMMON /CORRECTOR/ LC
      COMMON /PTEMP/ VT1,VT2,RES,
     *               KRYLOV,KRYLOVP,PRECON,PRECONP
      COMMON /MAPPING/ IJRGL
      PetscInt       I1,I7,ROW,COL(7),COL1,RANK
      PetscScalar    VAL(7),VAL1,PONE
      CHARACTER(LEN=100) PREFIX
      REAL*8 FIARR(1)
#ifdef USE_EXPORT
      PetscViewer BINVIEW
#endif
#include "petsc.user.inc"
C     
C=============================================================
C
#ifdef USE_EXPORT
      CALL PetscViewerBinaryOpen(PETSC_COMM_WORLD,
     *                           'mymat',FILE_MODE_WRITE,BINVIEW,IERR)
      CALL MatView(CMAT,BINVIEW,IERR)
      CALL PetscViewerDestroy(BINVIEW,IERR)
      CALL PetscViewerBinaryOpen(PETSC_COMM_WORLD,
     *                           'myrhs',FILE_MODE_WRITE,BINVIEW,IERR)
      CALL VecView(RHSVEC,BINVIEW,IERR)
      CALL PetscViewerDestroy(BINVIEW,IERR)
      CALL PetscViewerBinaryOpen(PETSC_COMM_WORLD,
     *                           'myfi',FILE_MODE_WRITE,BINVIEW,IERR)
      CALL VecView(FIVEC,BINVIEW,IERR)
      CALL PetscViewerDestroy(BINVIEW,IERR)
#endif
C
      IF (INISOL) THEN
        INISOL=.FALSE.
        CALL VecDuplicate(RHSVEC,VT1,IERR)
        CALL VecDuplicate(RHSVEC,VT2,IERR)
C
        CALL KSPCreate(PETSC_COMM_WORLD,KRYLOV,IERR)
        WRITE(PREFIX,"(A13)") "coupledsolve_"
        CALL KSPSetOptionsPrefix(KRYLOV,PREFIX,IERR)
      END IF
      CALL KSPSetOperators(KRYLOV,CMAT,CMAT,IERR)
C
C......CALCULATE RESIDUAL
C
      CALL MatMult(CMAT,FIVEC,VT1,IERR)
      CALL VecCopy(RHSVEC,VT2,IERR)
      CALL VecAXPY(VT2,-1.0D0,VT1,IERR)
      CALL VecNorm(VT2,NORM_2,RINIT,IERR)
C     CALL MatView(CMAT,PETSC_VIEWER_STDOUT_SELF,IERR)
C     CALL VecView(FIVEC,PETSC_VIEWER_STDOUT_SELF,IERR)
C     CALL VecView(RHSVEC,PETSC_VIEWER_STDOUT_SELF,IERR)
C     CALL VecView(VT2,PETSC_VIEWER_STDOUT_SELF,IERR)
      IF (LS.EQ.1) THEN
        IF (.NOT.LREAD) THEN
          RESINI(IFI)=RINIT
          RESFIN(IFI)=RINIT*SORMAX
          RESOR(IFI)=RINIT/(RESINI(IFI)+SMALL)
        ELSE
          RINIT=RESINI(IFI)
        END IF
        RTOL=SOR(IFI)
      ELSE
        RESOR(IFI)=RINIT/(RESINI(IFI)+SMALL)
        RTOL=RESOR(IFI)*SOR(IFI)
        IF (RTOL.GT.SOR(IFI)) RTOL=SOR(IFI)
      END IF
      IF (RINIT/(RESINI(IFI)+SMALL).LT.SORMAX) RETURN
C
C.....SOLVE COUPLED SYSTEM
C
C
C.....CREATE NULLSPACE - PRESSURE CORRECTION ONLY (SINGULAR MATRIX)
C
#ifndef USE_ZEROS
        CALL MatNullSpaceCreate(
     *       PETSC_COMM_WORLD,PETSC_FALSE,1,NSPANVEC,NULLSP,IERR)
        CALL KSPSetNullSpace(KRYLOV,NULLSP,IERR)
#endif
#ifdef USE_INFO
        CALL MatNullSpaceTest(NULLSP,CMAT,ISNULL,IERR)
        WRITE(*,*) "ISNULL", ISNULL
#endif
      CALL KSPSetTolerances(KRYLOV,
     *                      RTOL, 
     *                      PETSC_DEFAULT_REAL,
     *                      PETSC_DEFAULT_REAL,
     *                      PETSC_DEFAULT_INTEGER,
     *                      IERR)
      CALL KSPSetFromOptions(KRYLOV,IERR)
      CALL KSPSolve(KRYLOV,RHSVEC,FIVEC,IERR)
C
C
      RETURN
      END
#include "petsc.user.inc"
C END NEWCOUPLED
C
C#############################################################
      SUBROUTINE DESTROYPETSC
C#############################################################
C     This routine destroys the PETSc Matrix of the linear
C     system to be solved and objects related to SOLVESYS
C=============================================================
C
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "gradold3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      Vec FIVEC,VT1,VT2,RES
      KSP KRYLOV,KRYLOVP
      PC PRECON,PRECONP
      PetscErrorCode IERR
      COMMON /PTEMP/ VT1,VT2,RES,
     *               KRYLOV,KRYLOVP,PRECON,PRECONP
C     
C=============================================================
C
C.....DESTROY VECTORS
C
      CALL VecDestroy(UVWPVEC,IERR)
C
      CALL VecDestroy(DUXVEC,IERR)
      CALL VecDestroy(DUYVEC,IERR)
      CALL VecDestroy(DUZVEC,IERR)
C
      CALL VecDestroy(DVXVEC,IERR)
      CALL VecDestroy(DVYVEC,IERR)
      CALL VecDestroy(DVZVEC,IERR)
C
      CALL VecDestroy(DWXVEC,IERR)
      CALL VecDestroy(DWYVEC,IERR)
      CALL VecDestroy(DWZVEC,IERR)
C
      CALL VecDestroy(DPXVEC,IERR)
      CALL VecDestroy(DPYVEC,IERR)
      CALL VecDestroy(DPZVEC,IERR)
C
      CALL VecDestroy(TVEC,IERR)
C
      CALL VecDestroy(DFXOVEC,IERR)
      CALL VecDestroy(DFYOVEC,IERR)
      CALL VecDestroy(DFZOVEC,IERR)
C
      CALL VecDestroy(DENVEC,IERR)
      CALL VecDestroy(VISVEC,IERR)
      CALL VecDestroy(VOLVEC,IERR)
C
      CALL VecDestroy(XCVEC,IERR)
      CALL VecDestroy(YCVEC,IERR)
      CALL VecDestroy(ZCVEC,IERR)
C
      CALL VecDestroy(SUVWPVEC,IERR)
      CALL VecDestroy(APVEC,IERR)
      CALL VecDestroy(APRVEC,IERR)
C
      CALL VecDestroy(VT1,IERR)
      CALL VecDestroy(VT2,IERR)
      CALL VecDestroy(RES,IERR)
C
C.....DESTROY MATRICES
C
      CALL MatDestroy(AMAT,IERR)
C
C.....DESTROY KSP
C
      CALL KSPDestroy(KRYLOV,IERR) 
C
#ifndef USE_ZEROS
      CALL VecDestroy(NSPANVEC,IERR)
#endif
C
      RETURN
      END
C
C
C#############################################################
      SUBROUTINE DEBUGME
C#############################################################
C     This routine serves only for debugging purposes using
C     more than one mpi process
C=============================================================
C
C INCLUDE NECESSARY SYSTEM ROUTINES LIKE GETPIT AND HOSTNM
#ifdef USE_INTEL_COMPILER
        USE IFPORT
#endif
        IMPLICIT NONE
#include "mpif.h"
        CHARACTER*20 HNAME
        INTEGER I,PID,HSTAT
C
        HSTAT = HOSTNM(HNAME)
        PID = GETPID()
        PRINT *,"PID ",PID,"ON HOST ",TRIM(HNAME)," IS READY FOR ATTACH"
        I = 0
500     IF (I == 0) THEN
          CALL SLEEP(5)
          GOTO 500
        END IF
C
        RETURN
        END SUBROUTINE DEBUGME
C
C
