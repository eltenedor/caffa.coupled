C##########################################################
      PROGRAM CAFFA
C##########################################################
C     This version of CAFFA has been extended to 3D.
C     Please see below
CC 
C     This code incorporates the Finite Volume Method using
C     SIMPLE algorithm on colocated body-fitted grids. For
C     a description of the solution method, see the book by
C     Ferziger and Peric (1996) or paper by Demirdzic,
C     Muzaferija and Peric (1996). Description of code 
C     features is provided in the acompanying README-file.
C     This version is based on the laminar code including 
C     the multiple pressure corrections and the effects of 
C     non-smoothness of grid lines when computing gradients
C     (see code caffac.f in directory 2dgl) and includes the 
C     k-eps turbulence model, which was implemented by 
C     Martin Schmid, PhD student at the Institute of
C     Shipbuilding in Hamburg.
C
C     This is Version 1.3 of the code, August 1997.
C
C     The user may modify the code and give it to third
C     parties, provided that an acknowledgement to the
C     source of the original version is retained.
C
C                M. Peric, Hamburg, 1996
C                peric@schiffbau.uni-hamburg.de
C                M. Schmid, Hamburg, 1997
C                schmid@schiffbau.uni-hamburg.de
C     
C     This version has been extended to 3D problems in
C     block-structured grids. The K-e model has been
C     retained and a simple Smagorinsky LES model
C     has been introduced as well.
C
C     However, the functionality of multiple grid
C     levels has been left out. A future
C     extension to multigrid is planed.
C
C     The 'K' index now refers to the third ('Z') direction,
C     and not to the grid level as before.
C
C     Also a new index 'M' has been introduced for the 
C     grid-block numbering. General interfaces, between
C     and within, grid blocks are treated now, in addition
C     to the original OC cuts. The domain is now block
C     structured then.
C
C     Otherwise, the original notation was kept wherever
C     possible, and the book by Ferziger & Peric has been
C     followed as closely as possible for the 3D extension,
C     as well as the multi-block treatment.
C
C     Also an option for the linear solver has been introduced.
C     The code can be compiled to run Stone's SIP Solver
C     or CGSTAB solver. Please see the readme file
C
C     Optional compiler directives for OpenMP parallelization
C     at grid block level have been introduced. Please
C     see the readme file.
C       
C
C===========================================================
C NB: CAFFA stands for "Computer Aided Fluid Flow Analysis". 
C===========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "varold3d.inc"
#include "charac3d.inc"
#include "model3d.inc"
#include "grad3d.inc"
c
      INTEGER NCASE,IJK,I,ICONT,LS,M
      INTEGER NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT,K,J
C NEW
      INTEGER RCOUNTG,RCOUNT
C END NEW
c
      REAL*8 SOURCE,ERRORV,ERRORP,UA,VA,UVA,UVW,HPA,HPA2,
C NEW
     *              ERRORVG,ERRORPG
C END NEW
      REAL*8  LPI,TCA,TCD,WA
      PARAMETER (LPI=3.141592653589793238462643383279d0)
C NEW
      INTEGER PID,OMP_GET_THREAD_NUM,NTHREADS,OMP_GET_NUM_THREADS
      INTEGER IJP,IJB,II
      PetscErrorCode IERR
      PetscMPIInt RANK
      PetscLogDouble CALTIME,CALTIMS
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C END NEW
C=========================================================
C
C.....SET SOME CONSTANTS
C
C NEW
      CALL PetscInitialize(PETSC_NULL_CHARACTER,IERR)
      CALL MPI_COMM_RANK(PETSC_COMM_WORLD,RANK,IERR)
C END NEW
      CALL SETDAT
C      CALL MODDAT
C NEW
C     U(1:NXYZA)=0.; V(1:NXYZA)=0.; W(1:NXYZA)=0.; P(1:NXYZA)=0.
C END NEW
      F1(1:NXYZA)=0.; F2(1:NXYZA)=0.; F3(1:NXYZA)=0.;
C
C.....READ PROBLEM NAME AND OPEN FILES
C
C NEW
      IF (RANK.EQ.0) THEN
C END NEW
      PRINT *, ' ENTER PROBLEM NAME (SIX CHARACTERS):  '

C      READ(*,'(A6)') NAME
      WRITE(*,*) '****************************************************'
C NEW
      END IF
C END NEW
      NAME='control'
C NEW
      IF (RANK.EQ.0) THEN
C END NEW
      WRITE(*,*)'NAME OF PROBLEM SOLVED ',NAME
      WRITE(*,*) ''
      WRITE(*,*) '****************************************************'
C NEW
      END IF
C END NEW
C     
      NCASE=INDEX(NAME,' ')
      
      IF(NCASE.NE.ZERO) STOP
      WRITE( FILIN,'(A7,4H.cin)') NAME
      WRITE(FILOUT,'(A7,4H.out)') NAME
C     WRITE(FILBCK,'(A7,4H.bck)') NAME
C NEW
      WRITE(FILERR,'(A7,4H.err)') NAME
      WRITE(FILBCK,'(A7,I4.4,4H.bck)') NAME, RANK
      WRITE(FILPRC,'(A7,I4.4,4H.prc)') NAME, RANK
C END NEW

C
      OPEN (UNIT=5,FILE=FILIN)
      OPEN (UNIT=2,FILE=FILOUT)
C     OPEN (UNIT=4,FILE=FILBCK,FORM='binary')
      OPEN (UNIT=4,FILE=FILBCK,FORM='UNFORMATTED',POSITION='REWIND')
C NEW
      IF (RANK.EQ.0) OPEN (UNIT=23,FILE=FILERR)
      OPEN (UNIT=9,FILE=FILPRC)
C END NEW
      REWIND 2
      REWIND 5
C     REWIND 4
C NEW
      REWIND 9
C END NEW
C
C.....INPUT DATA AND INITIALIZATION
C
      CALL INIT
C
C.....INITIAL OUTPUT
C
      CALL OUTIN
      ITIM=0
      TIME=0.
C
C======================================================
C.....READ RESULTS OF PREVIOUS RUN IF REQUIRED
C======================================================
C
      IF(LREAD) THEN
C
C.....EXTRACT ARRAYS FROM VECTORS
C
C NEW COUPLED
      CALL VecGetSubVector(UVWPVEC,ISU,UVEC)
      CALL VecGetSubVector(UVWPVEC,ISV,VVEC)
      CALL VecGetSubVector(UVWPVEC,ISW,WVEC)
      CALL VecGetSubVector(UVWPVEC,ISP,PVEC)
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C END NEW COUPLED
C NEW
C         WRITE(FILRES,'(A7,4H.res)') NAME
          WRITE(FILRES,'(A7,I4.4,4H.res)') NAME, RANK
C         OPEN (UNIT=3,FILE=FILRES,FORM='binary')
          OPEN (UNIT=3,FILE=FILRES,FORM='UNFORMATTED',POSITION='REWIND')
C END NEW
          READ(3)ITIM,TIME,(F1(IJK),IJK=1,NIJKBKAL),
     *        (F2(IJK),IJK=1,NIJKBKAL),(F3(IJK),IJK=1,NIJKBKAL),
     *        (U(IJK), IJK=1,NIJKBKAL),(V(IJK), IJK=1,NIJKBKAL),
     *        (W(IJK), IJK=1,NIJKBKAL),(P(IJK), IJK=1,NIJKBKAL),
     *        (T(IJK), IJK=1,NIJKBKAL),(TE(IJK),IJK=1,NIJKBKAL),
     *        (ED(IJK),IJK=1,NIJKBKAL),(FMOC(I),I=1,NOCBKAL),
C NEW
     *        (FMF(I),I=1,NFSGBKAL),(RESINI(I),I=1,4),
     *        (RESOR(I),I=1,4)
C END NEW
C
          IF(LTIME) READ(3) (UO(IJK),IJK=1,NIJKBKAL),
     *        (VO(IJK),IJK=1,NIJKBKAL),(WO(IJK),IJK=1,NIJKBKAL),
     *        (TO(IJK),IJK=1,NIJKBKAL),(TEO(IJK),IJK=1,NIJKBKAL),
     *        (EDO(IJK),IJK=1,NIJKBKAL)
C         REWIND 3
          CLOSE(UNIT=3)
C
        ITIM=ITIM-1
C NEW
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreSubVector(UVWPVEC,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISP,PVEC,IERR)
C
      ELSE
C END NEW
C     ENDIF
C
C======================================================
C.....START TIME LOOP
C======================================================
C
c
C======================================================
C NEW
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGetSubVector(UVWPVEC,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPVEC,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPVEC,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPVEC,ISP,PVEC,IERR)
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
C
C END NEW
C Initialize FIELD VALUES
       TCA=LPI*0.25D0
       TCD=LPI*0.5D0
C NEW
C.....OpenMP : Here starts parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*            NKMT,NIMT,NJMT,KSTT,ISTT)
       DO M=1,NBLKS
         NKMT=NKBK(M)-1
         NIMT=NIBK(M)-1
         NJMT=NJBK(M)-1
         KSTT=KBK(M)
         ISTT=IBK(M)
         NJT=NJMT+1
         NIJT=(NIMT+1)*NJT
c     
         DO K=1,NKMT+1 
         DO I=1,NIMT+1
         DO J=1,NJMT+1
           IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
#ifdef USE_ANALYTICAL
           U(IJK)=0.5d0*DSIN(LPI*XC(IJK))*DCOS(LPI*YC(IJK))
     *          *COS(LPI*ZC(IJK)) 
           V(IJK)=0.5d0*DSIN(LPI*YC(IJK))*DCOS(LPI*XC(IJK))
     *          *DCOS(LPI*ZC(IJK))
           W(IJK)=-DSIN(LPI*ZC(IJK))*DCOS(LPI*XC(IJK))
     *          *DCOS(LPI*YC(IJK)) 
           P(IJK)=(-(TCA**2.D0)/2.D0)*(DEXP(2.D0*TCA*XC(IJK))+
     *         DEXP(2.D0*TCA*YC(IJK))+DEXP(2.D0*TCA*ZC(IJK))
     *         +DSIN(XC(IJK)**2.D0+YC(IJK)**2.D0+ZC(IJK)**2.D0)
     *         *DCOS(XC(IJK)**2.D0+YC(IJK)**2.D0+ZC(IJK)**2.D0))
C          P(IJK)=0.0D0
#else
C           U(IJK)=4.D0*0.3D0*YC(IJK)*(0.41D0-YC(IJK))/(0.41D0**2)
            U(IJK)=0.0D0
            V(IJK)=0.0D0
            W(IJK)=0.0D0  
#endif
         ENDDO
         ENDDO
         ENDDO
         DO K=2,NKMT 
         DO I=2,NIMT
         DO J=2,NJMT
           IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
           U(IJK)=U(IJK)*0.1D0
           V(IJK)=V(IJK)*0.1D0
           W(IJK)=W(IJK)*0.1D0
           P(IJK)=P(IJK)*0.1D0
C          U(IJK)=U(IJK)*1.0D0
C          V(IJK)=V(IJK)*1.0D0
C          W(IJK)=W(IJK)*1.0D0
C          P(IJK)=P(IJK)*1.0D0
         ENDDO
         ENDDO
         ENDDO
      ENDDO
C NEW
      END IF
C.....OpenMP : Here ends this parallel loop section
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
C
      CALL VecRestoreSubVector(UVWPVEC,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISP,PVEC,IERR)
C END NEW
C
C
      INIBC=.TRUE.
C NEW
      INISOL=.TRUE.
C END NEW
      ICONT=0
      ITIMS=ITIM+1
      ITIME=ITIM+ITSTEP
C
      CALL PetscBarrier(UVEC,IERR)
      CALL PetscTime(CALTIMS,IERR)
      DO 400 ITIM=ITIMS,ITIME
      TIME=TIME+DT
C
C.....SHIFT SOLUTIONS IN TIME (OOLD = OLD, OLD = CURRENT)
C
      IF(LTIME) THEN
        UOO=UO; VOO=VO; WOO=WO
C NEW
C       UO= U;  VO= V;  WO= WC
C.....EXTRACT ARRAYS FROM VECTORS
C
       CALL VecGetSubVector(UVWPVEC,ISU,UVEC,IERR)
       CALL VecGetSubVector(UVWPVEC,ISV,VVEC,IERR)
       CALL VecGetSubVector(UVWPVEC,ISW,WVEC,IERR)
C
       CALL VecGetArray(UVEC,UARR,UUI,IERR)
       CALL VecGetArray(VVEC,VARR,VVI,IERR)
       CALL VecGetArray(WVEC,WARR,WWI,IERR)
       CALL VecGetArray(TVEC,TARR,TTI,IERR)
        DO IJK=1,NIJKBKAL
          UO(IJK)=U(IJK)
          VO(IJK)=V(IJK)
          WO(IJK)=W(IJK)
          TO(IJK)=T(IJK)
        END DO
       CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
       CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
       CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
       CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreSubVector(UVWPVEC,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISW,WVEC,IERR)
C END NEW
C       TOO=TO;   TO= T
        TOO=TO
        TEOO=TEO; TEO=TE;
        EDOO=EDO; EDO=ED;
C
        WRITE(2,*) '  '
        WRITE(2,*) '  TIME = ',TIME
        WRITE(2,*) '  *****************************'
      ENDIF
C
C.....SET INLET BOUNDARY CONDITIONS
C
      IF(INIBC) CALL BCIN
C
C.....PRINT INITAL FIELDS
C
C      IF(LOUTS.AND.(ITIM.EQ.ITIMS)) CALL OUTRES
C
C.....PRINT INDEX TO MONITORING LOCATION 
C
C     WRITE(2,600) MMON,IMON,JMON,KMON
C
C======================================================
C.....START COUPLED ALGORITHM (OUTER ITERATIONS)
C======================================================
C
      DO LS=1,LSG
        IF(LCAL(IU))    CALL CALCUVW
        IF(LCAL(IP))    CALL CALCP(1)
                        CALL MASSFLUX
C       IF(LCAL(IEN))   CALL CALCSC(IEN,T,TO,TOO)
C
C.....Call to LES Smagorinsky routine.
C
C        IF(LCAL(ISMG))  CALL SMAGOR
C
C.....Call to K-e routines
C
C        IF(LCAL(ITE))   CALL CALCSC(ITE,TE,TEO,TEOO)
C        IF(LCAL(IED))   CALL CALCSC(IED,ED,EDO,EDOO)
C        IF(LCAL(IVIS))  CALL MODVIS
C
C.....NORMALIZE RESIDUALS, PRINT RES. LEVELS AND MONITORING VALUES
C
       SOURCE=MAX(RESOR(IU),RESOR(IV),RESOR(IWW),RESOR(IP))
        IF (RANK.EQ.0) THEN
          PRINT "(I8.7,4E12.4)",
C    *          LS,RESOR(IU),RESOR(IV),RESOR(IWW),RESOR(IP)
     *          LS,RESOR(IU)
          WRITE(23,"(I8.7,4E12.4)"),
C    *          LS,RESOR(IU),RESOR(IV),RESOR(IWW),RESOR(IP)
     *          LS,RESOR(IU)
        END IF
C NEW
        IF (.NOT.LTIME.AND.LWRITE
     *                .AND.MOD(LS,NOTT).EQ.0) 
     *    CALL SRES(RANK)
C END NEW
        IF(SOURCE.GT.SLARGE) GO TO 510
        IF(SOURCE.LT.SORMAX) GO TO 250
      END DO
C
  250 CONTINUE
C
C==========================================================
C.....SAVE SOLUTIONS FOR RE-START OR POSTPROCESSING; OUTPUT
C==========================================================
C
C.....UNSTEADY FLOW - INTERMEDIATE SOLUTIONS:
C
C NEW - OUTPUT STILL NOT IMPLEMENTED COMPLETELY
C END NEW
        IF(LTIME) THEN
C         WRITE(*,606) LS,(RESOR(I),I=1,5),(RESOR(I),I=7,8),
C    *            U(IJKMON),V(IJKMON),W(IJKMON),P(IJKMON),
C    *            T(IJKMON),RXVISC
C
C NEW
C         IF(MOD(ITIM,NOTT).EQ.0.AND.LWRITE) THEN
          IF(MOD(ITIM,NOTT).EQ.0.AND.LPOST) THEN
C END NEW
            ICONT=ICONT+1
C NEW
C           CALL POST(ICONT)
            CALL POST(ICONT,RANK)
C END NEW
          ENDIF
        ENDIF
C NEW
C END NEW
C
  400 CONTINUE
C NEW
      IF (RANK.EQ.0) THEN
        CALL PetscTime(CALTIME,IERR)
        PRINT "(A,E12.4)", "TIME FOR CALCULATION:", CALTIME-CALTIMS
        WRITE(23,"(A,E12.4)") ,"TIME FOR CALCULATION:",CALTIME-CALTIMS
      END IF
C END NEW
C
C.....STEADY FLOW, OR UNSTEADY FLOW - LAST TIME STEP: PRINT AND
C     SAVE RESULTS 
C
       ICONT=ICONT+1
       IF(LOUTE) CALL OUTRES
C NEW
       IF (LWRITE) CALL SRES(RANK)
C      CALL POST(ICONT)
       IF (LPOST) CALL POST(ICONT,RANK)
C      CALL SRES
C END NEW
       ITIM=0
       TIME=0.
C
C==========================================================
C     POSTPROCESSING ERROR CALCULATION FALK
C==========================================================
      ERRORV=0.d0
      ERRORP=0.d0
      RCOUNT=0
C NEW
      RCOUNTG=0
C END NEW
      TCA=LPI*0.25d0
      TCD=LPI*0.5d0
      M=1
      NKMT=NKBK(M)-1
      NIMT=NIBK(M)-1
      NJMT=NJBK(M)-1
      KSTT=KBK(M)
      ISTT=IBK(M)
      NJT=NJMT+1
      NIJT=(NIMT+1)*NJT
c      
      K=3
      I=2
      J=2
      IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
      HPA2=(-(TCA**2.D0)/2.D0)*(DEXP(2.D0*TCA*XC(IJK))
     *     +DEXP(2.D0*TCA*YC(IJK))+DEXP(2.D0*TCA*ZC(IJK))
     *     +DSIN(XC(IJK)**2.D0+YC(IJK)**2.D0+ZC(IJK)**2.D0)
     *     *DCOS(XC(IJK)**2.D0+YC(IJK)**2.D0+ZC(IJK)**2.D0))
C NEW
      CALL MPI_BCAST(HPA2,2,MPI_REAL8,0,PETSC_COMM_WORLD,IERR)
C END NEW
C       
C NEWCOUPLED
      CALL VecGetSubVector(UVWPVEC,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPVEC,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPVEC,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPVEC,ISP,PVEC,IERR)

      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
C END NEWCOUPLED
      DO M=1,NBLKS
        NKMT=NKBK(M)-1
        NIMT=NIBK(M)-1
        NJMT=NJBK(M)-1
        KSTT=KBK(M)
        ISTT=IBK(M)
        NJT=NJMT+1
        NIJT=(NIMT+1)*NJT
C      
        DO K=2,NKMT
        DO I=2,NIMT
        DO J=2,NJMT
          RCOUNT=RCOUNT+1
          IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
C NEW
          UA=0.5d0*DSIN(LPI*XC(IJK))*DCOS(LPI*YC(IJK))
     *         *DCOS(LPI*ZC(IJK)) 
          VA=0.5d0*DSIN(LPI*YC(IJK))*DCOS(LPI*XC(IJK))
     *         *DCOS(LPI*ZC(IJK))
          WA=-DSIN(LPI*ZC(IJK))*DCOS(LPI*XC(IJK))
     *         *DCOS(LPI*YC(IJK))            
C END NEW
          UVA=DSQRT(UA**2+VA**2+WA**2)
          UVW=DSQRT(U(IJK)**2+V(IJK)**2+W(IJK)**2)
          HPA=(-(TCA**2.D0)/2.D0)*(DEXP(2.D0*TCA*XC(IJK))+
     *         DEXP(2.D0*TCA*YC(IJK))+DEXP(2.D0*TCA*ZC(IJK))
     *         +DSIN(XC(IJK)**2.D0+YC(IJK)**2.D0+ZC(IJK)**2.D0)
     *         *DCOS(XC(IJK)**2.D0+YC(IJK)**2.D0+ZC(IJK)**2.D0))
c     
          ERRORV=ERRORV+(UVA-UVW)**2
C         ERRORP=ERRORP+((HPA-HPA2)-P(IJK))**2
          ERRORP=ERRORP+(HPA-P(IJK))**2
        ENDDO
        ENDDO
        ENDDO
      ENDDO    
C NEW
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
C
      CALL VecRestoreSubVector(UVWPVEC,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISP,PVEC,IERR)
C
C.....GLOBAL REDUCE OPERATION HERE!
C
       CALL MPI_REDUCE(
     *      ERRORV,ERRORVG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
       CALL MPI_REDUCE(
     *      ERRORP,ERRORPG,1,MPI_REAL8,MPI_SUM,0,PETSC_COMM_WORLD,IERR)
       CALL MPI_REDUCE(
     *      RCOUNT,RCOUNTG,1,MPI_INTEGER,MPI_SUM,0,MPI_COMM_WORLD,IERR)
       ERRORV=ERRORVG
       ERRORP=ERRORPG
       RCOUNT=RCOUNTG
C END NEW
      ERRORV=DSQRT(ERRORV/RCOUNT)
      ERRORP=DSQRT(ERRORP/RCOUNT)
C NEW
      IF (RANK.EQ.0) THEN
      WRITE(*,*)'FEHLER ZUR ANALYTISCHEN LÖSUNG GESCHW.',ERRORV
      WRITE(*,*)'FEHLER ZUR ANALYTISCHEN LÖSUNG DRUCK',ERRORP      
      WRITE(23,*)'FEHLER ZUR ANALYTISCHEN LÖSUNG GESCHW.',ERRORV
      WRITE(23,*)'FEHLER ZUR ANALYTISCHEN LÖSUNG DRUCK',ERRORP      
      END IF
C END NEW
C
C==========================================================
C.....CLOSE FILES, FORMATS
C==========================================================
C
      CLOSE(UNIT=8)
      CLOSE(UNIT=3)
      CLOSE(UNIT=4)
      CLOSE(UNIT=2)
      CLOSE(UNIT=5)
C NEW
      IF (RANK.EQ.0) CLOSE(UNIT=23)
C END NEW
C
      IF(RANK.EQ.0)
     *  PRINT *,'     *** CALCULATION FINISHED - SEE RESULTS ***'
C NEW
      CALL DESTROYPETSC
      CALL PetscFinalize(IERR)
C END NEW
      STOP
C
C.....MESSAGE FOR DIVERGENCE 
C
  510 IF (RANK.EQ.0) 
     *   PRINT *,'     *** TERMINATED - OUTER ITERATIONS DIVERGING ***'
C NEW
      CALL DESTROYPETSC
      CALL PetscFinalize(IERR)
C NEW
#include "petsc.user.inc"
C END NEW
C END NEW
C
C.....FORMAT SPECIFICATIONS
C
  600 FORMAT('IT',1X,
     * 'I-------------ABSOLUTE RESIDUAL SOURCE SUMS------------I',
     * 2X,
     * 'I--VALUES AT MONITOR POINT(',1X,I2,',',I3,',',I3,',',I3,
     * ')--I',/,
     * 'Nº',1X,2X,'UMOM',4X,'VMOM',4X,'WMOM',4X,'MASS',4X,
     *            'ENER',4X,'TKEN',4X,'TDIS',2X,
     * 2X,3X,'U' ,7X,'V',7X,'W',7X,'P',7X,'T',5X,'RXVIS')
  606 FORMAT(I6.6,1X,1P,7E8.1,2X,1P,6E8.1)
C
C
      END
C
C
C#########################################################
      SUBROUTINE CALCUVW
C#########################################################
C     This routine discretizes and solves the linearized
C     equations for X, Y and Z momentum componentS (U, V 
C     and W Cartesian velocity components).
C
C
C=========================================================
      implicit none
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      integer M,NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT
      integer K,J,I,IJK,II,IJB,IJP,IO,IW,ISY,IJN,MIJ
      integer LS
C
      real*8 GU,SB,APT,CP,CB,VISS,COEF,ARE,UPB,VPB,WPB
C NEWCOUPLED
      REAL*8 CPU,CBU,CPV,CBV,CPW,CBW
C END NEWCOUPLED
      real*8 VNP,XTP,YTP,ZTP,VISOL,YNP,ZNP,XNP,FDE,DPB
C NEW IMPORTANT CHANGE - CHECK ADDITIONAL SOURCE TERMS FALK
C      real*8  LPI,VISL
       real*8  LPI,VISLL
C END NEW
      parameter (LPI=3.141592653589793238462643383279d0,VISLL=1.d0) 
C NEW
C     real*8 tca,tcd,lden,tsg1,tsg2,tsg3,tsg4,tsg5,tsg6
C     REAL*8 TCA,LDEN,LMU
      Vec SUL,SVL,SWL,SPL,APL,
     *    UL,VL,WL,PL,
     *    DUXL,DUYL,DUZL,DVXL,DVYL,DVZL,
     *    DWXL,DWYL,DWZL,DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL
C NEWCOUPLED
      Vec UVWPL,SUVWPL
C END NEWCOUPLED
      PetscScalar PZERO,PONE
      PetscInt psize
      PetscErrorCode IERR
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C END NEW
C=========================================================
C NEW
      PZERO=0.0D0
      PONE=1.0D0
C END NEW
C
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
      IF(LCAL(IEN)) CALL VecGetArray(TVEC,TARR,TTI,IERR)
C END NEW
C
C.....CALCULATE GRADIENT VECTOR COMPONENTS AT CV-CENTER FOR U, V W, & P
C
C.....Call to Inner Walls FIX modifications routine
C
C NEW
C     CALL INNWALLFIX
C END NEW
C
C NEW
C NEWCOUPLED
      CALL VecGhostUpdateBegin(
     *     UVWPVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(
     *     UVWPVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecGetSubVector(UVWPL,ISU,UL,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VL,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WL,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PL,IERR)

      CALL GRADFI(UL,DUXVEC,DUYVEC,DUZVEC)
      CALL GRADFI(VL,DVXVEC,DVYVEC,DVZVEC)
      CALL GRADFI(WL,DWXVEC,DWYVEC,DWZVEC)
      CALL GRADFI(PL,DPXVEC,DPYVEC,DPZVEC)

C     CALL VecRestoreSubVector(UVWPL,ISP,PL,IERR)
C END NEWCOUPLED
C
C.....UPDATE DENSITY AND VISCOSITY
C
      CALL VecGhostUpdateBegin(
     *     DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
C.....GET ARRAY OF VECTOR VALUES: VELOCITY, GRADIENT COMPONENTS, DENS, VISC
C
      CALL VecGhostGetLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostGetLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostGetLocalForm(DUZVEC,DUZL,IERR)
      CALL VecGhostGetLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostGetLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostGetLocalForm(DVZVEC,DVZL,IERR)
      CALL VecGhostGetLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostGetLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostGetLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VISVEC,VISL,IERR)
C
      CALL VecGetArray(UL,UARR,UUI,IERR)
      CALL VecGetArray(VL,VARR,VVI,IERR)
      CALL VecGetArray(WL,WARR,WWI,IERR)
      CALL VecGetArray(PL,PARR,PPI,IERR)
C
      CALL VecGetArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecGetArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecGetArray(DUZL,DUZARR,DUZZI,IERR)
      CALL VecGetArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecGetArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecGetArray(DVZL,DVZARR,DVZZI,IERR)
      CALL VecGetArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecGetArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecGetArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VISL,VISARR,VISSI,IERR)

C
C NEWCOUPLED
      CALL VecGhostGetLocalForm(SUVWPVEC,SUVWPL,IERR)
      CALL VecGetSubVector(SUVWPL,ISU,SUL,IERR)
      CALL VecGetSubVector(SUVWPL,ISV,SVL,IERR)
      CALL VecGetSubVector(SUVWPL,ISW,SWL,IERR)
      CALL VecGhostGetLocalForm(APVEC,APL,IERR)
C END NEWCOUPLED
C
      CALL VecSet(SUL,PZERO,IERR)
      CALL VecSet(SVL,PZERO,IERR)
      CALL VecSet(SWL,PZERO,IERR)
      CALL VecSet(APL,PZERO,IERR)
C
      CALL VecGetArray(SUL,SUARR,SUUI,IERR)
      CALL VecGetArray(SVL,SVARR,SVVI,IERR)
      CALL VecGetArray(SWL,SWARR,SWWI,IERR)
      CALL VecGetArray(APL,APARR,APPI,IERR)
C END NEW
      AE(1:NIJKBKAL)=0.0D0; AW(1:NIJKBKAL)=0.0D0; 
      AN(1:NIJKBKAL)=0.0D0; AS(1:NIJKBKAL)=0.0D0
      AT(1:NIJKBKAL)=0.0D0; AB(1:NIJKBKAL)=0.0D0; 
      AL(1:NOCBKAL) =0.0D0; AR(1:NOCBKAL) =0.0D0
      GU=GDS(1)
C NEW
      AFL(1:NFSGBKAL)=0.0D0; AFR(1:NFSGBKAL)=0.0D0
C
      APU(1:NIJKBKAL)=0.0D0; 
      AWU(1:NIJKBKAL)=0.0D0; AEU(1:NIJKBKAL)=0.0D0;
      ANU(1:NIJKBKAL)=0.0D0; ASU(1:NIJKBKAL)=0.0D0;
      ATU(1:NIJKBKAL)=0.0D0; ABU(1:NIJKBKAL)=0.0D0;
      APV(1:NIJKBKAL)=0.0D0; 
      AWV(1:NIJKBKAL)=0.0D0; AEV(1:NIJKBKAL)=0.0D0;
      ANV(1:NIJKBKAL)=0.0D0; ASV(1:NIJKBKAL)=0.0D0;
      ATV(1:NIJKBKAL)=0.0D0; ABV(1:NIJKBKAL)=0.0D0;
      APW(1:NIJKBKAL)=0.0D0; 
      AWW(1:NIJKBKAL)=0.0D0; AEW(1:NIJKBKAL)=0.0D0;
      ANW(1:NIJKBKAL)=0.0D0; ASW(1:NIJKBKAL)=0.0D0;
      ATW(1:NIJKBKAL)=0.0D0; ABW(1:NIJKBKAL)=0.0D0;
C END NEW
C
C NEW
C
c.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT,
C$OMP*                    SB,APT)
C END NEW
      DO M=1,NBLKS
C
      NKMT=NKBK(M)-1
      NIMT=NIBK(M)-1
      NJMT=NJBK(M)-1
      KSTT=KBK(M)
      ISTT=IBK(M)
      NJT=NJMT+1
      NIJT=(NIMT+1)*NJT
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES: EAST
C
      DO K=2,NKMT
      DO I=2,NIMT-1
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        CALL FLUXUVW(IJK,IJK+NJT,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                           XER(IJK),YER(IJK),ZER(IJK),
     *               F1(IJK),AW(IJK+NJT), AE(IJK),FX(IJK),GU,M,
C NEWCOUPLED
     *                       AWU(IJK+NJT),AEU(IJK),
     *                       AWV(IJK+NJT),AEV(IJK),
     *                       AWW(IJK+NJT),AEW(IJK))
        APU(IJK)    =APU(IJK)    -AWU(IJK+NJT)
        APU(IJK+NJT)=APU(IJK+NJT)-AEU(IJK)
        APV(IJK)    =APV(IJK)    -AWV(IJK+NJT)
        APV(IJK+NJT)=APV(IJK+NJT)-AEV(IJK)
        APW(IJK)    =APW(IJK)    -AWW(IJK+NJT)
        APW(IJK+NJT)=APW(IJK+NJT)-AEW(IJK)
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES: NORTH
C
      DO K=2,NKMT
      DO I=2,NIMT
      DO J=2,NJMT-1
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        CALL FLUXUVW(IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                         XNR(IJK),YNR(IJK),ZNR(IJK),
     *               F2(IJK),AS(IJK+1), AN(IJK),FY(IJK),GU,M,
C NEWCOUPLED
     *                       ASU(IJK+1),ANU(IJK),
     *                       ASV(IJK+1),ANV(IJK),
     *                       ASW(IJK+1),ANW(IJK))
        APU(IJK)  =APU(IJK)  -ASU(IJK+1)
        APU(IJK+1)=APU(IJK+1)-ANU(IJK)
        APV(IJK)  =APV(IJK)  -ASV(IJK+1)
        APV(IJK+1)=APV(IJK+1)-ANV(IJK)
        APW(IJK)  =APW(IJK)  -ASW(IJK+1)
        APW(IJK+1)=APW(IJK+1)-ANW(IJK)
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES: TOP
C
      DO K=2,NKMT-1
      DO I=2,NIMT
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        CALL FLUXUVW(IJK,IJK+NIJT,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                            XTR(IJK),YTR(IJK),ZTR(IJK),
     *               F3(IJK),AB(IJK+NIJT), AT(IJK),FZ(IJK),GU,M,
C NEWCOUPLED
     *                       ABU(IJK+NIJT),ATU(IJK),
     *                       ABV(IJK+NIJT),ATV(IJK),
     *                       ABW(IJK+NIJT),ATW(IJK))
        APU(IJK)     =APU(IJK)     -ABU(IJK+NIJT)
        APU(IJK+NIJT)=APU(IJK+NIJT)-ATU(IJK)
        APV(IJK)     =APV(IJK)     -ABV(IJK+NIJT)
        APV(IJK+NIJT)=APV(IJK+NIJT)-ATV(IJK)
        APW(IJK)     =APW(IJK)     -ABW(IJK+NIJT)
        APW(IJK+NIJT)=APW(IJK+NIJT)-ATW(IJK)
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
C.....BUOYANCY SOURCE CONTRIBUTION
C
      IF(LCAL(IEN)) THEN
      DO K=2,NKMT
      DO I=2,NIMT
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        SB=-BETA*DEN(IJK)*VOL(IJK)*(T(IJK)-TREF)
        SU(IJK)=SU(IJK)+GRAVX*SB
        SV(IJK)=SV(IJK)+GRAVY*SB
        SW(IJK)=SW(IJK)+GRAVZ*SB
      END DO
      END DO
      END DO
      ENDIF
C NEW
C
C.....PRESSURE SOURCE CONTRIBUTION - NO LONGER NEEDED DUE TO IMPLICIT
C.....TREATMENT INSIDE FLUXUVW
C
C END NEW
C
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C%%%  falk START ADDITIONAL SOURCE TERMS DUE TO TAYLOR 3D-TESTCASE
#ifdef USE_ANALYTICAL
      DO K=2,NKMT
      DO I=2,NIMT
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
C NEW
        SU(IJK)=SU(IJK)+(LPI*(LPI*XC(IJK)*4.0D0-LPI**2*dexp(LPI*XC(IJK)*
     *  (1.0D0/2.0D0))+DCOS(LPI*XC(IJK))*DCOS(LPI*YC(IJK))**2*
     *  DSIN(LPI*XC(IJK))*32.0D0-DCOS(LPI*XC(IJK))*DCOS(LPI*ZC(IJK))**2*
     *  DSIN(LPI*XC(IJK))*16.0D0-LPI*XC(IJK)*DCOS(XC(IJK)**2+YC(IJK)**2+
     *  ZC(IJK)**2)**2*8.0D0+LPI*DCOS(LPI*YC(IJK))*DCOS(LPI*ZC(IJK))*
     *  DSIN(LPI*XC(IJK))*96.0D0)*(1.0D0/64.0D0))*VOL(IJK)

        SV(IJK)=SV(IJK)+(LPI*(LPI*YC(IJK)*4.0D0-LPI**2*DEXP(LPI*YC(IJK)*
     *  (1.0D0/2.0D0))+DCOS(LPI*XC(IJK))**2*DCOS(LPI*YC(IJK))*
     *  DSIN(LPI*YC(IJK))*32.0D0-DCOS(LPI*YC(IJK))*DCOS(LPI*ZC(IJK))**2*
     *  DSIN(LPI*YC(IJK))*16.0D0-LPI*YC(IJK)*DCOS(XC(IJK)**2+YC(IJK)**2+
     *  ZC(IJK)**2)**2*8.0D0+LPI*DCOS(LPI*XC(IJK))*DCOS(LPI*ZC(IJK))*
     *  DSIN(LPI*YC(IJK))*96.0D0)*(1.0D0/64.0D0))*VOL(IJK)

        SW(IJK)=SW(IJK)+(LPI*(LPI*ZC(IJK)*4.0D0-LPI**2*DEXP(LPI*ZC(IJK)*
     *  (1.0D0/2.0D0))+DCOS(LPI*XC(IJK))**2*DCOS(LPI*ZC(IJK))*
     *  DSIN(LPI*ZC(IJK))*32.0D0+DCOS(LPI*YC(IJK))**2*DCOS(LPI*ZC(IJK))*
     *  DSIN(LPI*ZC(IJK))*32.0D0-LPI*ZC(IJK)*DCOS(XC(IJK)**2+YC(IJK)**2+
     *  ZC(IJK)**2)**2*8.0D0-LPI*DCOS(LPI*XC(IJK))*DCOS(LPI*YC(IJK))*
     *  DSIN(LPI*ZC(IJK))*192.0D0)*(1.0D0/64.0D0))*VOL(IJK)
C END NEW
      END DO
      END DO
      END DO
#endif
c
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C
C.....UNSTEADY TERM CONTRIBUTION (GAMT = 0 -> IMPLICIT EULER;
C.....GAMT = 1 -> THREE TIME LEVELS; BLENDING POSSIBLE)
C
      IF(LTIME) THEN
      DO K=2,NKMT
      DO I=2,NIMT
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        APT=DEN(IJK)*VOL(IJK)*DTR
        SU(IJK)=SU(IJK)+APT*((1.0D0+GAMT)*UO(IJK)-0.5D0*GAMT*UOO(IJK))
        SV(IJK)=SV(IJK)+APT*((1.0D0+GAMT)*VO(IJK)-0.5D0*GAMT*VOO(IJK))
        SW(IJK)=SW(IJK)+APT*((1.0D0+GAMT)*WO(IJK)-0.5D0*GAMT*WOO(IJK))
        AP(IJK)=AP(IJK)+APT*(1.0D0+0.5D0*GAMT)
      END DO
      END DO
      END DO
      ENDIF
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
C.....VELOCITY INLET BOUNDARIES (CONSTANT GRADIENT BETWEEN BOUNDARY & CV-CENTER ASSUMED)
C
      DO II=1,NINLBKAL
        IJP=IJPI(II)
        IJB=IJI(II)
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
C.......EXTRAPOLATE PRESSURE TO BOUNDARY
        P(IJB)=P(IJP)
        CALL FLUXUVW(IJP,IJB,XIC(II),YIC(II),ZIC(II),
     *                       XIR(II),YIR(II),ZIR(II),
     *               FMI(II),CP, CB,ONE,ZERO,1,
C NEWCOUPLED
     *                       CPU,CBU,
     *                       CPV,CBV,
     *                       CPW,CBW)
        AP(IJP)=AP(IJP)-CB
        APU(IJP)=APU(IJP)+CBU
        APV(IJP)=APV(IJP)+CBV
        APW(IJP)=APW(IJP)+CBW
        SU(IJP)=SU(IJP)-CB*U(IJB)
        SV(IJP)=SV(IJP)-CB*V(IJB)
        SW(IJP)=SW(IJP)-CB*W(IJB)
      END DO
C
C.....PRESSURE INLET BOUNDARIES / OUTLET (CONSTANT GRADIENT BETWEEN BOUNDARY & CV-CENTER ASSUMED)
C
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
C.......EXTRAPOLATE PRESSURE TO BOUNDARY
        CALL FLUXUVW(IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                       XUR(IO),YOR(IO),ZOR(IO),
     *               FMO(IO),CP,CB,ONE,ZERO,1,
C NEWCOUPLED
     *                       CPU,CBU,
     *                       CPV,CBV,
     *                       CPW,CBW)
        AP(IJP)=AP(IJP)-CB
        APU(IJP)=APU(IJP)+CBU
        APV(IJP)=APV(IJP)+CBV
        APW(IJP)=APW(IJP)+CBW
        SU(IJP)=SU(IJP)-CB*U(IJB)-P(IJB)*XUR(IO)
        SV(IJP)=SV(IJP)-CB*V(IJB)-P(IJB)*YOR(IO)
        SW(IJP)=SW(IJP)-CB*W(IJB)-P(IJB)*ZOR(IO)
      END DO
C
C#include "boundaries.f"
C
C END NEWCOUPLED
C
C.....RESTORE ARRAYS THAT ARE NOT NEEDED ANYMORE
C
      CALL VecRestoreArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecRestoreArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecRestoreArray(DUZL,DUZARR,DUZZI,IERR)
C
      CALL VecRestoreArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecRestoreArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecRestoreArray(DVZL,DVZARR,DVZZI,IERR)
C
      CALL VecRestoreArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecRestoreArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecRestoreArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VISL,VISARR,VISSI,IERR)
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
      IF(LCAL(IEN)) CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecGhostRestoreLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostRestoreLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostRestoreLocalForm(DUZVEC,DUZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostRestoreLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostRestoreLocalForm(DVZVEC,DVZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostRestoreLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostRestoreLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VISVEC,VISL,IERR)
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecRestoreArray(APL,APARR,APPI,IERR)
C END NEWCOUPLED
      CALL VecGhostRestoreLocalForm(APVEC,APL,IERR)
C
      CALL VecGhostUpdateBegin(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(  APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C.....GET LOCAL PART OF AP,SU,APR
C
      CALL VecGetArray(APVEC,APARR,APPI,IERR)
C     CALL VecGetArray(SUVEC,SUARR,SUUI,IERR)
      CALL VecGetArray(APRVEC,APRARR,APRRI,IERR)
C
C.....FINAL COEFFICIENT AND SOURCES MATRIX FOR U-EQUATION
C
C.....OpenMP : Here starts parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED)
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*            NKMT,NIMT,NJMT,KSTT,ISTT)
C END NEW
C
      DO M=1,NBLKS
C
      NKMT=NKBK(M)-1
      NIMT=NIBK(M)-1
      NJMT=NJBK(M)-1
      KSTT=KBK(M)
      ISTT=IBK(M)
C
      DO K=2,NKMT 
      DO I=2,NIMT
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
C NEWCOUPLED - NO UNDERRELAXATION NEEDED
        AP(IJK)=AP(IJK)-AE(IJK)-AW(IJK)-AN(IJK)-AS(IJK)
     *                   -AT(IJK)-AB(IJK)
C       SU(IJK)=SU(IJK)+(1.0D0-URF(IU))*AP(IJK)*U(IJK)
C       SV(IJK)=SV(IJK)+(1.0D0-URF(IV))*AP(IJK)*V(IJK)
C       SW(IJK)=SW(IJK)+(1.0D0-URF(IWW))*AP(IJK)*W(IJK)
        APR(IJK)=1.0D0/(AP(IJK)+SMALL)
C END NEWCOUPLED
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C NEW
C
C.....UPDATE SOURCE AND DIAGONAL VECTOR ON ALL PROCESSORS, ASSEMBLE MATRIX
C
      CALL VecRestoreArray(SUL,SUARR,SUUI,IERR)
      CALL VecRestoreArray(SVL,SVARR,SVVI,IERR)
      CALL VecRestoreArray(SWL,SWARR,SWWI,IERR)
C
C NEWCOUPLED - WAIT UNTIL PRESSURE SOURCE TERM HAS BEEN CALCULATED
      CALL VecRestoreSubVector(SUVWPL,ISU,SUL,IERR)
      CALL VecRestoreSubVector(SUVWPL,ISV,SVL,IERR)
      CALL VecRestoreSubVector(SUVWPL,ISW,SWL,IERR)
      CALL VecGhostRestoreLocalForm(SUVWPVEC,SUVWPL,IERR)
C
      CALL VecRestoreArray(UL,UARR,UUI,IERR)
      CALL VecRestoreArray(VL,VARR,VVI,IERR)
      CALL VecRestoreArray(WL,WARR,WWI,IERR)
      CALL VecRestoreArray(PL,PARR,PPI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PL,IERR)
C
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecRestoreArray(APVEC,APARR,APPI,IERR)
      CALL VecRestoreArray(APRVEC,APRARR,APRRI,IERR)
C NEWCOUPLED
      CALL ASSEMBLESYSUVW(AMAT)
C END NEWCOUPLED
C
C
      RETURN
      END 
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C################################################################
      SUBROUTINE FLUXUVW(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                   FM,CAP,CAN,FAC,G,MB,
C NEW
     *                      CAPU,CANU,
     *                      CAPV,CANV,
     *                      CAPW,CANW)
C END NEW
C################################################################
C     This routine calculates momentum fluxes (convective and
C     diffusive) through the cell face between nodes IJP and IJN. 
C     IJ1 and IJ2 are the indices of CV corners defining the cell 
C     face. FM is the mass flux through the face, and FAC is the 
C     interpolation factor (distance from node IJP to cell face 
C     center over the sum of this distance and the distance from 
C     cell face center to node IJN). CAP and CAN are the 
C     contributions to matrix coefficients in the momentum
C     equations at nodes IJP and IJN. Diffusive fluxes are
C     discretized using central differences; for convective
C     fluxes, linear interpolation can be blended with upwind
C     approximation; see Sect. 8.6 for details. Note: cell
C     face surface vector is directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C     
C     CAP{U,V,W} and CAN{U,V,W} are the coefficients due to the
C     implicit consideration of the pressure gradient. They are
C     calculated according to darwish 2009.
C
C==============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "coef3d.inc"
#include "rcont3d.inc"
#include "propcell3d.inc"
C
      INTEGER IJN,IJP,MB,M
      INTEGER LS
C
      REAL*8 FM,FACP,FAC,VISI,DENI,ZZC,YYC,XXC
      REAL*8 YCR,ZCR,XCR,FMI,FMX,DUXI,DVXI,DWXI
      REAL*8 DUYI,DVYI,DWYI,DUZI,DVZI,DWZI,XI,YI,ZI
      REAL*8 UI,VI,WI,XPN,ZPN,YPN,VSOL,FCUE
      REAL*8 FCWE,FDUE,FDVE,FDWE,FCUI,FCVI,FCWI,FDWI
      REAL*8 CAN,CAP,FUC,G,FVC,SUM,FCVE,FDUI,FDVI,FWC 
C NEWCOUPLED
      REAL*8 CAPU,CANU,CAPV,CANV,CAPW,CANW
C END NEWCOUPLED
C
C NEW
#include "petsc.user.inc"
C END NEW
C=========================================================
C
C.....INTERPOLATE ALONG LINE P-N
C
      FACP=1.0D0-FAC
      VISI=VIS(IJN)*FAC+VIS(IJP)*FACP
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....COMPUTE FM CORRECTED FOR RELATIVE MOVEMENT
C
c$$$      FM=FM-DENI*((WROTY(MB)*ZZC-WROTZ(MB)*YYC)*XCR+
c$$$     *            (WROTZ(MB)*XXC-WROTX(MB)*ZZC)*YCR+
c$$$     *            (WROTX(MB)*YYC-WROTY(MB)*XXC)*ZCR)
      FMI=MIN(FM,ZERO)
      FMX=MAX(FM,ZERO)
C
C.....INTERPOLATE ALONG LINE P-N
C
      DUXI=DUX(IJN)*FAC+DUX(IJP)*FACP
      DVXI=DVX(IJN)*FAC+DVX(IJP)*FACP
      DWXI=DWX(IJN)*FAC+DWX(IJP)*FACP
      DUYI=DUY(IJN)*FAC+DUY(IJP)*FACP
      DVYI=DVY(IJN)*FAC+DVY(IJP)*FACP
      DWYI=DWY(IJN)*FAC+DWY(IJP)*FACP
      DUZI=DUZ(IJN)*FAC+DUZ(IJP)*FACP
      DVZI=DVZ(IJN)*FAC+DVZ(IJP)*FACP
      DWZI=DWZ(IJN)*FAC+DWZ(IJP)*FACP

      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
C
C.....CALCULATE CELL-FACE VELOCITIES AND VISCOSITY
C
      UI=U(IJN)*FAC+U(IJP)*FACP+DUXI*(XXC-XI)+DUYI*(YYC-YI)+
     *                          DUZI*(ZZC-ZI)
      VI=V(IJN)*FAC+V(IJP)*FACP+DVXI*(XXC-XI)+DVYI*(YYC-YI)+
     *                          DVZI*(ZZC-ZI)
      WI=W(IJN)*FAC+W(IJP)*FACP+DWXI*(XXC-XI)+DWYI*(YYC-YI)+
     *                          DWZI*(ZZC-ZI)
C
C.....DISTANCE VECTOR COMPONENTS, DIFFUSION COEFFICIENT
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      VSOL=VISI*DSQRT((XCR**2+YCR**2+ZCR**2)/
     *               (XPN**2+YPN**2+ZPN**2))
C
C.....EXPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCUE=FM*UI
      FCVE=FM*VI
      FCWE=FM*WI
C.....projektion auf den normalenvektor n
      FDUE=VISI*((DUXI+DUXI)*XCR+(DUYI+DVXI)*YCR+(DUZI+DWXI)*ZCR)
      FDVE=VISI*((DVXI+DUYI)*XCR+(DVYI+DVYI)*YCR+(DVZI+DWYI)*ZCR)
      FDWE=VISI*((DWXI+DUZI)*XCR+(DWYI+DVZI)*YCR+(DWZI+DWZI)*ZCR)
C
C.....IMPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCUI=FMI*U(IJN)+FMX*U(IJP)
      FCVI=FMI*V(IJN)+FMX*V(IJP)
      FCWI=FMI*W(IJN)+FMX*W(IJP)
C.....projektion auf den verbindungsvektor xi
      FDUI=VSOL*(DUXI*XPN+DUYI*YPN+DUZI*ZPN)
      FDVI=VSOL*(DVXI*XPN+DVYI*YPN+DVZI*ZPN)
      FDWI=VSOL*(DWXI*XPN+DWYI*YPN+DWZI*ZPN)
C
C.....COEFFICIENTS, DEFERRED CORRECTION, SOURCE TERMS
C
      CAP=-VSOL-FMX
      CAN=-VSOL+FMI
C NEWCOUPLED
C
C.....IMPLICIT TREATMENT OF PRESSURE GRADIENT
C
      CAPU=-FACP*XCR
      CANU=FAC*XCR
      CAPV=-FACP*YCR
      CANV=FAC*YCR
      CAPW=-FACP*ZCR
      CANW=FAC*ZCR
C END NEWCOUPLED
      FUC=G*(FCUE-FCUI)
      FVC=G*(FCVE-FCVI)
      FWC=G*(FCWE-FCWI)
C
      SU(IJP)=SU(IJP)-FUC+FDUE-FDUI
      SV(IJP)=SV(IJP)-FVC+FDVE-FDVI
      SW(IJP)=SW(IJP)-FWC+FDWE-FDWI
      SU(IJN)=SU(IJN)+FUC-FDUE+FDUI
      SV(IJN)=SV(IJN)+FVC-FDVE+FDVI
      SW(IJN)=SW(IJN)+FWC-FDWE+FDWI
C
      RETURN
C
C
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C############################################################## 
      SUBROUTINE CALCP(INTMF)
C############################################################## 
C     This routine assembles and solves the pressure-correction
C     equation using colocated grid. SIMPLE algorithm with one
C     corrector step (non-orthogonality effects neglected) is
C     applied.
C
C==============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
c
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "grad3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "varold3d.inc"
c
      INTEGER M,NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT
      INTEGER I,J,K,IJK,IJP,IJN,II,INTMF,IO,LC,ISY
C     INTEGER IJB
      INTEGER LS
      INTEGER IJB,IJK1,IJK2,IJK3,IJK4
C
      REAL*8 SUM,FLOWO,PPO,UN,FMCOR,DX12,DY12,DZ12,DX13,DY13,DZ13
      REAL*8 DX14,DY14,DZ14,S23,S34,FLOMON,XR23,YR23,ZR23
      REAL*8 XR34,YR34,ZR34,XNV,YNV,ZNV,FAC
C NEW
      REAL*8 DENI
      REAL*8 CP,CB
      Vec SUL,APL,APRL,PL,PPL,
     *    UL,VL,WL,
     *    DUXL,DUYL,DUZL,DVXL,DVYL,DVZL,
     *    DWXL,DWYL,DWZL,DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL,
C NEWCOUPLED
     *    UVWPL,SUVWPL,SPL
C END NEWCOUPLED
      PetscScalar PZERO,PONE,ZEROSC
      PetscErrorCode IERR
      PetscInt ZEROPP,RANK
      COMMON /CORRECTOR/ LC
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C END NEW
C==============================================================
C
C.....INITIALIZE SUM AND COEFs
C
      SUM=0.0D0
      PZERO=0.0D0
      PONE=1.0D0
C NEW
C     SU(1:NXYZA)=0.; AP(1:NXYZA)=0.; 
C NEWCOUPLED
      CALL VecGhostGetLocalForm(SUVWPVEC,SUVWPL,IERR)
      CALL VecGetSubVector(SUVWPL,ISP,SPL,IERR)
C END NEWCOUPLED
      CALL VecGhostGetLocalForm(APVEC,APL,IERR)
C
      CALL VecSet(SPL,PZERO,IERR)
      CALL VecSet(APL,PZERO,IERR)
C
      CALL VecGetArray(SPL,SPARR,SPPI,IERR)
C     CALL VecRestoreArray(SPL,SPARR,SPPI,IERR)
C     CALL VecRestoreSubVector(SUVWPL,ISP,SPL,IERR)
C     CALL VecGhostRestoreLocalForm(SUVWPVEC,SUVWPL,IERR)
      CALL VecGetArray(APL,APARR,APPI,IERR)
C END NEW
                                                  F1(1:NIJKBKAL)=0.0D0
      AE(1:NIJKBKAL)=0.0D0; AW(1:NIJKBKAL)=0.0D0; F2(1:NIJKBKAL)=0.0D0
      AN(1:NIJKBKAL)=0.0D0; AS(1:NIJKBKAL)=0.0D0; F3(1:NIJKBKAL)=0.0D0
      AT(1:NIJKBKAL)=0.0D0; AB(1:NIJKBKAL)=0.0D0; FMOC(1:NOCBKAL)=0.0D0
      AL(1:NOCBKAL)=0.0D0; AR(1:NOCBKAL)=0.0D0 
      APU(1:NIJKBKAL)=0.0D0; 
      APV(1:NIJKBKAL)=0.0D0; 
      APW(1:NIJKBKAL)=0.0D0; 
C NEW
      AFL(1:NFSGBKAL)=0.0D0; AFR(1:NFSGBKAL)=0.0D0
C
C.....GET ARRAY OF VECTOR VALUES (without update)
C
      CALL VecGhostGetLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostGetLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostGetLocalForm(DUZVEC,DUZL,IERR)
C
      CALL VecGhostGetLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostGetLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostGetLocalForm(DVZVEC,DVZL,IERR)
C
      CALL VecGhostGetLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostGetLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostGetLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecGetArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecGetArray(DUZL,DUZARR,DUZZI,IERR)
C
      CALL VecGetArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecGetArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecGetArray(DVZL,DVZARR,DVZZI,IERR)
C
      CALL VecGetArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecGetArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecGetArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
C
C.....GET ARRAY OF VECTOR VALUES
C
      CALL VecGhostUpdateBegin(
     *     APRVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(APRVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
C NEWCOUPLED
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecGetSubVector(UVWPL,ISU,UL,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VL,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WL,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PL,IERR)
C END NEWCOUPLED
      CALL VecGhostGetLocalForm(APRVEC,APRL,IERR)
C
      CALL VecGetArray(UL,UARR,UUI,IERR)
      CALL VecGetArray(VL,VARR,VVI,IERR)
      CALL VecGetArray(WL,WARR,WWI,IERR)
      CALL VecGetArray(PL,PARR,PPI,IERR)
      CALL VecGetArray(APRL,APRARR,APRRI,IERR)
C
C END NEW
C
C.....Call to Inner Walls FIX modifications routine
C
C NEW
C     CALL INNWALLFIX
C
C.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT)
C END NEW
      DO M=1,NBLKS
C
      NKMT=NKBK(M)-1
      NIMT=NIBK(M)-1
      NJMT=NJBK(M)-1
      KSTT=KBK(M)
      ISTT=IBK(M)
      NJT=NJMT+1
      NIJT=(NIMT+1)*NJT
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES (EAST, NORTH & TOP)
C
      DO K=2,NKMT 
      DO I=2,NIMT-1
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        CALL FLUXP(IJK,IJK+NJT,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                         XER(IJK),YER(IJK),ZER(IJK),
     *                        AW(IJK+NJT),AE(IJK),FX(IJK))
        DENI=DEN(IJK+NJT)*FX(IJK)+DEN(IJK)*(1.0D0-FX(IJK))
        AEU(IJK)    =AEU(IJK)    *DENI
        AWU(IJK+NJT)=AWU(IJK+NJT)*DENI
        AEV(IJK)    =AEV(IJK)    *DENI
        AWV(IJK+NJT)=AWV(IJK+NJT)*DENI
        AEW(IJK)    =AEW(IJK)    *DENI
        AWW(IJK+NJT)=AWW(IJK+NJT)*DENI
      END DO
      END DO
      END DO
C
      DO K=2,NKMT
      DO I=2,NIMT
      DO J=2,NJMT-1
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        CALL FLUXP(IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                       XNR(IJK),YNR(IJK),ZNR(IJK),
     *                     AS(IJK+1),AN(IJK),FY(IJK))
        DENI=DEN(IJK+1)*FY(IJK)+DEN(IJK)*(1.0D0-FY(IJK))
        APU(IJK)  =APU(IJK)  *DENI
        APU(IJK+1)=APU(IJK+1)*DENI
        APV(IJK)  =APV(IJK)  *DENI
        APV(IJK+1)=APV(IJK+1)*DENI
        APW(IJK)  =APW(IJK)  *DENI
        APW(IJK+1)=APW(IJK+1)*DENI
      END DO
      END DO
      END DO
C
      DO K=2,NKMT-1
      DO I=2,NIMT
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        CALL FLUXP(IJK,IJK+NIJT,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                          XTR(IJK),YTR(IJK),ZTR(IJK),
     *                     AB(IJK+NIJT),AT(IJK),FZ(IJK))
        DENI=DEN(IJK+NIJT)*FZ(IJK)+DEN(IJK)*(1.0D0-FZ(IJK))
        APU(IJK)     =APU(IJK)     *DENI
        APU(IJK+NIJT)=APU(IJK+NIJT)*DENI
        APV(IJK)     =APV(IJK)     *DENI
        APV(IJK+NIJT)=APV(IJK+NIJT)*DENI
        APW(IJK)     =APW(IJK)     *DENI
        APW(IJK+NIJT)=APW(IJK+NIJT)*DENI
      END DO
      END DO
      END DO
C
      END DO
c.....OpenMP : Here ends this parallel loop section
C
C.....O- AND C-GRID CUTS
C    
C     DO I=1,NOCBKAL
C       IJP=IJLPBK(I)
C       IJN=IJRPBK(I)
C       CALL FLUXP(IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
C    *                     XOCR(I),YOCR(I),ZOCR(I),
C    *                     AL(I),AR(I),FOCBK(I))
C       AP(IJP)=AP(IJP)-AR(I)
C       AP(IJN)=AP(IJN)-AL(I)
C       SP(IJP)=SP(IJP)-FMOC(I)
C       SP(IJN)=SP(IJN)+FMOC(I)
C     END DO
C NEW
C
C.....FACE SEGMENTS
C    
C     DO I=1,NFSGBKAL
C       IJP=IJFL(I)
C       IJN=IJFR(I)
C       CALL FLUXP(IJP,IJN,XFC(I),YFC(I),ZFC(I),
C    *                     XFR(I),YFR(I),ZFR(I),
C    *                     AFL(I),AFR(I),FFSGBK(I))
C       AP(IJP)=AP(IJP)-AFR(I)
C       AP(IJN)=AP(IJN)-AFL(I)
C       SP(IJP)=SP(IJP)-FMF(I)
C       SP(IJN)=SP(IJN)+FMF(I)
C     END DO
C
C.....INLET BOUNDARIES
C    
      DO II=1,NINLBKAL
        SP(IJPI(II))=SP(IJPI(II))-FMI(II)
      END DO
C
C.....PRESSURE INLET BOUNDARIES / OUTLET (CONSTANT GRADIENT BETWEEN BOUNDARY & CV-CENTER ASSUMED)
C
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
        DUX(IJB)=DUX(IJP)
        DUY(IJB)=DUY(IJP)
        DUZ(IJB)=DUZ(IJP)
        DVX(IJB)=DVX(IJP)
        DVY(IJB)=DVY(IJP)
        DVZ(IJB)=DVZ(IJP)
        DWX(IJB)=DWX(IJP)
        DWY(IJB)=DWY(IJP)
        DWZ(IJB)=DWZ(IJP)
        CALL FLUXP(IJP,IJB,XOC(IJK),YOC(IJK),ZOC(IJK),
     *                     XUR(IJK),YOR(IJK),ZOR(IJK),
     *                     CP,CB,FMO(IO))
        AP(IJP)=AP(IJP)-CB
        APU(IJP)=APU(IJP)+XUR(IO)
        APV(IJP)=APV(IJP)+YOR(IO)
        APW(IJP)=APW(IJP)+ZOR(IO)
        SP(IJP) =SP(IJP)-CB*P(IJB)
      END DO
C.....OpenMP : Start parallel loop section
C$OMP PARALLEL DO DEFAULT(SHARED) 
C$OMP*            PRIVATE(M,K,I,J,IJK,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT),
C$OMP*            REDUCTION(+:SUM)
C END NEW
      DO M=1,NBLKS
C
      NKMT=NKBK(M)-1
      NIMT=NIBK(M)-1
      NJMT=NJBK(M)-1
      KSTT=KBK(M)
      ISTT=IBK(M)
      NJT=NJMT+1
      NIJT=(NIMT+1)*NJT
C
C.....SOURCE TERM AND CENTRAL COEFFICIENT 
C
      DO K=2,NKMT
      DO I=2,NIMT
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        AP(IJK)=AP(IJK)-(AE(IJK)+AW(IJK)+AN(IJK)+AS(IJK)
     *                  +AT(IJK)+AB(IJK))
        APU(IJK)=AEU(IJK)+AWU(IJK)+ANU(IJK)+ASU(IJK)
     *                   +ATU(IJK)+ABU(IJK)
        APV(IJK)=AEV(IJK)+AWV(IJK)+ANV(IJK)+ASV(IJK)
     *                   +ATV(IJK)+ABV(IJK)
        APW(IJK)=AEW(IJK)+AWW(IJK)+ANW(IJK)+ASW(IJK)
     *                   +ATW(IJK)+ABW(IJK)
      END DO
      END DO
      END DO
C
      END DO
C.....OpenMP : Here ends this parallel loop section
C
      CALL VecRestoreArray(SPL,SPARR,SPPI,IERR)
      CALL VecRestoreArray(APL,APARR,APPI,IERR)
C
C.....ASSEMBLE P-SUBMATRIX
C
      CALL VecGhostRestoreLocalForm(APVEC,APL,IERR)
      CALL VecGhostUpdateBegin(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL ASSEMBLESYSP(AMAT)

      CALL VecRestoreSubVector(SUVWPL,ISP,SPL,IERR)
      CALL VecGhostRestoreLocalForm(SUVWPVEC,SUVWPL,IERR)
C
      CALL VecGhostUpdateBegin(SUVWPVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(SUVWPVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C.....RESTORE ARRAYS
C
      CALL VecRestoreArray(UL,UARR,UUI,IERR)
      CALL VecRestoreArray(VL,VARR,VVI,IERR)
      CALL VecRestoreArray(WL,WARR,WWI,IERR)
      CALL VecRestoreArray(PL,PARR,PPI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PL,IERR)
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
      CALL VecRestoreArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecRestoreArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecRestoreArray(DUZL,DUZARR,DUZZI,IERR)
C
      CALL VecRestoreArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecRestoreArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecRestoreArray(DVZL,DVZARR,DVZZI,IERR)
C
      CALL VecRestoreArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecRestoreArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecRestoreArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGhostRestoreLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostRestoreLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostRestoreLocalForm(DUZVEC,DUZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostRestoreLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostRestoreLocalForm(DVZVEC,DVZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostRestoreLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostRestoreLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(APRL,APRARR,APRRI,IERR)
C
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(APRVEC,APRL,IERR)
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
C
C.....SOLVING COUPLED SYSTEM
C
      CALL MatZeroRowsColumns(
     *     AMAT,NZERO,ZEROS,PONE,UVWPVEC,SUVWPVEC,IERR)
C
      CALL PetscLogStagePush(STAGE1,IERR)
      CALL SOLVESYS(UVWPVEC,IU,AMAT,SUVWPVEC)
      CALL PetscLogStagePop(IERR)
C
C END NEW
C
C NECESSARY IF NOT USING PC REDISTRIBUTE; KSP CHANGES BOUNDARY VALUES
      CALL BCIN
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C NEW COUPLED
C#########################################################
      SUBROUTINE MASSFLUX
C#########################################################
C     This routine discretizes and solves the linearized
C     equations for X, Y and Z momentum componentS (U, V 
C     and W Cartesian velocity components).
C
C
C=========================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      INTEGER M,NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT
      INTEGER K,J,I,IJK,II,IJB,IJP,IO,IW,ISY,IJN,MIJ
      INTEGER LS
C
      REAL*8 GU,SB,APT,CP,CB,VISS,COEF,ARE,UPB,VPB,WPB
      REAL*8 CPU,CBU,CPV,CBV,CPW,CBW
      REAL*8 VNP,XTP,YTP,ZTP,VISOL,YNP,ZNP,XNP,FDE,DPB
      REAL*8  LPI,VISLL
      PARAMETER (LPI=3.141592653589793238462643383279D0,VISLL=1.D0) 
C
      Vec APL,
     *    UL,VL,WL,PL,
     *    DUXL,DUYL,DUZL,DVXL,DVYL,DVZL,
     *    DWXL,DWYL,DWZL,DPXL,DPYL,DPZL,
     *    DENL,VISL,VOLL,
     *    XCL,YCL,ZCL
      Vec UVWPL,SUVWPL,SPL
      PetscScalar PZERO,PONE
      PetscInt psize
      PetscErrorCode IERR
      COMMON /OUTER/ LS
C
#include "petsc.user.inc"
C
C==============================================================
C
      CALL VecGhostGetLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostGetLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostGetLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostGetLocalForm(ZCVEC,ZCL,IERR)
C
      CALL VecGetArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCL,XCARR,XCCI,IERR)
      CALL VecGetArray(YCL,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCL,ZCARR,ZCCI,IERR)
C
C......UPDATE VELOCITY
C
      CALL VecGhostUpdateBegin(
     *     UVWPVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(
     *     UVWPVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostGetLocalForm(UVWPVEC,UVWPL,IERR)
      CALL VecGetSubVector(UVWPL,ISU,UL,IERR)
      CALL VecGetSubVector(UVWPL,ISV,VL,IERR)
      CALL VecGetSubVector(UVWPL,ISW,WL,IERR)
      CALL VecGetSubVector(UVWPL,ISP,PL,IERR)
C
C......UPDATE GRADIENTS
C
      CALL GRADFI(UL,DUXVEC,DUYVEC,DUZVEC)
      CALL GRADFI(VL,DVXVEC,DVYVEC,DVZVEC)
      CALL GRADFI(WL,DWXVEC,DWYVEC,DWZVEC)
      CALL GRADFI(PL,DPXVEC,DPYVEC,DPZVEC)
C
C.....UPDATE DENSITY AND VISCOSITY
C
      CALL VecGhostUpdateBegin(
     *     DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
C
C.....GET ARRAY OF VECTOR VALUES: VELOCITY, GRADIENT COMPONENTS, DENS, VISC
C
      CALL VecGhostGetLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostGetLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostGetLocalForm(DUZVEC,DUZL,IERR)
      CALL VecGhostGetLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostGetLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostGetLocalForm(DVZVEC,DVZL,IERR)
      CALL VecGhostGetLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostGetLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostGetLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VISVEC,VISL,IERR)
C
      CALL VecGetArray(UL,UARR,UUI,IERR)
      CALL VecGetArray(VL,VARR,VVI,IERR)
      CALL VecGetArray(WL,WARR,WWI,IERR)
      CALL VecGetArray(PL,PARR,PPI,IERR)
C
      CALL VecGetArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecGetArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecGetArray(DUZL,DUZARR,DUZZI,IERR)
      CALL VecGetArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecGetArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecGetArray(DVZL,DVZARR,DVZZI,IERR)
      CALL VecGetArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecGetArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecGetArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VISL,VISARR,VISSI,IERR)
C
C......CACLULATE MASS FLUXES
C
      DO M=1,NBLKS
C
      NKMT=NKBK(M)-1
      NIMT=NIBK(M)-1
      NJMT=NJBK(M)-1
      KSTT=KBK(M)
      ISTT=IBK(M)
      NJT=NJMT+1
      NIJT=(NIMT+1)*NJT
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES (EAST, NORTH & TOP)
C
      DO K=2,NKMT 
      DO I=2,NIMT-1
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        CALL FLUXM(IJK,IJK+NJT,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                         XER(IJK),YER(IJK),ZER(IJK),
     *             F1(IJK),FX(IJK))
      END DO
      END DO
      END DO
C
      DO K=2,NKMT
      DO I=2,NIMT
      DO J=2,NJMT-1
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        CALL FLUXM(IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                       XNR(IJK),YNR(IJK),ZNR(IJK),
     *             F2(IJK),FY(IJK))
      END DO
      END DO
      END DO
C
      DO K=2,NKMT-1
      DO I=2,NIMT
      DO J=2,NJMT
        IJK=LKBK(K+KSTT)+LIBK(I+ISTT)+J
        CALL FLUXM(IJK,IJK+NIJT,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                          XTR(IJK),YTR(IJK),ZTR(IJK),
     *             F3(IJK),FZ(IJK))
      END DO
      END DO
      END DO
C
      END DO
c.....OpenMP : Here ends this parallel loop section
C
C.....O- AND C-GRID CUTS
C    
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        CALL FLUXM(IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                     XOCR(I),YOCR(I),ZOCR(I),
     *             FMOC(I),FOCBK(I))
      END DO
C NEW
C
C.....FACE SEGMENTS
C    
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
        CALL FLUXM(IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                     XFR(I),YFR(I),ZFR(I),
     *             FMF(I),FFSGBK(I))
      END DO
C
C......RESTORE ARRAYS
C
      CALL VecRestoreArray(UL,UARR,UUI,IERR)
      CALL VecRestoreArray(VL,VARR,VVI,IERR)
      CALL VecRestoreArray(WL,WARR,WWI,IERR)
      CALL VecRestoreArray(PL,PARR,PPI,IERR)
C
      CALL VecRestoreSubVector(UVWPL,ISU,UL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISV,VL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISW,WL,IERR)
      CALL VecRestoreSubVector(UVWPL,ISP,PL,IERR)
      CALL VecGhostRestoreLocalForm(UVWPVEC,UVWPL,IERR)
C
      CALL VecRestoreArray(DUXL,DUXARR,DUXXI,IERR)
      CALL VecRestoreArray(DUYL,DUYARR,DUYYI,IERR)
      CALL VecRestoreArray(DUZL,DUZARR,DUZZI,IERR)
      CALL VecRestoreArray(DVXL,DVXARR,DVXXI,IERR)
      CALL VecRestoreArray(DVYL,DVYARR,DVYYI,IERR)
      CALL VecRestoreArray(DVZL,DVZARR,DVZZI,IERR)
      CALL VecRestoreArray(DWXL,DWXARR,DWXXI,IERR)
      CALL VecRestoreArray(DWYL,DWYARR,DWYYI,IERR)
      CALL VecRestoreArray(DWZL,DWZARR,DWZZI,IERR)
C
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VISL,VISARR,VISSI,IERR)
C
      CALL VecGhostRestoreLocalForm(DUXVEC,DUXL,IERR)
      CALL VecGhostRestoreLocalForm(DUYVEC,DUYL,IERR)
      CALL VecGhostRestoreLocalForm(DUZVEC,DUZL,IERR)
      CALL VecGhostRestoreLocalForm(DVXVEC,DVXL,IERR)
      CALL VecGhostRestoreLocalForm(DVYVEC,DVYL,IERR)
      CALL VecGhostRestoreLocalForm(DVZVEC,DVZL,IERR)
      CALL VecGhostRestoreLocalForm(DWXVEC,DWXL,IERR)
      CALL VecGhostRestoreLocalForm(DWYVEC,DWYL,IERR)
      CALL VecGhostRestoreLocalForm(DWZVEC,DWZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VISVEC,VISL,IERR)
C
      CALL VecRestoreArray(VOLL,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCL,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCL,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCL,ZCARR,ZCCI,IERR)
C
      CALL VecGhostRestoreLocalForm(VOLVEC,VOLL,IERR)
      CALL VecGhostRestoreLocalForm(XCVEC,XCL,IERR)
      CALL VecGhostRestoreLocalForm(YCVEC,YCL,IERR)
      CALL VecGhostRestoreLocalForm(ZCVEC,ZCL,IERR)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C##############################################################
      SUBROUTINE FLUXM(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                 FM,FAC)
C##############################################################
C     This routine calculates mass flux through the cell face 
C     between nodes IJP and IJN. IJ1 and IJ2 are the indices of 
C     CV corners defining the cell face. FM is the mass flux 
C     through the face, and FAC is the interpolation
C     factor (distance from node IJP to cell face center over
C     the sum of this distance and the distance from cell face 
C     center to node IJN). CAP and CAN are the contributions to
C     matrix coefficients in the pressure-correction equation
C     at nodes IJP and IJN. Surface vector directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C==============================================================
      IMPLICIT NONE
C
#include "finclude/petsc.h"
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "coef3d.inc"
#include "propcell3d.inc"
C
      INTEGER IJN,IJP
C
      REAL*8 FACP,FAC,XI,YI,ZI,DUXI,DVXI,DWXI
      REAL*8 DUYI,DVYI,DWYI,DUZI,DVZI,DWZI
      REAL*8 YYC,ZZC,UI,XXC,VI,WI,DENI,XPN,YPN
      REAL*8 ZPN,SMDPN,XCR,YCR,ZCR,CAP
      REAL*8 DPXI,DPYI,DPZI,FM
      REAL*8 VAP,VAN,SAP
C
#include "petsc.user.inc"
C
C==============================================================
C
      FACP=1.0D0-FAC
      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
C
      DUXI=DUX(IJN)*FAC+DUX(IJP)*FACP
      DVXI=DVX(IJN)*FAC+DVX(IJP)*FACP
      DWXI=DWX(IJN)*FAC+DWX(IJP)*FACP
C
      DUYI=DUY(IJN)*FAC+DUY(IJP)*FACP
      DVYI=DVY(IJN)*FAC+DVY(IJP)*FACP
      DWYI=DWY(IJN)*FAC+DWY(IJP)*FACP
C
      DUZI=DUZ(IJN)*FAC+DUZ(IJP)*FACP
      DVZI=DVZ(IJN)*FAC+DVZ(IJP)*FACP
      DWZI=DWZ(IJN)*FAC+DWZ(IJP)*FACP
C
C.....CALCULATE CELL-FACE VALUES (VELOCITIES AND DENSITY)
C
      UI=U(IJN)*FAC+U(IJP)*FACP+DUXI*(XXC-XI)+DUYI*(YYC-YI)+
     *                          DUZI*(ZZC-ZI)
      VI=V(IJN)*FAC+V(IJP)*FACP+DVXI*(XXC-XI)+DVYI*(YYC-YI)+
     *                          DVZI*(ZZC-ZI)
      WI=W(IJN)*FAC+W(IJP)*FACP+DWXI*(XXC-XI)+DWYI*(YYC-YI)+
     *                          DWZI*(ZZC-ZI)
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....SURFACE AND DISTANCE VECTOR COMPONENTS
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      SMDPN=(XCR**2+YCR**2+ZCR**2)/
     *      (XCR*XPN+YCR*YPN+ZCR*ZPN+SMALL)
C
C.....MASS FLUX
C
      CAP=-0.5D0*(VOL(IJP)*APR(IJP)+VOL(IJN)*APR(IJN))*DENI*SMDPN
      DPXI=0.5D0*(DPX(IJN)+DPX(IJP))*XPN
      DPYI=0.5D0*(DPY(IJN)+DPY(IJP))*YPN
      DPZI=0.5D0*(DPZ(IJN)+DPZ(IJP))*ZPN
      FM=DENI*(UI*XCR+VI*YCR+WI*ZCR)
     *   +CAP*(P(IJN)-P(IJP)-DPXI-DPYI-DPZI)
C
      RETURN
      END
C
#include "petsc.user.inc"
C
C
C##############################################################
      SUBROUTINE FLUXP(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
C    *                 FM,CAP,CAN,FAC)
C NEWCOUPLED
     *                    CAP,CAN,FAC)
C END NEWCOUPLED
C##############################################################
C     This routine calculates mass flux through the cell face 
C     between nodes IJP and IJN. IJ1 and IJ2 are the indices of 
C     CV corners defining the cell face. FM is the mass flux 
C     through the face, and FAC is the interpolation
C     factor (distance from node IJP to cell face center over
C     the sum of this distance and the distance from cell face 
C     center to node IJN). CAP and CAN are the contributions to
C     matrix coefficients in the pressure-correction equation
C     at nodes IJP and IJN. Surface vector directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C==============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "coef3d.inc"
#include "propcell3d.inc"
C
      INTEGER IJN,IJP
C
      REAL*8 FACP,FAC,XI,YI,ZI,DUXI,DVXI,DWXI
      REAL*8 DUYI,DVYI,DWYI,DUZI,DVZI,DWZI
      REAL*8 YYC,ZZC,UI,XXC,VI,WI,DENI,XPN,YPN
      REAL*8 ZPN,SMDPN,XCR,YCR,ZCR,CAP,CAN
      REAL*8 DPXI,DPYI,DPZI,FM
C NEWCOUPLED
      REAL*8 VAP,VAN,SAP,APRI,VOLI
C END NEWCOUPLED
C
C NEW
#include "petsc.user.inc"
C END NEW
C==============================================================
C
      FACP=1.0D0-FAC
C
C.....CALCULATE CELL-FACE VALUES (DENSITY)
C
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C NEWCOUPLED
      VOLI=0.5D0*(VOL(IJN)+VOL(IJP))
      APRI=0.5D0*(APR(IJN)+APR(IJP))
C END NEWCOUPLED
C
C.....SURFACE AND DISTANCE VECTOR COMPONENTS
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      SMDPN=(XCR**2+YCR**2+ZCR**2)/
     *      (XCR*XPN+YCR*YPN+ZCR*ZPN+SMALL)
C
C.....COEFFICIENTS FOR THE P-EQUATION
C
      CAP=-0.5D0*(VOL(IJP)*APR(IJP)+VOL(IJN)*APR(IJN))*DENI*SMDPN
      CAN=CAP
C
C.....SOURCE TERM DUE TO RHIE-CHOW-INTERPOLATION (NO NON-ORTH-CORR.)
C
      VAP=VOL(IJP)*APR(IJP)
      VAN=VOL(IJN)*APR(IJN)

C     SAP=0.5*((VAP*DPX(IJP)+VAN*DPX(IJN))*XCR+
C    *         (VAP*DPY(IJP)+VAN*DPY(IJN))*YCR+
C    *         (VAP*DPZ(IJP)+VAN*DPZ(IJP))*ZCR)*DENI

      DPXI=0.5D0*(DPX(IJN)+DPX(IJP))*XPN
      DPYI=0.5D0*(DPY(IJN)+DPY(IJP))*YPN
      DPZI=0.5D0*(DPZ(IJN)+DPZ(IJP))*ZPN
      SAP = -CAP*(DPXI+DPYI+DPZI)

      SP(IJP)=SP(IJP)+SAP
      SP(IJN)=SP(IJN)-SAP
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C##############################################################
      SUBROUTINE FLUXMC(IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
     *                 FM,FAC)
C##############################################################
C     This routine calculates mass flux through the cell face 
C     between nodes IJP and IJN. IJ1 and IJ2 are the indices of 
C     CV corners defining the cell face. FM is the mass flux 
C     through the face, and FAC is the interpolation
C     factor (distance from node IJP to cell face center over
C     the sum of this distance and the distance from cell face 
C     center to node IJN). CAP and CAN are the contributions to
C     matrix coefficients in the pressure-correction equation
C     at nodes IJP and IJN. Surface vector directed from P to N.
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C==============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "coef3d.inc"
#include "propcell3d.inc"
C
      INTEGER IJN,IJP
C
      REAL*8 FACP,FAC,XI,YI,ZI
      REAL*8 YYC,ZZC,XXC,DENI,XPN,YPN
      REAL*8 ZPN,SMDPN,XCR,YCR,ZCR
      REAL*8 DPXI,DPYI,DPZI,FM,RAPR,DN
C
C NEW
#include "petsc.user.inc"
C END NEW
C==============================================================
C
C.....INTERPOLATE ALONG LINE P-N COORDINATES
C
      FACP=1.0D0-FAC
      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP    
C
C.....CALCULATE CELL-FACE VALUES (DENSITY)
C
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....SURFACE AND DISTANCE VECTOR COMPONENTS
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      SMDPN=XCR**2+YCR**2+ZCR**2
      DN=XPN*XCR+YPN*YCR+ZPN*ZCR
C
C.....MASS FLUX CORRECTION
C
      RAPR=-0.5D0*(VOL(IJP)*APR(IJP)+VOL(IJN)*APR(IJN))*DENI
      DPXI=0.5D0*(DPX(IJN)+DPX(IJP))
      DPYI=0.5D0*(DPY(IJN)+DPY(IJP))
      DPZI=0.5D0*(DPZ(IJN)+DPZ(IJP))
      FM=RAPR*((DN*XCR-XPN*SMDPN)*DPXI+(DN*YCR-YPN*SMDPN)*DPYI+
     &     (DN*ZCR-ZPN*SMDPN)*DPZI)
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C###############################################################
C NEW
C     SUBROUTINE PRESB(FI)
      SUBROUTINE PRESB(FIVEC)
C END NEW
C###############################################################
C     This routine extrapolates the pressure or pressure
C     correction from interior to the boundary. Linear 
C     extrapolation is used, but one can also linearly
C     extrapolate the gradient...
C
C
C==============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
#include "model3d.inc"
C
      INTEGER M,K,I,J,IJK
C
C NEW
C     REAL*8 FI(NXYZA)
      REAL*8 FIARR( 1)
C
      PetscOffset FIII
      Vec FIVEC
      PetscErrorCode IERR
C NEW
#include "petsc.user.inc"
C END NEW
C==============================================================
C
C.....SET INDICES
C
C NEW
      CALL VecGetArray(FIVEC,FIARR,FIII,IERR)
C END NEW
      DO M=1,NBLKS 
      CALL SETIND(M)
C
C.....EXTRAPOLATE TO SOUTH AN NORTH BOUNDARIES
C
      DO K=2,NKM
      DO I=2,NIM
        IJK=LKBK(K+KST)+LIBK(I+IST)+1
        FI(IJK)=FI(IJK+1)+(FI(IJK+1)-FI(IJK+2))*FY(IJK+1)
        IJK=LKBK(K+KST)+LIBK(I+IST)+NJ
        FI(IJK)=FI(IJK-1)+(FI(IJK-1)-FI(IJK-2))*(1.0D0-FY(IJK-2))
      END DO
      END DO
C
C.....EXTRAPOLATE TO WEST AND EAST BOUNDARIES
C
      DO K=2,NKM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(1+IST)+J
        FI(IJK)=FI(IJK+NJ)+(FI(IJK+NJ)-FI(IJK+NJ+NJ))*FX(IJK+NJ)
        IJK=LKBK(K+KST)+LIBK(NI+IST)+J
        FI(IJK)=FI(IJK-NJ)+(FI(IJK-NJ)-FI(IJK-NJ-NJ))*
     *          (1.0D0-FX(IJK-NJ-NJ))
      END DO
      END DO
C
C.....EXTRAPOLATE TO BOTTOM AND TOP BOUNDARIES
C
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(1+KST)+LIBK(I+IST)+J
        FI(IJK)=FI(IJK+NIJ)+(FI(IJK+NIJ)-FI(IJK+NIJ+NIJ))*FZ(IJK+NIJ)
        IJK=LKBK(NK+KST)+LIBK(I+IST)+J
        FI(IJK)=FI(IJK-NIJ)+(FI(IJK-NIJ)-FI(IJK-NIJ-NIJ))*
     *         (1.0D0-FZ(IJK-NIJ-NIJ))
      END DO
      END DO
C
      END DO
C
C NEW
      CALL VecRestoreArray(FIVEC,FIARR,FIII,IERR)
C END NEW
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C###############################################################
C NEW
C     SUBROUTINE GRADFI(FI,DFX,DFY,DFZ)
C     SUBROUTINE GRADFI(FIVEC,DFXVEC,DFYVEC,DFZVEC)
      SUBROUTINE GRADFI(FIL,DFXVEC,DFYVEC,DFZVEC)
C END NEW
C###############################################################
C     This routine calculates the components of the gradient
C     vector of a scalar FI at the CV center, using conservative
C     scheme based on the Gauss theorem; see Sect. 8.6 for 
C     details. FIE are values at east side, FIN at north side.
C     Contributions from boundary faces are calculated in a
C     separate loops...
C
C     Takes as argument vector FIL which is already in local form
C
C
C===============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "var3d.inc"
#include "rcont3d.inc"
#include "logic3d.inc"
#include "geo3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "gradold3d.inc"
C
      INTEGER LC,M,NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT
      INTEGER I,II,IO,ISY,IW,K,LKK,LKI,JP,JPL,
C NEW
     *        J
C END NEW
C
C NEW
C     REAL*8 FI(NXYZA),DFX(NXYZA),DFY(NXYZA),DFZ(NXYZA)
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
C
      Vec    DFXVEC,DFYVEC,DFZVEC,
     *       DFXL  ,DFYL  ,DFZL,
     *       DFXOL ,DFYOL ,DFZOL,
     *       FIVEC,FIL
      PetscOffset FIII,FIIIN,DFXXI ,DFYYI ,DFZZI,
     *                       DFXOOI,DFYOOI,DFZOOI
      PetscScalar PZERO
      PetscErrorCode IERR
      PetscInt RANK
      integer LS
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
     *                      DFXOOI,DFYOOI,DFZOOI
      COMMON /OUTER/ LS
C NEW
#include "petsc.user.inc"
C END NEW
C==============================================================
C
C
C NEW
C     FIII=FIIIN
      PZERO=0.0D0
C NEWCOUPLED - NOT NEEDED ANYMORE, UPDATE BEFORE CALLING THIS SUBROUTINE
C     CALL VecGhostUpdateBegin(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     CALL VecGhostUpdateEnd(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C     CALL VecGhostGetLocalForm(FIVEC,FIL,IERR)
C END NEWCOUPLED
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
C END NEW
C
C.....INITIALIZE OLD GRADIENT
C NEW
C     DFXO=0.; DFYO=0.; DFZO=0.
      CALL VecSet(DFXOVEC,PZERO,IERR)
      CALL VecSet(DFYOVEC,PZERO,IERR)
      CALL VecSet(DFZOVEC,PZERO,IERR)
C END NEW
C
C.....START ITERATIVE CALCULATION OF GRADIENTS
C
      DO LC=1,NIGRAD
C NEW
C
C.....GET ARRAY OF VECTOR VALUES
C
        IF (LC.NE.1) THEN
          CALL VecGhostUpdateBegin(
     *         DFXOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateEnd(
     *         DFXOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateBegin(
     *         DFYOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateEnd(
     *         DFYOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateBegin(
     *         DFZOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
          CALL VecGhostUpdateEnd(
     *         DFZOVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
        END IF
C
        CALL VecGhostGetLocalForm(DFXVEC,DFXL,IERR)
        CALL VecGhostGetLocalForm(DFYVEC,DFYL,IERR)
        CALL VecGhostGetLocalForm(DFZVEC,DFZL,IERR)
C
        CALL VecGhostGetLocalForm(DFXOVEC,DFXOL,IERR)
        CALL VecGhostGetLocalForm(DFYOVEC,DFYOL,IERR)
        CALL VecGhostGetLocalForm(DFZOVEC,DFZOL,IERR)
C END NEW
C
C.......INITIALIZE NEW GRADIENT
C
C NEW
C       DFX(1:NXYZA)=0.; DFY(1:NXYZA)=0.; DFZ(1:NXYZA)=0.
        CALL VecSet(DFXL,PZERO,IERR)
        CALL VecSet(DFYL,PZERO,IERR)
        CALL VecSet(DFZL,PZERO,IERR)
C
        CALL VecGetArray(DFXL,DFXARR,DFXXI,IERR)
        CALL VecGetArray(DFYL,DFYARR,DFYYI,IERR)
        CALL VecGetArray(DFZL,DFZARR,DFZZI,IERR)
C
        CALL VecGetArray(DFXOL,DFXOARR,DFXOOI,IERR)
        CALL VecGetArray(DFYOL,DFYOARR,DFYOOI,IERR)
        CALL VecGetArray(DFZOL,DFZOARR,DFZOOI,IERR)
C END NEW
C
C NEW
c.....OpenMP : Start parallel loop
C$OMP PARALLEL DO DEFAULT(SHARED)
C$OMP*            PRIVATE(M,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,NJT,NIJT)
C END NEW
        DO M=1,NBLKS
C
        NKMT=NKBK(M)-1
        NIMT=NIBK(M)-1
        NJMT=NJBK(M)-1
        KSTT=KBK(M)
        ISTT=IBK(M)
        NJT=NJMT+1
        NIJT=(NIMT+1)*NJT
C        
C.......CONTRIBUTION FROM INNER EAST SIDES
C
        CALL GRADCOLOOP(KSTT,ISTT,NKMT,NIMT-1,NJMT,NJT,
C NEW
C    *       FI,DFX,DFY,DFZ,FX,XEC,YEC,ZEC,XER,YER,ZER)
     *       FIARR,DFXARR,DFYARR,DFZARR,FX,XEC,YEC,ZEC,XER,YER,ZER)
C END NEW
C
C.......CONTRIBUTION FROM INNER NORTH SIDES
C
        CALL GRADCOLOOP(KSTT,ISTT,NKMT,NIMT,NJMT-1,1,
C NEW
C    *       FI,DFX,DFY,DFZ,FY,XNC,YNC,ZNC,XNR,YNR,ZNR)
     *       FIARR,DFXARR,DFYARR,DFZARR,FY,XNC,YNC,ZNC,XNR,YNR,ZNR)
C END NEW
C
C.......CONTRIBUTION FROM INNER TOP SIDES
C
        CALL GRADCOLOOP(KSTT,ISTT,NKMT-1,NIMT,NJMT,NIJT,
C NEW
C    *       FI,DFX,DFY,DFZ,FZ,XTC,YTC,ZTC,XTR,YTR,ZTR)
     *       FIARR,DFXARR,DFYARR,DFZARR,FZ,XTC,YTC,ZTC,XTR,YTR,ZTR)
C END NEW
C
        END DO
C.....OpenMP : Here ends this parrallel do
c
C
C.......CONTRIBUTION FROM O- AND C-GRID CUTS
C
        DO I=1,NOCBKAL
C NEW
C         CALL GRADCO(FI,DFX,DFY,DFZ,FOCBK(I),IJLPBK(I),IJRPBK(I),
          CALL GRADCO(FIARR,DFXARR,DFYARR,DFZARR,
     *                FOCBK(I),IJLPBK(I),IJRPBK(I),
C END NEW
     *                XOCC(I),YOCC(I),ZOCC(I),
     *                XOCR(I),YOCR(I),ZOCR(I))
        END DO
C NEW
C
C.......CONTRIBUTION FROM FACE SEGMENT BOUNDARIES
C
        DO I=1,NFSGBKAL
C NEW
C         CALL GRADCO(FI,DFX,DFY,DFZ,FFSGBK(I),IJFL(I),IJFR(I),
          CALL GRADCO(FIARR,DFXARR,DFYARR,DFZARR,
     *                FFSGBK(I),IJFL(I),IJFR(I),
C END NEW
     *                XFC(I),YFC(I),ZFC(I),
     *                XFR(I),YFR(I),ZFR(I))
        END DO
C END NEW
C
C.......CONTRIBUTION FROM INLET BOUNDARIES
C
        DO II=1,NINLBKAL
          CALL GRADBC(IJPI(II),IJI(II),XIR(II),YIR(II),ZIR(II),
C NEW
C    *                DFX,DFY,DFZ,FI)
     *                DFXARR,DFYARR,DFZARR,FIARR)
C END NEW
        END DO
C
C.......CONTRIBUTION FROM OUTLET BOUNDARIES
C
        DO IO=1,NOUTBKAL
          CALL GRADBC(IJPO(IO),IJO(IO),XUR(IO),YOR(IO),ZOR(IO),
C NEW
C    *                DFX,DFY,DFZ,FI)
     *                DFXARR,DFYARR,DFZARR,FIARR)
C END NEW
        END DO
C
C.......CONTRIBUTION FROM SYMMETRY BOUNDARIES
C
        DO ISY=1,NSYMBKAL
          CALL GRADBC(IJPS(ISY),IJS(ISY),XNS(ISY),YNS(ISY),ZNS(ISY),
C NEW
C    *                DFX,DFY,DFZ,FI)
     *                DFXARR,DFYARR,DFZARR,FIARR)
C END NEW
        END DO
C
C.......CONTRIBUTION FROM WALL BOUNDARIES
C
        DO IW=1,NWALBKAL
          CALL GRADBC(IJPW(IW),IJW(IW),XNW(IW),YNW(IW),ZNW(IW),
C NEW
C    *                DFX,DFY,DFZ,FI)
     *                DFXARR,DFYARR,DFZARR,FIARR)
C END NEW
        END DO
C
C.......CALCULATE GRADIENT COMPONENTS AT CV-CENTERS
C
C NEW
C
C.....UPDATE GRADIENT COMPONENTS ON ALL PROCESSORS
C
C
        CALL VecRestoreArray(DFXL,DFXARR,DFXXI,IERR)
        CALL VecRestoreArray(DFYL,DFYARR,DFYYI,IERR)
        CALL VecRestoreArray(DFZL,DFZARR,DFZZI,IERR)
C
        CALL VecRestoreArray(DFXOL,DFXOARR,DFXOOI,IERR)
        CALL VecRestoreArray(DFYOL,DFYOARR,DFYOOI,IERR)
        CALL VecRestoreArray(DFZOL,DFZOARR,DFZOOI,IERR)
C
        CALL VecGhostRestoreLocalForm(DFXVEC,DFXL,IERR)
        CALL VecGhostRestoreLocalForm(DFYVEC,DFYL,IERR)
        CALL VecGhostRestoreLocalForm(DFZVEC,DFZL,IERR)
C
        CALL VecGhostRestoreLocalForm(DFXOVEC,DFXOL,IERR)
        CALL VecGhostRestoreLocalForm(DFYOVEC,DFYOL,IERR)
        CALL VecGhostRestoreLocalForm(DFZOVEC,DFZOL,IERR)
C
        CALL VecGhostUpdateBegin(DFXVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateEnd(DFXVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateBegin(DFYVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateEnd(DFYVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateBegin(DFZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
        CALL VecGhostUpdateEnd(DFZVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
        CALL VecGetArray(DFXVEC,DFXARR,DFXXI,IERR)
        CALL VecGetArray(DFYVEC,DFYARR,DFYYI,IERR)
        CALL VecGetArray(DFZVEC,DFZARR,DFZZI,IERR)
C
C.....OpenMP : Here starts a parallel do
C$OMP PARALLEL DO DEFAULT(SHARED)
C$OMP*            PRIVATE(M,K,I,J,
C$OMP*                    NKMT,NIMT,NJMT,KSTT,ISTT,
C$OMP*                    LKK,LKI,JP,JPL)
C END NEW
        DO M=1,NBLKS
C
        NKMT=NKBK(M)-1
        NIMT=NIBK(M)-1
        NJMT=NJBK(M)-1
        KSTT=KBK(M)
        ISTT=IBK(M)
C        
        DO K=2,NKMT
        LKK=LKBK(K+KSTT)
        DO I=2,NIMT
        LKI=LKK+LIBK(I+ISTT)
          JP=LKI+2
          JPL=LKI+NJMT
C NEW
C         DFX(JP:JPL)=DFX(JP:JPL)/VOL(JP:JPL)
C         DFY(JP:JPL)=DFY(JP:JPL)/VOL(JP:JPL)
C         DFZ(JP:JPL)=DFZ(JP:JPL)/VOL(JP:JPL)
          DO J=JP,JPL
            DFX(J)=DFX(J)/VOL(J)
            DFY(J)=DFY(J)/VOL(J)
            DFZ(J)=DFZ(J)/VOL(J)
          END DO
C END NEW
        END DO
        END DO
C
        END DO
C.....OpenMP : Here ends this parallel loop
C
C
        CALL VecRestoreArray(DFXVEC,DFXARR,DFXXI,IERR)
        CALL VecRestoreArray(DFYVEC,DFYARR,DFYYI,IERR)
        CALL VecRestoreArray(DFZVEC,DFZARR,DFZZI,IERR)
C
C.......SET OLD GRADIENT = NEW GRADIENT FOR THE NEXT ITERATION
C
        IF(LC.NE.NIGRAD) THEN
C NEW
C         DFXO=DFX
C         DFYO=DFY
C         DFZO=DFZ
          CALL VecCopy(DFXVEC,DFXOVEC,IERR)
          CALL VecCopy(DFYVEC,DFYOVEC,IERR)
          CALL VecCopy(DFZVEC,DFZOVEC,IERR)
C END NEW
        ENDIF
C
      END DO
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
C NEWCOUPLED - NOT NEEDED ANYMORE
C     CALL VecGhostRestoreLocalForm(FIVEC,FIL,IERR)
C END NEWCOUPLED
C
      CALL PetscBarrier(DFXVEC,IERR)
      CALL VecGhostUpdateBegin(
     *     DFXVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DFXVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     DFYVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DFYVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     DFZVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DFZVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C###############################################################
C NEW
C     SUBROUTINE GRADCO(FI   ,DFX   ,DFY   ,DFZ   ,FAC,IJP,IJN,
      SUBROUTINE GRADCO(FIARR,DFXARR,DFYARR,DFZARR,FAC,IJP,IJN,
C END NEW
     *                  XXC,YYC,ZZC,XCR,YCR,ZCR)
C###############################################################
C     This routine calculates contribution to the gradient
C     vector of a scalar FI at the CV center, arising from
C     an inner cell face (cell-face value of FI times the 
C     corresponding component of the surface vector).
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C     For inner cells we use now gradcoloop. Gradco is used
C     only for interfaces now.
C
C===============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "var3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "propcell3d.inc"
#include "gradold3d.inc"
C
      INTEGER IJN,IJP
C
      REAL*8 FACP,FAC,XI,YI,ZI,DFXI,DFYI,DFZI,XCC,YCC,ZCC
      REAL*8 DFXE,XCR,YCR,ZCR,DFYE,DFZE,FIE,XXC,YYC,ZZC
C NEW
C     REAL*8 FI(NXYZA),DFX(NXYZA),DFY(NXYZA),DFZ(NXYZA)
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
C
      PetscOffset FIII,DFXXI,DFYYI,DFZZI,
     *                 DFXOOI,DFYOOI,DFZOOI
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
     *                      DFXOOI,DFYOOI,DFZOOI
C NEW
#include "petsc.user.inc"
C END NEW
C==============================================================
C
C.....COORDINATES OF POINT ON THE LINE CONNECTING CENTER AND NEIGHBOR,
C     OLD GRADIENT VECTOR COMPONENTS INTERPOLATED FOR THIS LOCATION
C
      FACP=1.0D0-FAC
      XI=XC(IJN)*FAC+XC(IJP)*FACP
      YI=YC(IJN)*FAC+YC(IJP)*FACP
      ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
      DFXI=DFXO(IJN)*FAC+DFXO(IJP)*FACP
      DFYI=DFYO(IJN)*FAC+DFYO(IJP)*FACP
      DFZI=DFZO(IJN)*FAC+DFZO(IJP)*FACP
C
C.....VARIABLE VALUE AT THE CELL-FACE CENTER
C
      FIE=FI(IJN)*FAC+FI(IJP)*FACP+DFXI*(XXC-XI)+DFYI*(YYC-YI)+
     *                             DFZI*(ZZC-ZI)
C
C.....GRADIENT CONTRIBUTION FROM CELL FACE
C
      DFXE=FIE*XCR
      DFYE=FIE*YCR
      DFZE=FIE*ZCR  
C
C.....ACCUMULATE CONTRIBUTION AT CELL CENTER AND NEIGHBOR
C
      DFX(IJP)=DFX(IJP)+DFXE
      DFY(IJP)=DFY(IJP)+DFYE
      DFZ(IJP)=DFZ(IJP)+DFZE
      DFX(IJN)=DFX(IJN)-DFXE
      DFY(IJN)=DFY(IJN)-DFYE
      DFZ(IJN)=DFZ(IJN)-DFZE
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C###############################################################
      SUBROUTINE GRADCOLOOP(KSTTT,ISTTT,MKM,MIM,MJM,NPN,
C NEW
C    *           FI,DFX,DFY,DFZ,FACV,XXC,YYC,ZZC,XCR,YCR,ZCR)
     *           FIARR,DFXARR,DFYARR,DFZARR,
     *                          FACV,XXC,YYC,ZZC,XCR,YCR,ZCR)
C END NEW
C###############################################################
C     This routine calculates contribution to the gradient
C     vector of a scalar FI at the CV center, arising from
C     inner cell faces (cell-face value of FI times the 
C     corresponding component of the surface vector).
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C     This 'gradcoloop' routine substitutes gradco for the inner
C     cells. It incorporates de I,J,K loops, rather than being
C     called once for each node, so that it reduces the overhead
C     with significant savings ( gradient computations are 40%
C     faster this way ).      
C
C===============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "var3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "propcell3d.inc"
#include "gradold3d.inc"
C
      INTEGER K,I,J,MKM,MIM,MJM,IJP,KSTTT,ISTTT,IJN,NPN
C
C NEW
C     REAL*8 FI(NXYZA),DFX(NXYZA),DFY(NXYZA),DFZ(NXYZA)
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
C END NEW
      REAL*8 XXC(NXYZA),YYC(NXYZA),ZZC(NXYZA)
      REAL*8 XCR(NXYZA),YCR(NXYZA),ZCR(NXYZA)
      REAL*8 FACV(NXYZA)
      REAL*8 FAC,FACP,XI,YI,ZI,DFXI,DFYI,DFZI,FIE
      REAL*8 DFXE,DFYE,DFZE
C NEW
C
      PetscOffset FIII,DFXXI,DFYYI,DFZZI,
     *                 DFXOOI,DFYOOI,DFZOOI
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
     *                      DFXOOI,DFYOOI,DFZOOI
C
C NEW
#include "petsc.user.inc"
C END NEW
C==============================================================
C
C.....LOOP THROUGH NODES
C
      DO K=2,MKM
      DO I=2,MIM
      DO J=2,MJM
        IJP=LKBK(K+KSTTT)+LIBK(I+ISTTT)+J
        IJN=IJP+NPN
C
C.....COORDINATES OF POINT ON THE LINE CONNECTING CENTER AND NEIGHBOR,
C     OLD GRADIENT VECTOR COMPONENTS INTERPOLATED FOR THIS LOCATION
C
        FAC=FACV(IJP)
        FACP=1.0D0-FAC
        XI=XC(IJN)*FAC+XC(IJP)*FACP
        YI=YC(IJN)*FAC+YC(IJP)*FACP
        ZI=ZC(IJN)*FAC+ZC(IJP)*FACP
        DFXI=DFXO(IJN)*FAC+DFXO(IJP)*FACP
        DFYI=DFYO(IJN)*FAC+DFYO(IJP)*FACP
        DFZI=DFZO(IJN)*FAC+DFZO(IJP)*FACP
C
C.....VARIABLE VALUE AT THE CELL-FACE CENTER
C
        FIE=FI(IJN)*FAC+FI(IJP)*FACP
     *     +DFXI*(XXC(IJP)-XI)+DFYI*(YYC(IJP)-YI)
     *     +DFZI*(ZZC(IJP)-ZI)
C
C.....GRADIENT CONTRIBUTION FROM CELL FACE
C
        DFXE=FIE*XCR(IJP)
        DFYE=FIE*YCR(IJP)
        DFZE=FIE*ZCR(IJP)
C
C.....ACCUMULATE CONTRIBUTION AT CELL CENTER AND NEIGHBOR
C
        DFX(IJP)=DFX(IJP)+DFXE
        DFY(IJP)=DFY(IJP)+DFYE
        DFZ(IJP)=DFZ(IJP)+DFZE
        DFX(IJN)=DFX(IJN)-DFXE
        DFY(IJN)=DFY(IJN)-DFYE
        DFZ(IJN)=DFZ(IJN)-DFZE
C
      END DO
      END DO
      END DO
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C########################################################
      SUBROUTINE GRADBC(IJP,IJB,XCR,YCR,ZCR,
C NEW
C    *                  DFX,DFY,DFZ,FI)
     *                  DFXARR,DFYARR,DFZARR,FIARR)
C END NEW
C########################################################
C     This routine calculates the contribution of a 
C     boundary cell face to the gradient at CV-center.
C
C
C=======================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
C
      INTEGER IJP,IJB
C
C NEW
C     REAL*8 FI(NXYZA),DFX(NXYZA),DFY(NXYZA),DFZ(NXYZA)
      REAL*8 FIARR( 1),DFXARR( 1),DFYARR( 1),DFZARR( 1)
C END NEW
      REAL*8 XCR,YCR,ZCR
C NEW
C
      PetscOffset FIII,DFXXI,DFYYI,DFZZI,
     *                 DFXOOI,DFYOOI,DFZOOI
      COMMON /FOFFSET/ FIII,DFXXI ,DFYYI ,DFZZI,
     *                      DFXOOI,DFYOOI,DFZOOI
C
C NEW
#include "petsc.user.inc"
C END NEW
C=======================================================
C
      DFX(IJP)=DFX(IJP)+FI(IJB)*XCR
      DFY(IJP)=DFY(IJP)+FI(IJB)*YCR
      DFZ(IJP)=DFZ(IJP)+FI(IJB)*ZCR
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C#############################################################
C     SUBROUTINE CALCSC(IFI,FI,FIO,FIOO)
      SUBROUTINE CALCSC(IFI,FIVEC,FIO,FIOO)
C#############################################################
C     This routine discretizes and solves the scalar transport
C     equations (temperature, turbulent kinetic energy, diss.).
C NEW
C     !!!NOT VERIFIED!!
C END NEW
C=============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "varold3d.inc"
#include "grad3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C     
      INTEGER IFI,M,I,J,K,IJK,IJP,IJN,MIJ,II,IJB
      INTEGER IO,ISY
C
      REAL*8 GFI,URFFI,APT,CP,CB
C NEW
C     REAL*8 FI(NXYZA),FIO(NXYZA),FIOO(NXYZA)
      REAL*8 FIARR( 1),FIO(NXYZA),FIOO(NXYZA)
C
      PetscScalar PONE,PZERO
      Vec FIVEC,FIL,DPXL,DPYL,DPZL,DENL,VISL,SUL,APL
      PetscOffset FIII
      PetscErrorCode IERR
      COMMON /SCALARFI/ FIII
C NEW
#include "petsc.user.inc"
C END NEW
C============================================================
C NEW
      PZERO=0.0D0
      PONE=1.0D0
C
C.....FETCH SCALAR VALUES FROM OTHER PROCESSORS
C
      CALL VecGhostUpdateBegin(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(FIVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostGetLocalform(FIVEC,FIL,IERR)
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
C
C.....CALCULATE GRADIENTS OF FI
C
C      CALL GRADFI(FI,DPX,DPY,DPZ)
      CALL GRADFI(FIVEC,DPXVEC,DPYVEC,DPZVEC)
C
C.....UPDATE GHOSTED GRADIENT VECTOR COMPONENTS. Überprüfen ob nicht
C.....vielleicht eine separate Routine besser ist
C
      CALL PetscBarrier(FIVEC,IERR)
C
      CALL VecGhostUpdateBegin(
     *     DPXVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DPXVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     DPYVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DPYVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     DPZVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DPZVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
      CALL VecGhostUpdateBegin(
     *     DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(DENVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(
     *     VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VISVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C
C.....GET ARRAY OF VECTOR VALUES: GRADIENT COMPONENTS, DENS, VISC
C
      CALL VecGhostGetLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostGetLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostGetLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostGetLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostGetLocalForm(VISVEC,VISL,IERR)
C
      CALL VecGetArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecGetArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecGetArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecGetArray(DENL,DENARR,DENNI,IERR)
      CALL VecGetArray(VISL,VISARR,VISSI,IERR)
C
C END NEW
C
C.....INITIALIZE ARRAYS, SET BLENDING AND UNDER_RELAXATION COEFF.
C
C NEW
C     SU(1:NXYZA)=0.; AP(1:NXYZA)=0.
C
C.....Zero the local Representation (including the ghost elements)
C
      CALL VecGhostGetLocalForm(SUVEC,SUL,IERR)
      CALL VecGhostGetLocalForm(APVEC,APL,IERR)
C
      CALL VecSet(SUL,PZERO,IERR)
      CALL VecSet(APL,PZERO,IERR)
C
      CALL VecGetArray(SUL,SUARR,SUUI,IERR)
      CALL VecGetArray(APL,APARR,APPI,IERR)
C END NEW
      GFI=GDS(IFI)
      URFFI=1./URF(IFI)
C
C.....CALCULATE FLUXES THROUGH INNER CV-FACES (EAST, NORTH & TOP)
C
C
CC.....OpenMP : Start parallel loop section
CC$OMP PARALLEL DO DEFAULT(SHARED), PRIVATE(M,K,I,J,
CC$OMP*  IJK,NKMT,NIMT,NJMT,NJT,NIJT,KSTT,ISTT,SB,APT)
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM-1
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXSC(IFI,IJK,IJK+NJ,XEC(IJK),YEC(IJK),ZEC(IJK),
     *                             XER(IJK),YER(IJK),ZER(IJK),
C NEW
C    *               F1(IJK),AW(IJK+NJ),AE(IJK),FX(IJK),GFI,M,FI)
     *               F1(IJK),AW(IJK+NJ),AE(IJK),FX(IJK),GFI,M,FIARR)
C END NEW
      END DO
      END DO
      END DO
      END DO
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM-1
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXSC(IFI,IJK,IJK+1,XNC(IJK),YNC(IJK),ZNC(IJK),
     *                            XNR(IJK),YNR(IJK),ZNR(IJK),
C NEW
C    *               F2(IJK),AS(IJK+1),AN(IJK),FY(IJK),GFI,M,FI)
     *               F2(IJK),AS(IJK+1),AN(IJK),FY(IJK),GFI,M,FIARR)
C END NEW
      END DO
      END DO
      END DO
      END DO
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM-1
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL FLUXSC(IFI,IJK,IJK+NIJ,XTC(IJK),YTC(IJK),ZTC(IJK),
     *                              XTR(IJK),YTR(IJK),ZTR(IJK),
C NEW
C    *               F3(IJK),AB(IJK+NIJ),AT(IJK),FZ(IJK),GFI,M,FI)
     *               F3(IJK),AB(IJK+NIJ),AT(IJK),FZ(IJK),GFI,M,FIARR)
C END NEW
      END DO
      END DO
      END DO
      END DO
C
C.....CONTRIBUTION FROM O- AND C-GRID CUTS
C
      DO I=1,NOCBKAL
        IJP=IJLPBK(I)
        IJN=IJRPBK(I)
        MIJ=IBLKOCBK(I)
        CALL FLUXSC(IFI,IJP,IJN,XOCC(I),YOCC(I),ZOCC(I),
     *                          XOCR(I),YOCR(I),ZOCR(I),
C NEW
C    *              FMOC(I),AL(I),AR(I),FOCBK(I),GFI,MIJ,FI)
     *              FMOC(I),AL(I),AR(I),FOCBK(I),GFI,MIJ,FIARR)
C END NEW
        AP(IJP)=AP(IJP)-AR(I)
        AP(IJN)=AP(IJN)-AL(I)
      END DO
C
C.....FACE SEGMENT BOUNDARIES (THESE ARE NO INTERNAL BOUNDARIES!)
C
      DO I=1,NFSGBKAL
        IJP=IJFL(I)
        IJN=IJFR(I)
        CALL FLUXSC(IFI,IJP,IJN,XFC(I),YFC(I),ZFC(I),
     *                          XFR(I),YFR(I),ZFR(I),
C NEW
C    *              FMF(I),AFL(I),AFR(I),FFSGBK(I),GFI,MIJ,FI)
     *              FMF(I),AFL(I),AFR(I),FFSGBK(I),GFI,MIJ,FIARR)
C END NEW
        AP(IJP) =AP(IJP) -AFR(I)
        AP(IJN) =AP(IJN) -AFL(I)
      END DO
C END NEW
C
C.....UNSTEADY TERM CONTRIBUTION
C
      IF(LTIME) THEN
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        APT=DEN(IJK)*VOL(IJK)*DTR
        SU(IJK)=SU(IJK)+APT*((1.+GAMT)*FIO(IJK)-0.5*GAMT*FIOO(IJK))
        AP(IJK)=AP(IJK)+APT*(1.+0.5*GAMT)
      END DO
      END DO
      END DO
      END DO
      ENDIF
C
C.....INLET BOUNDARIES
C
      DO II=1,NINLBKAL
        IJP=IJPI(II)
        IJB=IJI(II)
        DPX(IJB)=DPX(IJP)
        DPY(IJB)=DPY(IJP)
        DPZ(IJB)=DPZ(IJP)
        CALL FLUXSC(IFI,IJP,IJB,XIC(II),YIC(II),ZIC(II),
     *                          XIR(II),YIR(II),ZIR(II),
C NEW
C    *               FMI(II),CP,CB,ONE,ZERO,1,FI)
     *               FMI(II),CP,CB,ONE,ZERO,1,FIARR)
C END NEW
        AP(IJP)=AP(IJP)-CB
        SU(IJP)=SU(IJP)-CB*FI(IJB)
      END DO
C
C.....OUTLET BOUNDARIES
C
      DO IO=1,NOUTBKAL
        IJP=IJPO(IO)
        IJB=IJO(IO)
        DPX(IJB)=DPX(IJP)
        DPY(IJB)=DPY(IJP)
        DPZ(IJB)=DPZ(IJP)
        CALL FLUXSC(IFI,IJP,IJB,XOC(IO),YOC(IO),ZOC(IO),
     *                          XUR(IO),YOR(IO),ZOR(IO),
C NEW
C    *              FMO(IO),CP,CB,ONE,ZERO,1,FI)
     *              FMO(IO),CP,CB,ONE,ZERO,1,FIARR)
C END NEW
        AP(IJP)=AP(IJP)-CB
        SU(IJP)=SU(IJP)-CB*FI(IJB)
      END DO
C NEW
C
C.....RESTORE GHOSTED VERSION
C
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
      CALL VecGhostRestoreLocalForm(FIVEC,FIL,IERR)
C END NEW
C
C.....WALL BOUNDARY CONDITIONS AND SOURCES FOR TEMPERATURE
C
      IF(IFI.EQ.IEN) CALL TEMP
C
C.....WALL BOUNDARY CONDITIONS AND SOURCE FOR K-e
C
C     IF(IFI.EQ.ITE) CALL KINE
C     IF(IFI.EQ.IED) CALL DISE
C
C.....GENERIC SOURCE TERM MODIFICATIONS
C
C NEW
C
C.....GET LOCAL REPRESENTATION OF VECTOR
C
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
C END NEW
C     CALL SOURCESC(IFI,FI)
C NEW
C
C.....RESTORE ARRAYS THAT ARE NOT NEEDED ANYMORE
C
      CALL VecRestoreArray(DPXL,DPXARR,DPXXI,IERR)
      CALL VecRestoreArray(DPYL,DPYARR,DPYYI,IERR)
      CALL VecRestoreArray(DPZL,DPZARR,DPZZI,IERR)
C
      CALL VecRestoreArray(DENL,DENARR,DENNI,IERR)
      CALL VecRestoreArray(VISL,VISARR,VISSI,IERR)
C
      CALL VecGhostRestoreLocalForm(DPXVEC,DPXL,IERR)
      CALL VecGhostRestoreLocalForm(DPYVEC,DPYL,IERR)
      CALL VecGhostRestoreLocalForm(DPZVEC,DPZL,IERR)
C
      CALL VecGhostRestoreLocalForm(DENVEC,DENL,IERR)
      CALL VecGhostRestoreLocalForm(VISVEC,VISL,IERR)
C
C END NEW
C
C.....FINAL COEFFICIENT AND SOURCE MATRIX FOR FI-EQUATION
C
C.....OpenMP : Here starts parallel loop section
CC$OMP PARALLEL DO DEFAULT(SHARED), PRIVATE(M,
CC$OMP*  K,I,J,IJK,NKMT,NIMT,NJMT)
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        AP(IJK)=(AP(IJK)-AE(IJK)-AW(IJK)-AN(IJK)-AS(IJK)
     *                  -AB(IJK)-AT(IJK))*URFFI
        SU(IJK)=SU(IJK)+(1.-URF(IFI))*AP(IJK)*FI(IJK)
      END DO
      END DO
      END DO
      END DO
C.....OpenMP : Here ends this parallel loop section
C NEW
C
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
C
C.....UPDATE SOURCE AND DIAGONAL VECTOR ON ALL PROCESSORS, ASSEMBLE MATRIX
C
      CALL VecRestoreArray(SUL,SUARR,SUUI,IERR)
      CALL VecRestoreArray(APL,APARR,APPI,IERR)
C
      CALL VecGhostRestoreLocalForm(SUVEC,SUL,IERR)
      CALL VecGhostRestoreLocalForm(APVEC,APL,IERR)
C
      CALL VecGhostUpdateBegin(SUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(SUVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateBegin(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
      CALL VecGhostUpdateEnd(APVEC,ADD_VALUES,SCATTER_REVERSE,IERR)
C
C     CALL ASSEMBLESYS(AMAT)
C     CALL MatZeroRows(AMAT,NZERO,ZEROS,PONE,FIVEC,SUVEC,IERR)
C END NEW
C
C.....SOLVING EQUATION SYSTEM FOR FI-EQUATION
C
C     CALL SOLVER(FI,IFI)
C NEW
C
C.....SOLVING EQUATION SYSTEM FOR V-VELOCITY USING PETSC SOLVER
C
C     CALL SOLVESYS(FIVEC,IFI,AMAT)
C
      CALL VecGetArray(FIL,FIARR,FIII,IERR)
C END NEW
C
C.....SYMMETRY AND OUTLET BOUNDARIES
C
      DO ISY=1,NSYMBKAL
        FI(IJS(ISY))=FI(IJPS(ISY))
      END DO
C
      DO IO=1,NOUTBKAL
        FI(IJO(IO))=FI(IJPO(IO))
      END DO
C NEW
C
      CALL VecRestoreArray(FIL,FIARR,FIII,IERR)
C END NEW
C
      RETURN
      END 
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C################################################################
      SUBROUTINE FLUXSC(IFI,IJP,IJN,XXC,YYC,ZZC,XCR,YCR,ZCR,
C NEW
C    *                  FM,CAP,CAN,FAC,G,MB,FI)
     *                  FM,CAP,CAN,FAC,G,MB,FIARR)
C END NEW
C################################################################
C     This routine calculates scalar fluxes (convective and
C     diffusive) through the cell face between nodes IJP and IJN.
C     It is analogous to the routine FLUXUV, see above. 
C
C     XXC, YYC, ZZC are the coordinates of the face center
C     while XCR, YCR, ZCR are the surface normal vector
C     components (aligned with the direction P->N so that
C     the normal vector is pointing outwards from the P cell)
C
C================================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "grad3d.inc"
#include "coef3d.inc"
#include "model3d.inc"
#include "propcell3d.inc"
C
      INTEGER IJN,IJP,MB,IFI
C
C NEW
C     REAL*8 FI(NXYZA)
      REAL*8 FIARR( 1)
C END NEW
      REAL*8 FM,FACP,FAC,ZZC,XXC,YYC,XCR,YCR,ZCR,FMI
      REAL*8 FMX,FII,DENI,VISI,DFXI,DFYI,DFZI,XPN,YPN,ZPN
      REAL*8 VSOL,FCFIE,FCFII,FDFII,CAN,CAP,FFIC,G,FDFIE
C NEW
      PetscOffset FIII
      COMMON /SCALARFI/ FIII
C NEW
#include "petsc.user.inc"
C END NEW
C==============================================================
C
C.....INTERPOLATE ALONG LINE P-N
C
      FACP=1.-FAC
      DENI=DEN(IJN)*FAC+DEN(IJP)*FACP
C
C.....COMPUTE FM CORRECTED FOR RELATIVE MOVEMENT
C
      FM=FM-DENI*((WROTY(MB)*ZZC-WROTZ(MB)*YYC)*XCR+
     *            (WROTZ(MB)*XXC-WROTX(MB)*ZZC)*YCR+
     *            (WROTX(MB)*YYC-WROTY(MB)*XXC)*ZCR)
      FMI=MIN(FM,ZERO)
      FMX=MAX(FM,ZERO)
C
C.....INTERPOLATE ALONG LINE P-N
C
      FII=FI(IJN)*FAC+FI(IJP)*FACP
      VISI=VIS(IJN)*FAC+VIS(IJP)*FACP-VISC
      DFXI=DPX(IJN)*FAC+DPX(IJP)*FACP
      DFYI=DPY(IJN)*FAC+DPY(IJP)*FACP
      DFZI=DPZ(IJN)*FAC+DPZ(IJP)*FACP
C
C.....DIFFUSION COEFFICIENT
C
C
      IF(IFI.EQ.IEN) DCOEF=(VISC+VISI/SIGT)/PRANL
C
C     IF(IFI.EQ.ITE) DCOEF=VISC+VISI/SIGTE
C     IF(IFI.EQ.IED) DCOEF=VISC+VISI/SIGED
C
C.....DISTANCE VECTOR COMPONENTS, DIFFUSION COEFFICIENT
C
      XPN=XC(IJN)-XC(IJP)
      YPN=YC(IJN)-YC(IJP)
      ZPN=ZC(IJN)-ZC(IJP)
      VSOL=DCOEF*SQRT((XCR**2+YCR**2+ZCR**2)/
     *               (XPN**2+YPN**2+ZPN**2))
C
C.....EXPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCFIE=FM*FII
      FDFIE=DCOEF*(DFXI*XCR+DFYI*YCR+DFZI*ZCR)
C
C.....IMPLICIT CONVECTIVE AND DIFFUSIVE FLUXES
C
      FCFII=FMI*FI(IJN)+FMX*FI(IJP)
      FDFII=VSOL*(DFXI*XPN+DFYI*YPN+DFZI*ZPN)
C
C.....COEFFICIENTS, DEFERRED CORRECTION, SOURCE TERMS
C
      CAN=-VSOL+FMI
      CAP=-VSOL-FMX
      FFIC=G*(FCFIE-FCFII)
      SU(IJP)=SU(IJP)-FFIC+FDFIE-FDFII
      SU(IJN)=SU(IJN)+FFIC-FDFIE+FDFII
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C############################################################### 
      SUBROUTINE TEMP
C###############################################################
C     This routine assembles the source terms (volume integrals)
C     and applies wall boundary conditions for the temperature
C     (energy) equation.
C===============================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "coef3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "model3d.inc"
C
      INTEGER M,IW,IJP,IJB
C
      REAL*8 COEF
C NEW
      PetscErrorCode IERR
C NEW
#include "petsc.user.inc"
C END NEW
C==============================================================      
C
C NEW
C
C.....GET ARRAY OF TEMPERATURE
C
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C END NEW
C.....NO VOLUMETRIC SOURCES OF THERMAL ENERGY 
C
C.....ISOTHERMAL WALL BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IW=IWST+1,IWST+NWALI
        IJP=IJPW(IW)
        IJB=IJW(IW)
        COEF=DCOEF*SRDW(IW)
        AP(IJP)=AP(IJP)+COEF
        SU(IJP)=SU(IJP)+COEF*T(IJB)
      END DO
      END DO
C
C.....ADIABATIC WALL BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IW=IWAT+1,IWAT+NWALA
        T(IJW(IW))=T(IJPW(IW))
      END DO
      END DO
C
C.....Call to Inner Walls TT modifications routine
C
C NEW
C     CALL INNWALLTT
C END NEW
C
C NEW
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C END NEW
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C########################################################
      SUBROUTINE SETIND(M)
C########################################################
C     This routine sets the indices for the current grid
C     block.
C========================================================     
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "bound3d.inc"
C
      INTEGER M
C========================================================
C
      NI=NIBK(M)
      NJ=NJBK(M)
      NK=NKBK(M)
      IST=IBK(M)
      JST=JBK(M)
      KST=KBK(M)
      IJKST=IJKBK(M)
      NIJK=NIJKBK(M)
C
      NINL=NINLBK(M)
      NOUT=NOUTBK(M)
      NSYM=NSYMBK(M)
      NWAL=NWALBK(M)
      NWALA=NWALABK(M)
      NWALI=NWALIBK(M)
C NEW
      NMTM=NMTMBK(M)
      NFSG=NFSGBK(M)
C END NEW
      IIST=IIBK(M)
      IOST=IOBK(M)
      ISST=ISBK(M)
      IWST=IWBK(M)
      IWAT=IWABK(M)
C NEW
      IMST=IMBK(M)
      IFST=IFBK(M)
C END NEW
C
      NIM=NI-1
      NJM=NJ-1
      NKM=NK-1
      NIJ=NI*NJ
C
      RETURN
      END
C
C
C########################################################
      SUBROUTINE INIT
C########################################################
C     This routine reads input parameters, grid data etc.
C
C     Extended to 3D and block-structured grids 
C========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "varold3d.inc"
#include "model3d.inc"
C
      INTEGER I,M,K,J,IJK,NJBKAL,NIABK,NOABK,NSABK,NWABK
C NEW
      PetscErrorCode IERR
      CHARACTER(LEN=100) STAGENAME
C NEW
C NEW
#include "petsc.user.inc"
C END NEW
C========================================================
C
C.....READ INPUT DATA IN THE FOLLOWING ORDER OF RECORDS:
C
C   1.  TITLE FOR THE PROBLEM SOLVED;
C   2.  LOGICAL CONTROL PARAMETERS;
C   3.  INDICES OF MONITORING LOCATION AND PRESSURE REFERENCE POINT,
C       NUMBER OF PRESSURE CORRECTIONS AND ITERATIONS ON GRADIENT;
C   4.  CONVERGENCE AND DIVERGENCE CRITERION, SIP-PARAMETER;
C   5.  DENSITY, DYNAMIC VISCOSITY AND PRANDTL NUMBER;
C   6.  GRAVITY COMP., EXPANSION COEF., HOT, COLD AND REFERENCE TEMP.;
C   7.  FIELD INITIALIZATION (UIN,VIN,WIN,PIN,TIN,TEIN,EDIN);
C   8.  LID VELOCITY AND PHYSICAL PARAMETERS;
C   9.  NO. OF TIME STEPS, OUTPUT CONTROL, TIME STEP, BLENDING FACTOR;
C  10.  LOGICAL CONTROL VARIABLES (EQ. TO BE SOLVED: U,V,W,PP,T,SMG,TE,ED,VIS);
C  11.  UNDER-RELAXATION FACTORS;
C  12.  CONVERGENCE CRITERION FOR INNER ITERATIONS;
C  13.  MAXIMUM ALLOWED NUMBER OF INNER ITERATIONS;
C  14.  BLENDING FACTOR FOR CONVECTIVE FLUXES;
C  15.  NUMBER OF OUTER ITERATIONS PER TIME STEP
C  16.  NUMBER OF GRID BLOCKS
C
      READ(5,'(A50)') TITLE
C NEW
C     READ(5,*) LREAD,LWRITE,LTEST,LOUTS,LOUTE,LTIME,LGRAD
      READ(5,*) LREAD,LWRITE,LPOST,LTEST,LOUTS,LOUTE,LTIME,LGRAD
C END NEW
      READ(5,*) IMON,JMON,KMON,MMON,IPR,JPR,KPR,MPR,NPCOR,NIGRAD 
      READ(5,*) SORMAX,SLARGE,ALFA
      READ(5,*) DENS,VISC,PRANL
      READ(5,*) GRAVX,GRAVY,GRAVZ,BETA,TH,TC,TREF
      READ(5,*) UIN,VIN,WIN,PIN,TIN,TEIN,EDIN
      READ(5,*) ULID,TPER,TGEN
      READ(5,*) ITSTEP,NOTT,DT,GAMT
      READ(5,*) (LCAL(I),I=1,NFI)
      READ(5,*) (URF(I),I=1,NFI)
      READ(5,*) (SOR(I),I=1,NFI)
      READ(5,*) (NSW(I),I=1,NFI)
      READ(5,*) (GDS(I),I=1,NFI)
      READ(5,*) LSG
C
C.....READ BLOCK AND GRID DATA, GEOMETRY 
C
      CALL READGRIDS
C
C.....OFFSET B.C. INDEXES
C
      CALL OFFSETBC
C
C.....CHECK IF MONITORING POINT IS OK.
C
      IF(IMON.GT.NIBK(MMON)-1) IMON=NIBK(MMON)/2
      IF(JMON.GT.NJBK(MMON)-1) JMON=NJBK(MMON)/2
      IF(KMON.GT.NKBK(MMON)-1) KMON=NKBK(MMON)/2
C
C.....SET MONITORING LOCATION 
C
      IJKMON=LKBK(KBK(MMON)+KMON)+LIBK(IBK(MMON)+IMON)+JMON
C
C.....SET PRESSURE REFERENCE LOCATION
C
      IJKPR=LKBK(KBK(MPR)+KPR)+LIBK(IBK(MPR)+IPR)+JPR
C
C.....RECIPROCAL VALUES OF URF & TIME STEP
C
      URFU=1.0D0/(URF(IU)+SMALL)
      URFV=1.0D0/(URF(IV)+SMALL)
      URFW=1.0D0/(URF(IWW)+SMALL)
      DTR=1.0D0/DT
C
C.....INITIALIZE VISCOSITY AND DENSITY AT ALL NODES
C
C NEW
C     VIS=VISC
C     DEN=DENS
      CALL VecSet(VISVEC,VISC,IERR)
      CALL VecSet(DENVEC,DENS,IERR)
C END NEW
C NEW
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGetSubVector(UVWPVEC,ISU,UVEC,IERR)
      CALL VecGetSubVector(UVWPVEC,ISV,VVEC,IERR)
      CALL VecGetSubVector(UVWPVEC,ISW,WVEC,IERR)
      CALL VecGetSubVector(UVWPVEC,ISP,PVEC,IERR)
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C END NEW

C
C.....INITIALIZE VARIABLES AT INNER NODES OF ALL GRID BLOCKS
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        U(IJK) =UIN
        V(IJK) =VIN
        W(IJK) =WIN
        P(IJK) =PIN
        T(IJK) =TIN
        UO(IJK)=UIN
        VO(IJK)=VIN
        WO(IJK)=WIN
        TO(IJK)=TIN
        TE(IJK)=TEIN
        ED(IJK)=EDIN
        TEO(IJK)=TEIN
        EDO(IJK)=EDIN
      END DO
      END DO
      END DO
      END DO
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C
      CALL VecRestoreSubVector(UVWPVEC,ISU,UVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISV,VVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISW,WVEC,IERR)
      CALL VecRestoreSubVector(UVWPVEC,ISP,PVEC,IERR)
C END NEW
C
C.....Initialize Geometry. 
C
      CALL INITGEO
C
C.....Call to Inner Walls Initialization Routine
C
C NEW
C     CALL INNWALLS
C END NEW
C
C.....Call to Special I.C. Routine           
C
      CALL INITCOND
C
C NEW
C
C......Petsc Logging Stages
C
      WRITE (STAGENAME,"(A8)") "CPLD_SOLVE"
      CALL PetscLogStageRegister(STAGENAME,STAGE1,IERR)
C END NEW
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C###########################################################
      SUBROUTINE READGRIDS
C###########################################################
C     This routine reads the block and grid files where
C     information about topology, geometry and boundary
C     is stored.
C
C     First the 'block' file ('name'.bck) file is read
C     and then each grid block is read through a loop.
C
C
C===========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "geo3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "varold3d.inc"
#include "model3d.inc"
#include "charac3d.inc"
C
      INTEGER I,M,K,XECC(NXYZA)
      INTEGER NJBKAL,NIABK,NOABK,NSABK,NWABK,NOCABK
C NEW
      INTEGER NMABK,NFABK
C END NEW
      INTEGER NWAISBK,NWAADBK,NINX,NOUX,NSYX,NWAX,NOCX
C NEW
      INTEGER NMTX
      integer RANK
      PetscErrorCode IERR
C NEW
#include "petsc.user.inc"
C END NEW
C===========================================================
C
C.....READ BLOCK FILE (UNIT=4)
C
      READ(4)  NBLKS,NIBKAL,NJBKAL,NKBKAL,NIJKBKAL,
     *         NINLBKAL,NOUTBKAL,NSYMBKAL,NWALBKAL,
     *         NOCBKAL,NIABK,NOABK,NSABK,NWABK,NOCABK,
C NEW
     *         NFSGBKAL,NFABK
C END NEW
C
      READ(4) (LIBK(I) ,I=1,NIBKAL),( LKBK(I),I=1,NKBKAL),
     *        (NIBK(I) ,I=1,NBLKS) ,( NJBK(I),I=1,NBLKS),
     *        (NKBK(I) ,I=1,NBLKS) ,(  IBK(I),I=1,NBLKS),
     *        ( JBK(I) ,I=1,NBLKS) ,(  KBK(I),I=1,NBLKS),
     *        (IJKBK(I),I=1,NBLKS),(NIJKBK(I),I=1,NBLKS),
C NEW
     *        IJKPRC
C END NEW
C
      READ(4) (NINLBK(I),I=1,NBLKS),(IIBK(I),I=1,NBLKS),
     *        (NOUTBK(I),I=1,NBLKS),(IOBK(I),I=1,NBLKS),
     *        (NWALBK(I),I=1,NBLKS),(IWBK(I),I=1,NBLKS),
     *        (NSYMBK(I),I=1,NBLKS),(ISBK(I),I=1,NBLKS),
     *        (NWALABK(I),I=1,NBLKS),(IWABK(I),I=1,NBLKS),
     *        (NWALIBK(I),I=1,NBLKS),
C NEW
     *        (NMTMBK(I),I=1,NBLKS),(IMBK(I),I=1,NBLKS),
     *        (NFSGBK(I),I=1,NBLKS),(IFBK(I),I=1,NBLKS)
C END NEW
C
      READ(4) (IJLPBK(I) ,I=1,NOCABK),(IJLBBK(I) ,I=1,NOCABK),
     *        (IJRPBK(I) ,I=1,NOCABK),(IJRBBK(I) ,I=1,NOCABK),
     *        (IJOC1BK(I),I=1,NOCABK),(IJOC2BK(I),I=1,NOCABK),
     *        (IJOC3BK(I),I=1,NOCABK),(IJOC4BK(I),I=1,NOCABK),
     *        (FOCBK(I)  ,I=1,NOCABK),(ITAGOCBK(I),I=1,NOCABK),
     *        (IBLKOCBK(I),I=1,NOCABK)
C NEW
      READ(4) (IJFL(I),I=1,NFABK),(IJFR(I),I=1,NFABK),
C    *        (IJF1(I),I=1,NFABK),(IJF2(I),I=1,NFABK),
C    *        (IJF3(I),I=1,NFABK),(IJF4(I),I=1,NFABK),
C APPLY GHOSTING TO STORE FSG CORNER VERTEX COORDINATES
C    *        (X(I),I=NIJKBKAL+1,NIJKBKAL+NFABK*4),
C    *        (Y(I),I=NIJKBKAL+1,NIJKBKAL+NFABK*4),
C    *        (Z(I),I=NIJKBKAL+1,NIJKBKAL+NFABK*4),
     *        (FFSGBK(I),I=1,NFABK),
     *        (XFR(I),I=1,NFABK),(YFR(I),I=1,NFABK),
     *        (ZFR(I),I=1,NFABK),
     *        (XFC(I),I=1,NFABK),(YFC(I),I=1,NFABK),
     *        (ZFC(I),I=1,NFABK)
C
C.....CREATE VECTORS, GHOSTING ETC.
C
      CALL DISTRIBUTELOAD
C
C.....PREPARE VOLUME VECTOR
C
      CALL VecGetArray(VOLVEC,VOLARR,VOLLI,IERR)
      CALL VecGetArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecGetArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecGetArray(ZCVEC,ZCARR,ZCCI,IERR)
C END NEW
C
C.....LOOP THROUGH BLOCKS READING EACH GRID BLOCK FILE INTO THE ARRAYS
C
      DO M=1,NBLKS
C
      CALL SETIND(M)
C
C.....READ FILE NAME FOR THIS GRID BLOCK, OPEN FILE
C
C NEW
C     READ(5,'(A7)') NAMEGRD
      READ(9,'(A13)') FILGRD
C END NEW

C     WRITE(FILGRD,'(A7,4H.grd)') NAMEGRD
      
      OPEN (UNIT=8,FILE=FILGRD,FORM='UNFORMATTED',POSITION='REWIND')
C
      READ(8) NI,NJ,NK,NIJK,NIABK,NOABK,NSABK,NWABK,NOCABK,
C NEW
     *        NMABK,
C END NEW
     *        NWAISBK,NWAADBK,NINX,NOUX,NSYX,NWAX,NOCX,
C NEW
     *        NMTX
C END NEW
C
C.....ARRAY XEC IS USED HERE FOR 'dummy' READING
C
      READ(8) (XECC(I),I=1,NI),(XECC(K),K=1,NK)
C
      READ(8) (IJI(I),I=IIST+1,IIST+NINX),(IJPI(I),I=IIST+1,IIST+NINX),
     *   (IJI1(I) ,I=IIST+1,IIST+NINX),(IJI2(I),I=IIST+1,IIST+NINX),
     *   (IJI3(I) ,I=IIST+1,IIST+NINX),(IJI4(I),I=IIST+1,IIST+NINX),
     *   (ITAGI(I),I=IIST+1,IIST+NINX)
C
      READ(8) (IJO(I),I=IOST+1,IOST+NOUX),(IJPO(I),I=IOST+1,IOST+NOUX),
     *   (IJO1(I) ,I=IOST+1,IOST+NOUX),(IJO2(I),I=IOST+1,IOST+NOUX),
     *   (IJO3(I) ,I=IOST+1,IOST+NOUX),(IJO4(I),I=IOST+1,IOST+NOUX),
     *   (ITAGO(I),I=IOST+1,IOST+NOUX)

      READ(8) (IJW(I),I=IWST+1,IWST+NWAX),(IJPW(I),I=IWST+1,IWST+NWAX),
     *   (IJW1(I) ,I=IWST+1,IWST+NWAX),(IJW2(I),I=IWST+1,IWST+NWAX),
     *   (IJW3(I) ,I=IWST+1,IWST+NWAX),(IJW4(I),I=IWST+1,IWST+NWAX),
     *   (ITAGW(I),I=IWST+1,IWST+NWAX)
C
      READ(8) (IJS(I),I=ISST+1,ISST+NSYX),(IJPS(I),I=ISST+1,ISST+NSYX),
     *   (IJS1(I) ,I=ISST+1,ISST+NSYX),(IJS2(I),I=ISST+1,ISST+NSYX),
     *   (IJS3(I) ,I=ISST+1,ISST+NSYX),(IJS4(I),I=ISST+1,ISST+NSYX),
     *   (ITAGS(I),I=ISST+1,ISST+NSYX)
C
C.....ARRAY XECC IS USED HERE FOR 'dummy' READING
C
      READ(8) (XECC(I),I=1,NOCX), (XECC(I),I=1,NOCX),
     *        (XECC(I),I=1,NOCX), (XECC(I),I=1,NOCX),
     *        (XECC(I),I=1,NOCX), (XECC(I),I=1,NOCX),
     *        (XECC(I),I=1,NOCX)
C   
C NEW
C.....ARRAY XECC IS USED HERE FOR 'dummy' READING
C
      READ(8)
     *        (IJML(I),I=IMST+1,IMST+NMTX),(IJMR(I),I=IMST+1,IMST+NMTX),
     *        (XECC(I),I=1,NMTX),(XECC(I),I=1,NMTX),
     *        (XECC(I),I=1,NMTX),(XECC(I),I=1,NMTX),
     *        (XECC(I),I=1,NMTX)
C
C END NEW
      READ(8) (X(I),I=IJKST+1,IJKST+NIJK),(Y(I),I=IJKST+1,IJKST+NIJK),
     *   (Z(I) ,I=IJKST+1,IJKST+NIJK), (XC(I),I=IJKST+1,IJKST+NIJK),
     *   (YC(I),I=IJKST+1,IJKST+NIJK), (ZC(I),I=IJKST+1,IJKST+NIJK),
     *   (FX(I),I=IJKST+1,IJKST+NIJK), (FY(I),I=IJKST+1,IJKST+NIJK),
     *   (FZ(I),I=IJKST+1,IJKST+NIJK),(VOL(I),I=IJKST+1,IJKST+NIJK),
     *   (SRDW(I),I=IWST+1,IWST+NWAX),(XNW(I),I=IWST+1,IWST+NWAX),
     *   (YNW(I) ,I=IWST+1,IWST+NWAX),(ZNW(I),I=IWST+1,IWST+NWAX),
     *   (SRDS(I),I=ISST+1,ISST+NSYX),(XNS(I),I=ISST+1,ISST+NSYX),
     *   (YNS(I),I=ISST+1,ISST+NSYX),(ZNS(I),I=ISST+1,ISST+NSYX),
C
C.....ARRAY XEC IS USED HERE FOR 'dummy' READING
C
     *   (XEC(I),I=1,NOCX)
C
C     REWIND 8
      CLOSE(UNIT=8)

C
      END DO
C
C
C NEW
C
C.....RESTORE VOLUME ARRAY AND EXCHANGE VALUES
C
      CALL VecRestoreArray(VOLVEC,VOLARR,VOLLI,IERR)
      CALL VecRestoreArray(XCVEC,XCARR,XCCI,IERR)
      CALL VecRestoreArray(YCVEC,YCARR,YCCI,IERR)
      CALL VecRestoreArray(ZCVEC,ZCARR,ZCCI,IERR)
C
      CALL VecGhostUpdateBegin(
     *     VOLVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(VOLVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(XCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(XCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(YCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(YCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateBegin(ZCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
      CALL VecGhostUpdateEnd(ZCVEC,INSERT_VALUES,SCATTER_FORWARD,IERR)
C END NEW
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C###########################################################
      SUBROUTINE OUTIN
C###########################################################
C     This routine prints title and parameters used in
C     computation.
C===========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "logic3d.inc"
#include "varold3d.inc"
#include "model3d.inc"
C
      REAL*8 OMGT
C NEW
#include "petsc.user.inc"
C END NEW
C===========================================================
C
C.....PRINT TITLE, DENSITY, VISCOSITY, CONV. CRITERION, SIP-PARAMETER
C    
      WRITE(2,'(A50)') TITLE
      WRITE(2,*) '====================================================='
      WRITE(2,*) '  '
      WRITE(2,*) '     FLUID DENSITY     : ',DENS
      WRITE(2,*) '     DYNAMIC VISCOSITY : ',VISC
      WRITE(2,*) '     CONVERGENCE CRIT. : ',SORMAX
      WRITE(2,*) '     SIP-PARAMETER     : ',ALFA
      WRITE(2,*) '    '
      WRITE(2,*) '    '
C
C
C
C.....PRINT PARAMETERS FOR ENERGY EQUATION
C
      IF(LCAL(IEN)) THEN
        WRITE(2,*) '     PRANDTL NUMBER        : ',PRANL
        WRITE(2,*) '     HOT WALL TEMPERATURE  : ',TH
        WRITE(2,*) '     COLD WALL TEMPERATURE : ',TC
        WRITE(2,*) '     REFERENCE TEMPERATURE : ',TREF
        WRITE(2,*) '     '
      ENDIF
C
C.....PRINT UNDER-RELAXATION FACTORS
C
      IF(LCAL(IU))  WRITE(2,*) '     UNDER-RELAXATION FOR U: ',URF(IU)
      IF(LCAL(IV))  WRITE(2,*) '     UNDER-RELAXATION FOR V: ',URF(IV)
      IF(LCAL(IWW)) WRITE(2,*) '     UNDER-RELAXATION FOR W: ',URF(IWW)
      IF(LCAL(IP))  WRITE(2,*) '     UNDER-RELAXATION FOR P: ',URF(IP)
      IF(LCAL(IEN)) WRITE(2,*) '     UNDER-RELAXATION FOR T: ',URF(IEN)
      IF(LCAL(ITE)) WRITE(2,*) '     UNDER-RELAXATION FOR TE:',URF(ITE)
      IF(LCAL(IED)) WRITE(2,*) '     UNDER-RELAXATION FOR ED:',URF(IED)
      WRITE(2,*) '     '
C
C.....PRINT BLENDING FACTORS FOR CONVECTIVE FLUXES (CDS-PART)
C
      WRITE(2,*) '     DIFFUSIVE FLUXES DISCRETIZED USING CDS '
      WRITE(2,*) '     CONTRIBUTION OF CDS VS. UDS IN CONV. FLUXES: '
      IF(LCAL(IU))  WRITE(2,*) '     BLENDING FACTOR FOR U: ',GDS(IU)
      IF(LCAL(IV))  WRITE(2,*) '     BLENDING FACTOR FOR V: ',GDS(IV)
      IF(LCAL(IWW)) WRITE(2,*) '     BLENDING FACTOR FOR W: ',GDS(IWW)
      IF(LCAL(IEN)) WRITE(2,*) '     BLENDING FACTOR FOR T: ',GDS(IEN)
      IF(LCAL(ITE)) WRITE(2,*) '     BLENDING FACTOR FOR TE:',GDS(ITE)
      IF(LCAL(IED)) WRITE(2,*) '     BLENDING FACTOR FOR ED:',GDS(IED)
      WRITE(2,*) '     '
C
C.....PRINT CONVERGENCE CRITERION FOR INNER ITERATIONS
C
      IF(LCAL(IU))  WRITE(2,*) '     CONV. CRIT. FOR U: ',SOR(IU)
      IF(LCAL(IV))  WRITE(2,*) '     CONV. CRIT. FOR V: ',SOR(IV)
      IF(LCAL(IWW)) WRITE(2,*) '     CONV. CRIT. FOR W: ',SOR(IWW)
      IF(LCAL(IP))  WRITE(2,*) '     CONV. CRIT. FOR P: ',SOR(IP)
      IF(LCAL(IEN)) WRITE(2,*) '     CONV. CRIT. FOR T: ',SOR(IEN)
      IF(LCAL(ITE)) WRITE(2,*) '     CONV. CRIT. FOR TE:',SOR(ITE)
      IF(LCAL(IED)) WRITE(2,*) '     CONV. CRIT. FOR ED:',SOR(IED)
      WRITE(2,*) '     '
C
C.....PRINT MAXIMUM NUMBER OF INNER ITERATIONS
C
      IF(LCAL(IU))  WRITE(2,*) '     MAX. INNER ITER. FOR U: ',NSW(IU)
      IF(LCAL(IV))  WRITE(2,*) '     MAX. INNER ITER. FOR V: ',NSW(IV)
      IF(LCAL(IWW)) WRITE(2,*) '     MAX. INNER ITER. FOR W: ',NSW(IWW)
      IF(LCAL(IP))  WRITE(2,*) '     MAX. INNER ITER. FOR P: ',NSW(IP)
      IF(LCAL(IEN)) WRITE(2,*) '     MAX. INNER ITER. FOR T: ',NSW(IEN)
      IF(LCAL(ITE)) WRITE(2,*) '     MAX. INNER ITER. FOR TE:',NSW(ITE)
      IF(LCAL(IED)) WRITE(2,*) '     MAX. INNER ITER. FOR ED:',NSW(IED)
      WRITE(2,*) '     '
C
C.....PRINT TIME STEP SIZE, OUTPUT CONTROL, BLENDING FACTOR
C
      IF(LTIME) THEN
        OMGT=1.-GAMT
        WRITE(2,*) '     TIME STEP SIZE:                  ',DT
        WRITE(2,*) '     NUMBER OF TIME STEPS TO PERFORM: ',ITSTEP
        WRITE(2,*) '     SAVE RESULTS EVERY   ',NOTT,' TIME STEPS'
        WRITE(2,*) '     BLENDING ',GAMT,' OF THREE TIME LEVEL SCHEME'
        WRITE(2,*) '         WITH ',OMGT,' OF IMPLICIT EULER SCHEME '
        WRITE(2,*) '     '
      ENDIF
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C###########################################################
      SUBROUTINE OUTRES
C###########################################################
C     This routine prints out the variable fields.
C
C
C===========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "logic3d.inc"
#include "rcont3d.inc"
#include "var3d.inc"
#include "model3d.inc"
C
      INTEGER M
C NEW
#include "petsc.user.inc"
C END NEW
C===========================================================
C
C.....PRINT VELOCITIES, PRESSURE AND TEMPERATURE
C
      DO M=1,NBLKS
C     IF(LCAL(IU))  CALL PRINT(M,U,'U VEL.')
C     IF(LCAL(IV))  CALL PRINT(M,V,'V VEL.')
C     IF(LCAL(IWW)) CALL PRINT(M,W,'W VEL.')
C     IF(LCAL(IP))  CALL PRINT(M,P,'PRESS.')
C     IF(LCAL(IEN)) CALL PRINT(M,T,'TEMPER')
      IF(LCAL(IU))  CALL PRINT(M,UVEC,'U VEL.')
      IF(LCAL(IV))  CALL PRINT(M,VVEC,'V VEL.')
      IF(LCAL(IWW)) CALL PRINT(M,WVEC,'W VEL.')
      IF(LCAL(IP))  CALL PRINT(M,PVEC,'PRESS.')
      IF(LCAL(IEN)) CALL PRINT(M,TVEC,'TEMPER')
C     IF(LCAL(ITE)) CALL PRINT(M,TE,'TKENE.')
C     IF(LCAL(IED)) CALL PRINT(M,ED,'EDISI.')
      END DO
C
C.....PRINT MASS FLUXES IF TESTING
C
C     IF(LTEST.AND.LCAL(IU)) CALL PRINT(F1,'MASS_E')
C     IF(LTEST.AND.LCAL(IU)) CALL PRINT(F2,'MASS_N')
C
      RETURN
C
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C##########################################################
C NEW
C     SUBROUTINE PRINT(M,FI,HEDFI)
      SUBROUTINE PRINT(M,FIVEC,HEDFI)
C END NEW
C##########################################################
C     This routine prints the field variables in an easy to
C     read form.
C==========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
C
      INTEGER M,NL,LKK,IEND,L,IBEG,I,J,K
C
C NEW
C     REAL*8 FI(NXYZA)
      REAL*8 FIARR( 1)
      Vec FIVEC
      PetscOffset FIII
      PetscErrorCode IERR
C END NEW
C
      CHARACTER(LEN=6) HEDFI
C NEW
#include "petsc.user.inc"
C END NEW
C===========================================================
C NEW
      CALL VecGetArray(FIVEC,FIARR,FIII,IERR)
C END NEW
C
C.....SET INDICES, PRINT HEADER, FIND HOW MANY 12-COLUMN BLOCKS
C
      CALL SETIND(M)
      WRITE(2,20) HEDFI
C
      NL=NI/12+1
      IF(MOD(NI,12).EQ.0) NL=NL-1
C
C.....PRINT THE ARRAY FI
C
      DO K=1,NK
        LKK=LKBK(K+KST)
        WRITE(2,21) K
        IEND=0
C
        DO L=1,NL
          IBEG=IEND+1
          IEND=MIN(IBEG+11,NI)
          WRITE(2,'(3X,4HI = ,I3,11I10)') (I,I=IBEG,IEND)
          WRITE(2,*) '  J'
          DO J=NJ,1,-1
            WRITE(2,'(1X,I3,1P12E10.2)')
     *            J,(FI(LKK+LIBK(I+IST)+J),I=IBEG,IEND)
          END DO
        END DO
C
      END DO
C NEW
      CALL VecRestoreArray(FIVEC,FIARR,FIII,IERR)
C END NEW
C
   20 FORMAT(2X,26('*-'),6X,A7,6X,26('-*'))
   21 FORMAT(2X,26('*-'),6X,'K=',I4,6X,26('-*'))
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C########################################################
      SUBROUTINE SETDAT
C########################################################
C     In this routine some constants are assigned values.
C
C
C========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "model3d.inc"
C========================================================
C
      IU=1
      IV=2
      IWW=3
      IP=4
      IEN=5
C
      SMALL=1.E-20
      GREAT=1.E+20
      ONE=1.0D0
      ZERO=0.0D0
      PII=3.14159265358979
C
C
      RETURN
      END
C
C
C###################################################################
C NEW
C      SUBROUTINE SRES
       SUBROUTINE SRES(RANK)
C END NEW
C###################################################################
C     This routine writes out the results onto a file
C     so that re-start is possible at a later stage.
C===================================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "parinw3d.inc"
#include "logic3d.inc"
#include "indexc3d.inc"
#include "var3d.inc"
#include "varold3d.inc"
#include "bound3d.inc"
#include "bcinw3d.inc"
#include "charac3d.inc"
#include "model3d.inc"
#include "rcont3d.inc"
C
      INTEGER IJK,I
C NEW
      INTEGER RANK
      PetscErrorCode IERR
#include "petsc.user.inc"
C END NEW
C==================================================================
C
C NEW
      WRITE(FILRES,'(A7,I4.4,4H.res)') NAME, RANK
C     OPEN (UNIT=3,FILE=FILRES,FORM='binary')
      OPEN (UNIT=3,FILE=FILRES,FORM='UNFORMATTED',POSITION='REWIND')
C     REWIND 3
C
C.....EXTRACT ARRAYS FROM VECTORS
C
      CALL VecGetArray(UVEC,UARR,UUI,IERR)
      CALL VecGetArray(VVEC,VARR,VVI,IERR)
      CALL VecGetArray(WVEC,WARR,WWI,IERR)
      CALL VecGetArray(PVEC,PARR,PPI,IERR)
      CALL VecGetArray(TVEC,TARR,TTI,IERR)
C END NEW
C
      WRITE(3)ITIM,TIME,(F1(IJK),IJK=1,NIJKBKAL),
     *        (F2(IJK),IJK=1,NIJKBKAL),(F3(IJK),IJK=1,NIJKBKAL),
     *        (U(IJK), IJK=1,NIJKBKAL),(V(IJK), IJK=1,NIJKBKAL),
     *        (W(IJK), IJK=1,NIJKBKAL),(P(IJK), IJK=1,NIJKBKAL),
     *        (T(IJK), IJK=1,NIJKBKAL),(TE(IJK), IJK=1,NIJKBKAL),
     *        (ED(IJK), IJK=1,NIJKBKAL),(FMOC(I),I=1,NOCBKAL),
C NEW
     *        (FMF(I),I=1,NFSGBKAL),(RESINI(I),I=1,4),
     *        (RESOR(I),I=1,4)
C END NEW
C
      IF(LTIME) WRITE(3) (UO(IJK),IJK=1,NIJKBKAL),
     *        (VO (IJK),IJK=1,NIJKBKAL),(WO (IJK),IJK=1,NIJKBKAL),
     *        (TO (IJK),IJK=1,NIJKBKAL),(TEO(IJK), IJK=1,NIJKBKAL),
     *        (EDO(IJK),IJK=1,NIJKBKAL)
C
      CLOSE(UNIT=3)
C NEW
C
C.....RESTORE ARRAYS FROM VECTORS
C
      CALL VecRestoreArray(UVEC,UARR,UUI,IERR)
      CALL VecRestoreArray(VVEC,VARR,VVI,IERR)
      CALL VecRestoreArray(WVEC,WARR,WWI,IERR)
      CALL VecRestoreArray(PVEC,PARR,PPI,IERR)
      CALL VecRestoreArray(TVEC,TARR,TTI,IERR)
C END NEW
C
      RETURN
      END
C NEW
#include "petsc.user.inc"
C END NEW
C
C
C########################################################
      SUBROUTINE INITGEO
C########################################################
C     This routine initializes geometry arrays
C========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
#include "bound3d.inc"
C
      INTEGER M,K,I,J,IJK,II,IO
      INTEGER RANK,IERR
C========================================================
C
C.....Loop through nodes. East faces
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=1,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL CALCFACE(IJK,IJK-1,IJK-1-NIJ,IJK-NIJ,
     *                XEC(IJK),YEC(IJK),ZEC(IJK),
     *                XER(IJK),YER(IJK),ZER(IJK))
      END DO
      END DO
      END DO
      END DO
C
C.....Loop through nodes. North faces
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=2,NKM
      DO I=2,NIM
      DO J=1,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL CALCFACE(IJK-NJ,IJK,IJK-NIJ,IJK-NIJ-NJ,
     *                XNC(IJK),YNC(IJK),ZNC(IJK),
     *                XNR(IJK),YNR(IJK),ZNR(IJK))
C
      END DO
      END DO
      END DO
      END DO
C
C.....Loop through nodes. Top faces
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO K=1,NKM
      DO I=2,NIM
      DO J=2,NJM
        IJK=LKBK(K+KST)+LIBK(I+IST)+J
        CALL CALCFACE(IJK,IJK-NJ,IJK-NJ-1,IJK-1,
     *                XTC(IJK),YTC(IJK),ZTC(IJK),
     *                XTR(IJK),YTR(IJK),ZTR(IJK))
C
      END DO
      END DO
      END DO
      END DO
C
C.....Loop through Inlet faces
C
      DO II=1,NINLBKAL
        CALL CALCFACE(IJI1(II),IJI2(II),IJI3(II),IJI4(II),
     *                XIC(II),YIC(II),ZIC(II),
     *                XIR(II),YIR(II),ZIR(II))
      END DO
C
C.....Loop through Outlet faces
C
      DO IO=1,NOUTBKAL
        CALL CALCFACE(IJO1(IO),IJO2(IO),IJO3(IO),IJO4(IO),
     *                XOC(IO),YOC(IO),ZOC(IO),
     *                XUR(IO),YOR(IO),ZOR(IO))
      END DO
C
C.....Loop through OC faces
C
      DO I=1,NOCBKAL
        CALL CALCFACE(IJOC1BK(I),IJOC2BK(I),IJOC3BK(I),IJOC4BK(I),
     *                XOCC(I),YOCC(I),ZOCC(I),
     *                XOCR(I),YOCR(I),ZOCR(I))
      END DO
C NEW
C
C.....Loop through FSG faces
C
C     DO I=1,NFSGBKAL
C       CALL CALCFACE(IJF1(I),IJF2(I),IJF3(I),IJF4(I),
C    *                XFC(I),YFC(I),ZFC(I),
C    *                XFR(I),YFR(I),ZFR(I))
C     END DO
C END NEW
C
      RETURN
C
      END
C
C
C########################################################
      SUBROUTINE CALCFACE(IJK1,IJK2,IJK3,IJK4,
     *                    XXXC,YYYC,ZZZC,XXCR,YYCR,ZZCR)
C########################################################
C     This routine calculates the surface vector and
C     centre of cell IJK.
C
C     IJK1,IJK2,IJK3,IJK4 define a face by its four corners
C     in clockwise order ( so that the normal vector points out)
C     XXXC,YYYC,ZZZC are the coord of baricenter of the face
C     XXCR,YYCR,ZZCR are the components of the (normal) surface
C     vector (pointing out)
C
C     See 8.6.4 for details
C
C========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "rcont3d.inc"
#include "geo3d.inc"
C
      INTEGER IJK1,IJK2,IJK3,IJK4
C
      REAL*8 DX12,DY12,DZ12,DX13,DY13,DZ13,DX14,DY14,DZ14
      REAL*8 XR23,YR23,ZR23,XR34,YR34,ZR34,XXCR,YYCR,ZZCR
      REAL*8 S23,S34,XXXC,YYYC,ZZZC
C========================================================
C
C.....Vectors to vertices ( from IJK1 to IJK2, IJK3 and IJK4 )
      DX12=X(IJK2)-X(IJK1)
      DY12=Y(IJK2)-Y(IJK1)
      DZ12=Z(IJK2)-Z(IJK1)
C
      DX13=X(IJK3)-X(IJK1)
      DY13=Y(IJK3)-Y(IJK1)
      DZ13=Z(IJK3)-Z(IJK1)
C
      DX14=X(IJK4)-X(IJK1)
      DY14=Y(IJK4)-Y(IJK1)
      DZ14=Z(IJK4)-Z(IJK1)
C
C.....Cross Products for triangle surface vectors 
C.....This is (IJK1,IJK2,IJK3)
      XR23=DY12*DZ13-DZ12*DY13
      YR23=DZ12*DX13-DX12*DZ13
      ZR23=DX12*DY13-DY12*DX13
C
C.....This is (IJK1,IJK3,IJK4)
      XR34=DY13*DZ14-DZ13*DY14
      YR34=DZ13*DX14-DX13*DZ14
      ZR34=DX13*DY14-DY13*DX14
C
C.....Face surface vectors (add both triangles)
      XXCR=0.5D0*(XR23+XR34)
      YYCR=0.5D0*(YR23+YR34)
      ZZCR=0.5D0*(ZR23+ZR34)
C
C.....Baricenters of each triangle
      DX12=(X(IJK1)+X(IJK2)+X(IJK3))/3.0D0
      DY12=(Y(IJK1)+Y(IJK2)+Y(IJK3))/3.0D0
      DZ12=(Z(IJK1)+Z(IJK2)+Z(IJK3))/3.0D0
C
      DX14=(X(IJK1)+X(IJK3)+X(IJK4))/3.0D0
      DY14=(Y(IJK1)+Y(IJK3)+Y(IJK4))/3.0D0
      DZ14=(Z(IJK1)+Z(IJK3)+Z(IJK4))/3.0D0
C
C.....Area of each triangle
      S23=SQRT(XR23**2+YR23**2+ZR23**2)
      S34=SQRT(XR34**2+YR34**2+ZR34**2)
C
C.....Baricenter of face (weighted average)
      XXXC=(DX12*S23+DX14*S34)/(S23+S34+SMALL)
      YYYC=(DY12*S23+DY14*S34)/(S23+S34+SMALL)
      ZZZC=(DZ12*S23+DZ14*S34)/(S23+S34+SMALL)
C
      RETURN
      END
C
C
C########################################################
      SUBROUTINE OFFSETBC
C########################################################
C     This routine modifies the index arrays of B.C.s
C     so that they include the inter-block offsets
C
C========================================================
      implicit none
C
C NEW
#include "finclude/petsc.h"
C END NEW
C
#include "param3d.inc"
#include "indexc3d.inc"
#include "rcont3d.inc"
#include "bound3d.inc"
C
      INTEGER M,II,IO,ISY,IW,
C NEW
     *        IM
C END NEW
C========================================================
C
C.....INLET BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO II=IIST+1,IIST+NINL
        IJI(II) =IJI(II) +IJKST
        IJPI(II)=IJPI(II)+IJKST
        IJI1(II)=IJI1(II)+IJKST      
        IJI2(II)=IJI2(II)+IJKST      
        IJI3(II)=IJI3(II)+IJKST      
        IJI4(II)=IJI4(II)+IJKST      
      END DO
      END DO
C
C.....OUTLET BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IO=IOST+1,IOST+NOUT
        IJO(IO) =IJO(IO) +IJKST
        IJPO(IO)=IJPO(IO)+IJKST
        IJO1(IO)=IJO1(IO)+IJKST      
        IJO2(IO)=IJO2(IO)+IJKST      
        IJO3(IO)=IJO3(IO)+IJKST      
        IJO4(IO)=IJO4(IO)+IJKST      
      END DO
      END DO
C
C.....SYMETRY BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO ISY=ISST+1,ISST+NSYM
        IJS(ISY) =IJS(ISY) +IJKST
        IJPS(ISY)=IJPS(ISY)+IJKST
        IJS1(ISY)=IJS1(ISY)+IJKST      
        IJS2(ISY)=IJS2(ISY)+IJKST      
        IJS3(ISY)=IJS3(ISY)+IJKST      
        IJS4(ISY)=IJS4(ISY)+IJKST      
      END DO
      END DO
C
C.....WALL BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IW=IWST+1,IWST+NWAL
        IJW(IW) =IJW(IW) +IJKST
        IJPW(IW)=IJPW(IW)+IJKST
        IJW1(IW)=IJW1(IW)+IJKST      
        IJW2(IW)=IJW2(IW)+IJKST      
        IJW3(IW)=IJW3(IW)+IJKST      
        IJW4(IW)=IJW4(IW)+IJKST      
      END DO
      END DO
C NEW
C
C.....MTM BOUNDARIES
C
      DO M=1,NBLKS
      CALL SETIND(M)
      DO IM=IMST+1,IMST+NMTM
        IJML(IM)=IJML(IM)+IJKST
        IJMR(IM)=IJMR(IM)+IJKST
      END DO
      END DO
C END NEW
C
C
      RETURN
C
      END
C
C
C---------------------------------------------------------------
C     Here come the routines with user-programmed input data and
C     user-programmed interpretation of results.
C     For each case, create separate user-files,
C     and copy them prior to compilation to the file 'user.f'
C     (routine BCIN provides boundary conditions)
C---------------------------------------------------------------
#include "user.f"
C#include "innwalls.user.f"
C#include "turbulence.models.f"
C NEW
#ifdef USE_TECPLOT
#include "tecpost.f"
#else
#include "vtkpost.f"
#endif
C END NEW
C---------------------------------------------------------------
C     Next lines incorporate the optional solvers. 
C     Only one file should contain the chosen solver in a 
C     subroutine of the form :
C
C     SUBROUTINE SOLVER(FI,IFI)
C
C     The other file should be left empty. It is convenient
C     to set up subdirectories with the desired combination
C     and use the -I<subdir> flag when compiling
C---------------------------------------------------------------
#ifdef USE_SIPSOL
C#include "cgstab3d.f"
#include "sipsol3d.f"
#endif
C---------------------------------------------------------------
C
C NEW
C     Here the PETSc specific soubroutines get included into the
C     CAFFA Code
# include "petsc.user.f"
C END NEW

