FC=mpif90
FCDEFAULT=/work/build/petsc/petsc-3.5.2/arch-default/bin/mpif90 
RM=/bin/rm

######################################################
#PREPROCESSOR FLAGS FOR DIFFERENT BUILDS AND PLATFORMS
######################################################
# -DUSE_ANALYTICAL to use the analytical solution for order verification
# -DUSE_SIPSOL for SIPSOL instead of PETSc 
# -DUSE_TECPLOT to generate .plt instead of .vtk output
# -DUSE_ZEROS if you don't want to use a nullspace for solving the poisson equation (check petsc.user.f for details)
# -DUSE_INFO if you want to print additional information (NULLSPACE,STAGES). This can be useful for bug tracking
# -DUSE_INTERPOLATION if you want to interpolate the pressure correction instead of taking it at the cellcenter
# -DUSE_EXPORT if you want to export the Matrix and Rhs vector each time you call SOLVESYS
# USE COMPILERFLAG -mcmodel=medium for very large cases
#####################################################
#....ADD ADDITIONAL OPTIONS
OPTIONS=-DUSE_INTERPOLATION
#####################################################
#....INTEL
FCFLAGS_DBG_INTEL=-fpp -debug all -traceback -DUSE_INTEL_COMPILER -DUSE_INFO
FCFLAGS_OPT_INTEL=-fpp -O3 -DUSE_INTEL_COMPILER -traceback
#....GNU
FCFLAGS_DBG_GNU=-cpp -g -fbacktrace -ffixed-line-length-none -DUSE_GNU_COMPILER -DUSE_INFO
# Disabled OPTIMIZATION FOR gcc version 4.8.2
FCFLAGS_OPT_GNU=-cpp -ffixed-line-length-none -DUSE_GNU_COMPILER 
FCFLAGS_DEFAULT=-cpp -fPIC -DUSE_ANALYTICAL -DUSE_GNU_COMPILER -Wall -Wno-unused-variable -ffixed-line-length-none -ffree-line-length-0 -Wno-unused-dummy-argument -g

#GENERAL FLAGS
INCLUDE=-I$(PETSC_DIR)/include -I.
TECPLOTINCLUDE=-I/amd/software/linux64/tecplot/360/2011/include
LIBS=-Wl,-rpath=$(PETSC_DIR)/lib -L$(PETSC_DIR)/lib -lpetsc
STATICLIBS=-lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread -lm -lX11 -lpthread -lmpi_f90 -lmpi_f77 -lifport -lifcore -lmpi_cxx -ldl -lmpi -lrt -lnsl -lutil -limf -lsvml -lirng -lipgo -ldecimal -lcilkrts -lstdc++ -lgcc_s -lirc -lirc_s
TECPLOTLIBS=/amd/software/linux64/tecplot/360/2011/lib/tecio64.a
DEFAULTLIB=-Wl,-rpath,/work/build/petsc/petsc-3.5.2/arch-default/lib -L/work/build/petsc/petsc-3.5.2/arch-default/lib  -lpetsc -Wl,-rpath,/work/build/petsc/petsc-3.5.2/arch-default/lib -lflapack -lfblas -lX11 -lpthread -lm -Wl,-rpath,/usr/lib/gcc/x86_64-linux-gnu/4.6 -L/usr/lib/gcc/x86_64-linux-gnu/4.6 -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -lmpichf90 -lgfortran -lm -lgfortran -lm -lquadmath -lm -lmpichcxx -lstdc++ -Wl,-rpath,/work/build/petsc/petsc-3.5.2/arch-default/lib -L/work/build/petsc/petsc-3.5.2/arch-default/lib -Wl,-rpath,/usr/lib/gcc/x86_64-linux-gnu/4.6 -L/usr/lib/gcc/x86_64-linux-gnu/4.6 -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -ldl -Wl,-rpath,/work/build/petsc/petsc-3.5.2/arch-default/lib -lmpich -lopa -lmpl -lrt -lpthread -lgcc_s -ldl
DEFAULTINC=-I/work/build/petsc/petsc-3.5.2/include -I/work/build/petsc/petsc-3.5.2/arch-default/include -I.

#VARIABLENAMES
OUTPUT=caffa3d.cpld.lnx
EXECUTABLE=caffa3d.MB.f
EXPATH=./
FFILES=$(EXECUTABLE)

all: opt-intel

opt-intel:
	$(FC) $(FFILES) $(FCFLAGS_OPT_INTEL) $(INCLUDE) -o $(EXPATH)$(OUTPUT) $(LIBS) $(OPTIONS)

opt-intel-analytical:
	$(FC) $(FFILES) $(FCFLAGS_OPT_INTEL) -DUSE_ANALYTICAL $(INCLUDE) -o $(EXPATH)$(OUTPUT) $(LIBS) $(OPTIONS)

opt-intel-sip:
	$(FC) $(FFILES) $(FCFLAGS_OPT_INTEL) -DUSE_SIPSOL $(INCLUDE) -o $(EXPATH)$(OUTPUT) $(LIBS) $(OPTIONS)

opt-intel-tecplot:
	$(FC) $(FFILES) $(FCFLAGS_OPT_INTEL) -DUSE_TECPLOT $(INCLUDE) $(TECPLOTINCLUDE) -o $(EXPATH)$(OUTPUT) $(LIBS) $(TECPLOTLIBS) $(OPTIONS)

opt-intel-static:
	$(FC) $(FFILES) $(FCFLAGS_OPT_INTEL) $(INCLUDE) -o $(EXPATH)$(OUTPUT) $(LIBS) $(STATICLIBS) $(OPTIONS)

dbg-intel:
	$(FC) $(FFILES) $(FCFLAGS_DBG_INTEL) $(INCLUDE) -o $(EXPATH)$(OUTPUT) $(LIBS) $(OPTIONS)

opt-gnu:
	$(FC) $(FFILES) $(FCFLAGS_OPT_GNU) $(INCLUDE) -o $(EXPATH)$(OUTPUT) $(LIBS) $(OPTIONS)

opt-gnu-analytical:
	$(FC) $(FFILES) $(FCFLAGS_OPT_GNU) -DUSE_ANALYTICAL $(INCLUDE) -o $(EXPATH)$(OUTPUT) $(LIBS) $(OPTIONS)

dbg-gnu:
	$(FC) $(FFILES) $(FCFLAGS_DBG_GNU) $(INCLUDE) -o $(EXPATH)$(OUTPUT) $(LIBS) $(OPTIONS)

dbg-gnu-analytical:
	$(FC) $(FFILES) $(FCFLAGS_DBG_GNU) -DUSE_ANALYTICAL $(INCLUDE) -o $(EXPATH)$(OUTPUT) $(LIBS) $(OPTIONS)

default:
	$(FCDEFAULT) $(FFILES) $(FCFLAGS_DEFAULT) $(DEFAULTINC) -o $(EXPATH)$(OUTPUT) $(DEFAULTLIB) $(OPTIONS)

clean:
	$(RM) -f *.o $(EXPATH)$(OUTPUT)

